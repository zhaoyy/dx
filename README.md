

#
> A vue.js project

  ``` Bash```
# 拉取项目
  git clone http://101.132.150.182/cpic/SCRM-Admin-Web.git

# 安装依赖
  npm install

# 运行项目
  npm run dev

# 目录

## src/constants/chat/noticeType.js
   微客服--->接收app端消息类型
## src/mixins/src/msgTIAVSubmitMessage.js   
   后台--->营销--->素材管理--->新增/修改素材 确定按钮 触发新增/修改素材 方法
## src/mixins/src/txtImgAudVideoInitDataMixin.js 
   后台--->营销--->素材管理--->新增/修改素材 保存（文本，图片，语音，视频，小程序，图文链接）内容到【modifyObj】对象中
## src/pages/system/deviceManage/materialManage.vue
   后台--->营销--->素材管理--->素材管理左侧  (新增/编辑/删除/上移/下移素材) （内有mManageMessageAdd.vue组件）
## src/pages/system/deviceManage/components/mManageMessageAdd.vue
   后台--->营销--->素材管理--->新增模板消息的 标题，消息类型，可点击取消和确定 组件(内有messageOfTxtImgAudVido.vue组件)
   微客服--->推送消息适用
## src/components/common/messageAdd/components/messageOfTxtImgAudVido.vue
   后台--->营销--->素材管理--->新增模板消息/推送消息内 文本，图片，视屏，小程序，图文链接 组件
   微客服--->推送消息适用
## src/pages/chat/home/components/pushAssistant/newMessage/components/readPush.vue
   微客服--->一键推送页面
## src/components/common/materialManageGroup.vue
   模板消息

```
SCRM-Admin-Web
├─ .babelrc
├─ .editorconfig
├─ .eslintignore
├─ .eslintrc.js
├─ .git
│    ├─ HEAD
│    ├─ config
│    ├─ description
│    ├─ hooks
│    │    ├─ applypatch-msg.sample
│    │    ├─ commit-msg.sample
│    │    ├─ fsmonitor-watchman.sample
│    │    ├─ post-update.sample
│    │    ├─ pre-applypatch.sample
│    │    ├─ pre-commit.sample
│    │    ├─ pre-push.sample
│    │    ├─ pre-rebase.sample
│    │    ├─ pre-receive.sample
│    │    ├─ prepare-commit-msg.sample
│    │    └─ update.sample
│    ├─ index
│    ├─ info
│    │    └─ exclude
│    ├─ logs
│    │    ├─ HEAD
│    │    └─ refs
│    ├─ objects
│    │    ├─ info
│    │    └─ pack
│    ├─ packed-refs
│    └─ refs
│           ├─ heads
│           ├─ remotes
│           └─ tags
├─ .gitignore
├─ .postcssrc.js
├─ README.md
├─ build
│    ├─ build.js
│    ├─ check-versions.js
│    ├─ logo.png
│    ├─ utils.js
│    ├─ vue-loader.conf.js
│    ├─ webpack.base.conf.js
│    ├─ webpack.dev.conf.js
│    └─ webpack.prod.conf.js
├─ config
│    ├─ dev.env.js
│    ├─ index.js
│    ├─ prod.env.js
│    └─ test.env.js
├─ index.html
├─ package-lock.json
├─ package.json
├─ src
│    ├─ App.vue
│    ├─ README.MD
│    ├─ api
│    │    ├─ chat
│    │    ├─ common
│    │    ├─ oms
│    │    ├─ pms
│    │    ├─ sms
│    │    └─ system
│    ├─ assets
│    │    ├─ css
│    │    ├─ images
│    │    └─ js
│    ├─ components
│    │    ├─ README.MD
│    │    ├─ common
│    │    ├─ libs
│    │    └─ packages
│    ├─ config
│    │    └─ index.js
│    ├─ constants
│    │    ├─ chat
│    │    ├─ index.js
│    │    ├─ messageType
│    │    ├─ radio
│    │    ├─ resource
│    │    └─ selects
│    ├─ directives
│    │    ├─ index.js
│    │    └─ src
│    ├─ filters
│    │    ├─ index.js
│    │    └─ src
│    ├─ indexDB
│    │    ├─ chat
│    │    ├─ config.js
│    │    └─ indexedDB.js
│    ├─ main.js
│    ├─ mixins
│    │    ├─ index.js
│    │    └─ src
│    ├─ pages
│    │    ├─ README.MD
│    │    ├─ chat
│    │    ├─ common
│    │    ├─ login
│    │    ├─ main.vue
│    │    ├─ oms
│    │    ├─ pms
│    │    ├─ sms
│    │    ├─ system
│    │    └─ ui
│    ├─ plugins
│    │    ├─ index.js
│    │    └─ src
│    ├─ router
│    │    ├─ chat
│    │    ├─ common
│    │    ├─ index.js
│    │    ├─ login
│    │    ├─ oms
│    │    ├─ pms
│    │    ├─ router.js
│    │    ├─ sms
│    │    ├─ system
│    │    └─ ui
│    ├─ storage
│    │    ├─ cookie
│    │    ├─ index.js
│    │    ├─ local
│    │    └─ session
│    ├─ store
│    │    ├─ actions.js
│    │    ├─ getters.js
│    │    ├─ index.js
│    │    ├─ mutation-types.js
│    │    ├─ mutations.js
│    │    └─ state.js
│    └─ utils
│           ├─ index.js
│           ├─ logger.js
│           ├─ src
│           └─ validator.js
├─ static
│    ├─ .gitkeep
│    ├─ images
│    │    ├─ favicon.png
│    │    ├─ login-logo@2x.png
│    │    └─ system-logo@2x.png
│    ├─ js
│    │    ├─ amr
│    │    ├─ canvas2image
│    │    ├─ ckeditor
│    │    ├─ echarts
│    │    ├─ recorder
│    │    ├─ rsa
│    │    └─ txWebIM
│    └─ sound
│           └─ msg.mp3
└─ test
       ├─ e2e
       │    ├─ custom-assertions
       │    ├─ nightwatch.conf.js
       │    ├─ runner.js
       │    └─ specs
       └─ unit
              ├─ .eslintrc
              ├─ jest.conf.js
              ├─ setup.js
              └─ specs
```