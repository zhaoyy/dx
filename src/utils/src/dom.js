export const addClass = (elem, cls) => {
  if (!elem) {
    return
  }
  if (!hasClass(elem, cls)) {
    elem.className = !elem.className ? cls : elem.className + ' ' + cls
  }
}

export const removeClass = (elem, cls) => {
  if (!elem) {
    return
  }
  if (hasClass(elem, cls)) {
    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, '') + ' '
    while (newClass.indexOf(' ' + cls + ' ') >= 0) {
      newClass = newClass.replace(' ' + cls + ' ', ' ')
    }
    elem.className = newClass.trim()
  }
}

export const hasClass = (elem, cls) => {
  if (!elem) {
    return
  }
  cls = cls || ''
  // 当cls没有参数时，返回false
  if (cls.replace(/\s/g, '').length === 0) return false
  return new RegExp(' ' + cls + ' ').test(' ' + elem.className + ' ')
}
