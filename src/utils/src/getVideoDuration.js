export default async (url) => {
  // 推送给好友时，视频需要获取时长
  let node = document.createElement('video')
  node.src = url
  let duration = await new Promise((resolve) => {
    node.oncanplaythrough = function() {
      resolve(this.duration)
    }
  })
  node = null
  return duration
}
