// 数据脱敏
export default (value, beginIndex = 3, size = 4) => {
  if (value) {
    value = String(value)
    let reg = new RegExp(`(.{1,${beginIndex}}).{0,${size}}(.*)`)
    return value.replace(reg, `$1${Array(size).fill('*').join('')}$2`)
  }
  return value
}
