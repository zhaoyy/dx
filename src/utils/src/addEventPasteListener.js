/**
 * 获取剪贴板的图片
 * @param {[type]} e 事件元素对象
 */
const addEventPasteListener = async function(e) {
  let cbd = e.clipboardData
  let ua = window.navigator.userAgent
  if (cbd.items && cbd.items.length === 2 && cbd.items[0].kind === 'string' && cbd.items[1].kind === 'file' &&
    cbd.types && cbd.types.length === 2 && cbd.types[0] === 'text/plain' && cbd.types[1] === 'Files' &&
    ua.match(/Macintosh/i) && Number(ua.match(/Chrome\/(\d{2})/i)[1]) < 49) {
    return
  }
  let arr = Object.keys(cbd.items)
  if (arr.length === 0) return
  // 获取图片
  let item, i, blob
  for (i = 0; i < cbd.items.length; i++) {
    item = cbd.items[i]
    if (item.kind === 'file') {
      blob = item.getAsFile()
      if (blob.size === 0) {
        return true
      }
    } else {
      return new Promise((resolve, reject) => {
        item.getAsString(str => {
          resolve({
            type: 'string',
            string: str
          })
        })
      })
    }
  }
  if (!blob) return
  var reader = new FileReader()
  // var imgs = new Image()
  // imgs.file = blob
  // reader.onload = (function(aImg) {
  //   return function(e) {
  //     aImg.src = e.target.result
  //   }
  // })(imgs)
  reader.readAsDataURL(blob)
  return new Promise((resolve, reject) => {
    reader.onload = function (e) {
      resolve({
        base64: this.result,
        size: blob.size,
        type: blob.type,
        blob: blob
      })
    }
  })
}

export default addEventPasteListener
