class MyNotification {
  constructor(options) {
    Notification.requestPermission()
  }

  notifications = []

  showMaxLength = 3

  tag = 0

  /**
   * 显示通知
   * @return {[type]} [description]
   */
  showNotification({
    title = '标题',
    body = '消息',
    icon = '图片',
    tag
  } = {}) {
    // 检查权限
    Notification.requestPermission().then((permission) => {
      if (permission !== 'granted') return
      if (!tag) {
        tag = (this.tag++).toString()
      }
      // let tag = Math.floor(Math.random()*100)
      let notification = new Notification(title, {
        body,
        icon,
        tag
      })
      let that = this
      notification.onclose = function() {
        // 删除数组里面当前关闭的对象
        that.notifications.forEach((val, index) => {
          if (val.tag === tag) {
            that.notifications.splice(index, 1)
            return false
          }
        })
      }
      notification.onshow = function() {
        if (that.notifications.length === that.showMaxLength) {
          // 关闭最久之前的的一条
          that.notifications[0].close()
        }
        that.notifications.push(this)
      }
    })
  }
}

export default MyNotification
