import Vue from 'vue'
import date from 'element-ui/src/utils/date'

const watchDateRangeToTimestamp = (modelOrFn, beginName, endName) => {
  return function (val) {
    val = val || []
    let begin = val[0]
    let end = val[1]
    let model = modelOrFn
    if (typeof model === 'function') {
      model = modelOrFn()
    }
    Vue.set(model, beginName, begin ? begin.getTime() : '')
    Vue.set(model, endName, end ? end.getTime() : '')
  }
}

const watchDateToTimestamp = (modelOrFn, dateName) => {
  return function (val) {
    let model = modelOrFn
    if (typeof model === 'function') {
      model = modelOrFn()
    }
    Vue.set(model, dateName, val ? val.getTime() : '')
  }
}

// 时间精确到天
/*
const accurateTodate = date => {
  date.setHours(0)
  date.setMinutes(0)
  date.setSeconds(0)
  date.setMilliseconds(0)
  return date
}
*/
// 时间精确到天
const accurateTodate = (date, h = 0, m = 0, s = 0, ms = 0) => {
  date.setHours(h)
  date.setMinutes(m)
  date.setSeconds(s)
  date.setMilliseconds(ms)
  return date
}

// 得到区间的两个日期
const getRangeDate = (range = 7, accToDate = true) => {
  let now = new Date()
  let then = new Date()
  then.setDate(then.getDate() - range)
  if (accToDate) {
    now = accurateTodate(now)
    then = accurateTodate(then)
  }
  return {
    now,
    then
  }
}

// 将日期时间转换成[xxx前]的文本显示
// 1小时内显示 xx分钟前
// 1小时-24小时内显示 xx小时前
// 1天-30天内显示 xx天前
// 1月-12月内显示 xx月前
// 大于1年显示 xx年前
const timeMinute = 60 * 1000
const timeHour = timeMinute * 60
const timeDate = timeHour * 24
const timeMonth = timeDate * 30
const timeYear = timeMonth * 12
const rangeMsg = [{
  time: {
    begin: 0,
    end: timeMinute
  },
  div: 0,
  label: '刚刚'
}, {
  time: {
    begin: timeMinute,
    end: timeHour
  },
  div: timeMinute,
  label: '分钟前'
}, {
  time: {
    begin: timeHour,
    end: timeDate
  },
  div: timeHour,
  label: '小时前'
}, {
  time: {
    begin: timeDate,
    end: timeMonth
  },
  div: timeDate,
  label: '天前'
}, {
  time: {
    begin: timeMonth,
    end: timeYear
  },
  div: timeMonth,
  label: '月前'
}, {
  time: {
    begin: timeYear,
    end: Infinity
  },
  div: timeYear,
  label: '年前'
}]
const dateAgo = timestamp => {
  let now = Date.now()
  let diff = now - timestamp

  for (let i = 0, l = rangeMsg.length; i < l; i++) {
    let v = rangeMsg[i]
    if (v.time.begin < diff && v.time.end > diff) {
      if (v.div === 0) {
        return v.label
      }
      return `${Math.floor(diff / v.div)}${v.label}`
    }
  }
  return ''
}

// n:当前时间前几天  date:当前时间
const getBeforeDate = (n = 0, date) => {
  // 60*60*24*1000 = 86400000 / 天
  let currentDate = new Date()
  if (date) {
    currentDate = new Date(date)
  }
  let timeLen = 86400000 * n
  let time = currentDate - timeLen
  return time
}

// 当前时间前几天
// const getBeforeDate = (n) => {
//   var d = new Date()
//   var year = d.getFullYear()
//   var mon = d.getMonth() + 1
//   var day = d.getDate()
//   var hours = d.getHours()
//   var minutes = d.getMinutes()
//   var seconds = d.getSeconds()
//   if (day <= n) {
//     if (mon > 1) {
//       mon = mon - 1
//     } else {
//       year = year - 1
//       mon = 12
//     }
//   }
//   d.setDate(d.getDate() - n)
//   year = d.getFullYear()
//   mon = d.getMonth() + 1
//   day = d.getDate()
//   var s = year + '-' + mon + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
//   return new Date(s)
// }

// 获取当天时间0点到24点的时间戳，精度到分
const getTodayDate = (hour = 0, minute = 0) => {
  let newData = new Date()
  // 今天0点
  let dateStr = newData.toLocaleDateString()
  if (hour > 23) {
    hour = 23
  }
  if (minute > 59) {
    minute = 59
  }
  if (hour > 0 || minute > 0) {
    // 时
    dateStr += ' ' + (hour > 9 ? hour : ('0' + hour))
    // 分
    if (minute > 0) {
      dateStr += ':' + (minute > 9 ? minute : ('0' + minute))
    } else {
      dateStr += ':00'
    }
    // 秒
    dateStr += ':00'
  }
  let value = new Date(new Date(dateStr)).getTime()
  return value
}

const getBeforeNumTimeStramp = (n, zero = true) => {
  let newData = new Date()
  let year = newData.getFullYear()
  let month = newData.getMonth()
  let day = newData.getDate()
  let hor = zero ? 0 : newData.getHours()
  let min = zero ? 0 : newData.getMinutes()
  let sec = zero ? 0 : newData.getSeconds()
  let today = new Date(year, month, day, hor, min, sec, 0).getTime()
  n = n || 0
  let marTime = n * 24 * 3600 * 1000
  let step = n === 0 ? 1000 : 0
  return today - marTime - step
}

// 获取近n天时间间隔的有效时间选择区间,n--至少为1
const getDateZone = (n = 1, data, skey, ekey) => {
  function getStarOrEndTime(time, type) {
    let timeVal = new Date(time)
    if (type === 'start') {
      return new Date(timeVal.getFullYear(), timeVal.getMonth(), timeVal.getDate(), 0, 0, 0, 0).getTime()
    } else {
      return new Date(timeVal.getFullYear(), timeVal.getMonth(), timeVal.getDate(), 23, 59, 59, 999).getTime()
    }
  }

  let cSTime = getStarOrEndTime(new Date().getTime(), 'start')
  let cETime = getStarOrEndTime(new Date().getTime(), 'end')
  let startTime = getStarOrEndTime(data[skey], 'start')
  let endTime = getStarOrEndTime(data[ekey], 'end')
  let marTime = endTime - startTime - n * 24 * 3600 * 1000
  let sTime
  let eTime
  if (startTime <= cSTime && cSTime <= endTime) {
    // 当前时间--在活动区间内
    let martime = cETime - startTime - n * 24 * 3600 * 1000
    if (martime > 0) {
      // 当前时间24点与活动开始时间间隔大于n天
      sTime = cETime - n * 24 * 3600 * 1000
    } else {
      // 当前时间24点与活动开始时间间隔小于n天
      sTime = startTime
    }
    eTime = cETime
  } else {
    // 当前时间大于活动结束时间
    if (marTime > 0) {
      sTime = endTime - n * 24 * 3600 * 1000
    } else {
      sTime = startTime
    }
    eTime = endTime
  }
  let timeObj = {}
  timeObj[skey] = sTime
  timeObj[ekey] = eTime
  return timeObj
}

// 把时间戳转为时间
// 例如900000 => 00:15
const stampToTime = (stamp) => {
  let day = Math.floor(stamp / 1000 / 60 / 60 / 24) + ''
  let hour = Math.floor(stamp / 1000 / 60 / 60 % 24) + ''
  let minute = Math.floor(stamp / 1000 / 60 % 60) + ''
  let second = Math.floor(stamp / 1000 % 60) + ''
  if (hour < 10) {
    hour = '0' + hour
  }
  if (minute < 10) {
    minute = '0' + minute
  }
  if (second < 10) {
    second = '0' + second
  }
  return {
    day,
    hour,
    minute,
    second
  }
}

const timeLongFmt = (stamp, type = 1) => {
  let value = stamp || 0
  let timeLongObj = stampToTime(value)
  let timeString = ''
  if (type === 1) {
    // 只显示有值的时间
    timeString += timeLongObj.day === '0' ? '' : (timeLongObj.day + '天')
    timeString += timeLongObj.hour === '00' ? '' : (timeLongObj.hour + '小时')
    timeString += timeLongObj.minute === '00' ? '' : (timeLongObj.minute + '分')
    timeString += timeLongObj.second === '00' ? '' : (timeLongObj.second + '秒')
  } else if (type === 2) {
    // 最大值之后所有值都显示
    timeString += timeLongObj.day === '0' ? '' : (timeLongObj.day + '天')
    timeString += (timeLongObj.day === '0' && timeLongObj.hour === '00') ? '' : (timeLongObj.hour + '小时')
    timeString += (timeLongObj.day === '0' && timeLongObj.hour === '00' && timeLongObj.minute === '00') ? '' : (timeLongObj.minute + '分')
    timeString += (timeLongObj.day === '0' && timeLongObj.hour === '00' && timeLongObj.minute === '00' && timeLongObj.second === '00') ? '' : (timeLongObj.second + '秒')
  } else if (type === 3) {
    // 显示完整值
    timeString += timeLongObj.day + '天'
    timeString += timeLongObj.hour + '小时'
    timeString += timeLongObj.minute + '分'
    timeString += timeLongObj.second + '秒'
  }
  return timeString || '0'
}

/**
 * 获取当月一号时间
 */
const currentMonthOne = () => {
  const currentDate = new Date()
  return new Date(currentDate.getFullYear(), currentDate.getMonth(), 1)
}

const strToTimestamp = (param) => {
  let date = new Date(param)
  let Y = date.getFullYear() + '-'
  let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-'
  let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' '
  let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':'
  let m = date.getMinutes() < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':'
  let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
  return Y + M + D + h + m + s
}

export {
  getTodayDate,
  date,
  dateAgo,
  watchDateRangeToTimestamp,
  watchDateToTimestamp,
  getRangeDate,
  getBeforeDate,
  getDateZone,
  getBeforeNumTimeStramp,
  stampToTime,
  timeLongFmt,
  accurateTodate,
  currentMonthOne,
  strToTimestamp
}
