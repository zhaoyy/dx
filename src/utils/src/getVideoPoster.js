export default (video, scale = 1) => {
  return new Promise((resolve, reject) => {
    let videoDom = document.createElement('video')
    videoDom.setAttribute('crossorigin', 'anonymous')

    function videoLoadeddata() {
      const canvas = document.createElement('canvas')
      canvas.width = videoDom.videoWidth * scale
      canvas.height = videoDom.videoHeight * scale
      canvas.getContext('2d').drawImage(videoDom, 0, 0, canvas.width, canvas.height)
      let dataURL = canvas.toDataURL('image/png')
      videoDom.removeEventListener('loadeddata', videoLoadeddata)
      videoDom = null
      if (/^data:,$/i.test(dataURL)) {
        reject(new Error('错误的base64'))
      } else {
        resolve(dataURL)
      }
    }

    function videoError() {
      videoDom.removeEventListener('error', videoError)
      videoDom = null
      reject(new Error('视频无法加载'))
    }

    videoDom.addEventListener('loadeddata', videoLoadeddata)
    videoDom.addEventListener('error', videoError)
    videoDom.src = video
  })
}
