import {
  Message
} from 'element-ui'
// 数值未超过99999时数字全部显示，超过99999时显示00.00+w
const toFixedW = (num) => {
  if (!num) {
    return 0
  }
  if (num > 99999) {
    num = num / 10000
    if (num % 1 === 0) {
      return num + 'w'
    } else {
      return num.toFixed(2) + 'w'
    }
  } else {
    return num
  }
}
const formatSeconds = (val) => {
  let secondTime = parseInt(val) // 秒
  let minuteTime = 0 // 分
  let hourTime = 0 // 小时
  let result = ''
  if (secondTime > 60) { // 如果秒数大于60，将秒数转换成整数
    // 获取分钟，除以60取整数，得到整数分钟
    minuteTime = parseInt(secondTime / 60)
    // 获取秒数，秒数取佘，得到整数秒数
    secondTime = parseInt(secondTime % 60)
    // 如果分钟大于60，将分钟转换成小时
    if (minuteTime > 60) {
      // 获取小时，获取分钟除以60，得到整数小时
      hourTime = parseInt(minuteTime / 60)
      // 获取小时后取佘的分，获取分钟除以60取佘的分
      minuteTime = parseInt(minuteTime % 60)
    }
  }
  if (secondTime > 0) {
    result = secondTime + '秒'
  }
  if (minuteTime > 0) {
    result = minuteTime + '分' + result
  }
  if (hourTime > 0) {
    result = hourTime + '小时' + result
  }
  return result
}
// el:$event  format:文件格式
const fileUpload = (el, format = ['xls', 'xlsx'], elSize = 10) => {
  el.preventDefault() // 取消默认行为
  let file = el.target.files[0]
  el.target.value = null
  let arr = file.name.split('.')
  let isEtc = format.some((item) => {
    return arr[1] === item
  })
  if (!isEtc) {
    Message({
      message: '文件格式错误！',
      type: 'warning',
      duration: 3000
    })
    return null
  } else {
    let size = file.size // byte
    size = size / 1024 // kb
    size = (size / 1024).toFixed(3) // mb
    if (size > elSize) {
      Message({
        message: `文件格式大小不能超过${elSize}M，当前文件大小为${size}M！`,
        type: 'warning',
        duration: 3000
      })
      return
    }
    let formData = new FormData()
    formData.append('file', file)
    return {
      formData: formData,
      file: file
    }
  }
}

export {
  toFixedW,
  formatSeconds,
  fileUpload
}
