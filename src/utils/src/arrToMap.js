export default (arr, key = 'id') => {
  const map = {}
  arr.forEach(v => {
    if (!map[v[key]]) {
      map[v[key]] = v
    }
  })
  return map
}
