import getVideoPoster from './src/getVideoPoster'
import changeFileSize from './src/changeFileSize'
import addEventPasteListener from './src/addEventPasteListener'
import formatFloat from './src/formatFloat'
import qrCodeGetBase64Src from './src/qrCodeGetBase64Src'
import MyNotification from './src/myNotification'
import provinceArea from './src/areaList'
import arrToMap from './src/arrToMap'
import drug from './src/drug'
import deepCopy from './src/deepCopy'
import getVideoDuration from './src/getVideoDuration'
import userAgent from './src/userAgent'

export * from './src/date'
export * from 'vma-vue-assist/dist/static/js/utils'
export * from './src/numToFixe'
export * from './src/router'
export * from './src/echarts'
export * from './src/dom'
// export * from './src/Aes'

export {
  getVideoPoster,
  changeFileSize,
  addEventPasteListener,
  qrCodeGetBase64Src,
  MyNotification,
  formatFloat,
  provinceArea,
  arrToMap,
  drug,
  deepCopy,
  getVideoDuration,
  userAgent

}
