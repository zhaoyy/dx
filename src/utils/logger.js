import {
  isDev,
  isTest
} from '@/config'
import userAgent from './src/userAgent'

const extendLog = (logFn) => {
  return function () {
    if (isDev || isTest) {
      if (userAgent() !== 'IE' && window.console) {
        logFn.apply(window, arguments)
      }
    }
  }
}

const windowConsole = window.console
class Logger {
  static log = extendLog(windowConsole.log)
  static debug = extendLog(windowConsole.debug)
  static info = extendLog(windowConsole.info)
  static warn = extendLog(windowConsole.warn)
  static error = extendLog(windowConsole.error)
}

export default Logger
