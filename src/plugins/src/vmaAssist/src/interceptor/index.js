import Loader from './src/loader'
import handleError from './src/handleError'
import {
  macKeyCookie
} from '@/storage'
import Qs from 'qs'

export const paramsSerializer = params => {
  return Qs.stringify(params, {
    arrayFormat: 'brackets'
  })
}

export default {
  errorHandle: {
    handleError(error) {
      handleError(error)
      return Promise.reject(error)
    }
  },
  loading: {
    showLoader() {
      Loader.show()
    },
    hideLoader() {
      Loader.hide()
    }
  },
  authorization: {
    getMacKey() {
      return macKeyCookie.get() || ''
    },
    compatUrl(url, params) {
      params = paramsSerializer(params)
      url += (url.indexOf('?') === -1 ? '?' : '&') + params
      return url
    }
  },
  returnResponseData: true
}
