import vmaAssist, {
  utils
} from 'vma-vue-assist'
import interceptor, {
  paramsSerializer
} from './src/interceptor'
import {
  api,
  isDev
} from '@/config'

export default {
  install(Vue, opts = {}) {
    // document
    // https://coding.net/s/ffeab740-54c4-4d30-a231-3902175af22c
    Vue.use(vmaAssist, {
      debug: isDev,
      plugins: {
        axios: {
          defaults: {
            baseURL: api,
            timeout: 120000,
            paramsSerializer
          },
          interceptor
        }
      }
    })
    Vue.prototype.fmt = utils.fmt
  }
}
