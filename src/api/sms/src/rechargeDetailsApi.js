import axios from 'axios'
const API_MODULE_V = `/super/v1.0`

export default {
  /**
   * 获取短信充值记录
   * @param {*} params
   */
  getSmsRechargeList(params) {
    return axios({
      url: `${API_MODULE_V}/msg/recharge`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 充值明细
   * @param {*} params
   */
  getSmsRechargeDetails(params) {
    return axios({
      url: `${API_MODULE_V}/msg/recharge/detail`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取短信发送记录
   * @param {*} params
   */
  getSmsSendList(params) {
    return axios({
      url: `${API_MODULE_V}/send/log`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  // 红包明细
  /**
   * 获取红包领取记录列表
   * @param {*} params
   */
  getPacketsReceiveList(params) {
    return axios({
      url: `${API_MODULE_V}/redpage/get`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取晒图活动记录列表
   * @param {*} params
   */
  getShowPictureList(params) {
    return axios({
      url: `${API_MODULE_V}/redpage/pic`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取红包充值记录列表
   * @param {*} params
   */
  getPacketsRechargeList(params) {
    return axios({
      url: `${API_MODULE_V}/redpage/recharge`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取红包充值-充值明细
   * @param {*} params
   */
  getPacketsRechargeDetail(params) {
    return axios({
      url: `${API_MODULE_V}/redpage/recharge/detail`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  }
}
