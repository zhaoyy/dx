import axios from 'axios'
const API_MODULE_V = `/super/v1.0`

export default {
  /**
   * 获取短信模板列表
   * @param {*} params
   */
  getSmsTemplateList(params) {
    return axios({
      url: `${API_MODULE_V}/template/`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 模版审核
   * @param {*} params
   */
  templateAudit(params) {
    return axios({
      url: `${API_MODULE_V}/template/`,
      method: 'PUT',
      data: params
    })
  }
}
