import axios from 'axios'
const API_MODULE_V = `/super/v1.0`

export default {
  /**
   * 企业列表-企业列表
   * @param {*} params
   */
  getEnterList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/enter`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 企业列表-个人号列表
   * @param {*} params
   */
  getEnterAccount(params) {
    return axios({
      url: `${API_MODULE_V}/customer/enter/account`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 企业列表--设备列表
   * @param {*} params
   */
  getEnterEuq(params) {
    return axios({
      url: `${API_MODULE_V}/customer/enter/euq`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 企业列表--店铺列表
   * @param {*} params
   */
  getEnterShop(params) {
    return axios({
      url: `${API_MODULE_V}/customer/enter/shop`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 企业列表--企业详情
   * @param {*} params
   */
  getEnterDetails(params) {
    return axios({
      url: `${API_MODULE_V}/customer/enter/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 获取代理商列表
   * @param {*} params
   */
  getOmsList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商--详情
   * @param {*} params
   */
  getOmsListDetail(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/${params}`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 获取代理商--企业列表
   * @param {*} params
   */
  getOmsListEnterpriseList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/enterprise`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商--设备列表
   * @param {*} params
   */
  getOmsListEquipList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/euq`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商--店铺列表
   * @param {*} params
   */
  getOmsListShopList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/shop`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商--个人号列表
   * @param {*} params
   */
  getOmsListAccountList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/account`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  }
}
