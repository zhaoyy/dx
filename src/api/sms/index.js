import smsTemplateApi from './src/smsTemplateApi'
import rechargeDetailsApi from './src/rechargeDetailsApi'
import systemSettingsApi from './src/systemSettingsApi'
import oemListApi from './src/oemListApi'
import customerManageApi from './src/customerManageApi'
import homeApi from './src/homeApi'

export {
  smsTemplateApi,
  rechargeDetailsApi,
  systemSettingsApi,
  oemListApi, // 客户管理-贴牌商列表
  customerManageApi,
  homeApi
}
