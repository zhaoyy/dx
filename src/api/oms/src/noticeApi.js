import axios from 'axios'
const API_MODULE_V = `/oem/v1.0`

export default {
  /**
   * 获取消息通知列表
   * @param {*} params
   */
  getNoticeList(params) {
    return axios({
      url: `${API_MODULE_V}/index/notification`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取消息通知详情
   * @param {*} params
   */
  getNoticeDetails(params) {
    return axios({
      url: `${API_MODULE_V}/index/notification/${params}`,
      method: 'GET'
    })
  },
  /**
   * 新增消息
   * @param {*} params
   */
  addNotice(params) {
    return axios({
      url: `${API_MODULE_V}/index/notification`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 获取企业下拉框
   * @param {*} params
   */
  getEnterList() {
    return axios({
      url: `${API_MODULE_V}/index/notification/enter`,
      method: 'GET'
    })
  },
  /**
   * 获取代理商下拉框
   * @param {*} params
   */
  getProxyList() {
    return axios({
      url: `${API_MODULE_V}/index/notification/proxy`,
      method: 'GET'
    })
  }
}
