import axios from 'axios'
const API_MODULE_V = `/oem/v1.0`

export default {
  /**
   * 获取代理商列表
   * @param {*} params
   */
  getAgentList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商列表--详情
   * @param {*} params
   */
  getAgentListDetail(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/${params}`,
      method: 'GET'
    })
  },
  /**
   * 获取代理商列表--企业列表
   * @param {*} params
   */
  getAgentListEnterpriseList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/enterprise`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商列表--设备列表
   * @param {*} params
   */
  getAgentListEquipList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/euq`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商列表--店铺列表
   * @param {*} params
   */
  getAgentListShopList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/shop`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商列表--个人号列表
   * @param {*} params
   */
  getAgentListAccountList(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/account`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商列表--可用设备数
   * @param {*} params
   */
  getAgentListEquipNum(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/equ/num`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商列表--新增设备
   * @param {*} params
   */
  getAgentListEquipNumAdd(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/equ`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 获取代理商列表--可用店铺数
   * @param {*} params
   */
  getAgentListShopNum(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/shop/num`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商列表--新增店铺
   * @param {*} params
   */
  getAgentListShopNumAdd(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/shop`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 获取代理商列表--可用个人号数
   * @param {*} params
   */
  getAgentListNum(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/account/num`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商列表--可用个人号数
   * @param {*} params
   */
  getAgentListNumAdd(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/account`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 获取代理商列表--可用账号数
   * @param {*} params
   */
  getAgentListAccountNum(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/shop/enter`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商列表--可用账号数新增
   * @param {*} params
   */
  getAgentListAccountNumAdd(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/enter`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 获取代理商列表--可用企业数
   * @param {*} params
   */
  getAgentListEnterNum(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/shop/enter`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取代理商列表--禁用/启用
   * @param {*} params
   */
  getAgentListProhibit(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/isable`,
      method: 'PUT',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 获取代理商列表--重置密码
   * @param {*} params
   */
  getAgentListResetPwd(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/reset`,
      method: 'PUT',
      removeEmpty: true,
      data: params
    })
  },
  /**
   * 获取代理商列表--代理商详情-编辑用
   * @param {*} params
   */
  getAgentListDetailEdit(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/edit/${params}`,
      method: 'GET'
    })
  },
  /**
   * 获取代理商列表--新增代理商
   * @param {*} params
   */
  getAgentListAgentAdd(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 获取代理商列表--编辑代理商
   * @param {*} params
   */
  getAgentListAgentModify(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 可用工作群
   *
   * @param {*} params
   * @returns
   */
  groupNum(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/group/num`,
      method: 'GET'
    })
  },
  /**
   * 新增群
   * @param {*} params
   */
  enterAddGroup(params) {
    return axios({
      url: `${API_MODULE_V}/customer/proxy/group`,
      method: 'POST',
      removeEmpty: true,
      data: params
    })
  }
}
