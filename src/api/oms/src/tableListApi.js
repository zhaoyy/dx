import axios from 'axios';
const API_MODULE_V = `/oem/v1.0/oms/wechat`
const API_EVERY = `/oem/v1.0/oms/every`
export default {
    /**
     * @params 设备统计
     */
    getEqCount(params) {
        return axios({
            url: `${API_MODULE_V}/eqCount`,
            method: 'GET',
            params: params
        })
    },
    /**
     * @params
     */
    /**
     * @params 微信加粉量
     */
    getAddFansCount(params) {
        return axios({
            url: `${API_MODULE_V}/addFansCount`,
            method: 'GET',
            params: params
        })
    },
    /**
     * @params 人均加粉量
     */
    getAddFansOwnCount(params) {
        return axios({
            url: `${API_MODULE_V}/addFansOwnCount`,
            method: 'GET',
            params: params
        })
    },
    /**
     * @params 服务对话量
     */
    getStaffTalkCount(params) {
        return axios({
            url: `${API_MODULE_V}/staffTalkCount`,
            method: 'GET',
            params: params
        })
    },
    /**
     * @params 客户主动进线量
     */
    getJdByStaffCount(params) {
        return axios({
            url: `${API_MODULE_V}/jdbyStaffCount`,
            method: 'GET',
            params: params
        })
    },
    /**
     * @params 非工作日
     */
    getStaffCountNoDay(params) {
        return axios({
            url: `${API_MODULE_V}/staffCountNoDay`,
            method: 'GET',
            params: params
        })
    },
    /**
     * @params 设备统计-导出
     */
    eqExport() {
        return axios({
            url: `${API_MODULE_V}/eqExport`,
            method: 'GET',
            responseType: 'blob',
            // params: params
        })
    },
    /**
     * @params 对话统计-导出
     */
    staffTalkCountExport() {
        return axios({
            url: `${API_MODULE_V}/staffTalkCountExport`,
            method: 'GET',
            responseType: 'blob',
            // params: params
        })
    },
    /**
     * @params 微信加粉统计-导出
     */
    addFansCountExport() {
        return axios({
            url: `${API_MODULE_V}/addFansCountExport`,
            method: 'GET',
            responseType: 'blob',
            // params: params
        })
    },

    /**
     * @params 非工作时间-导出
     */
    staffCountNoDayExport() {
        return axios({
            url: `${API_MODULE_V}/staffCountNoDayExport`,
            method: 'GET',
            responseType: 'blob',
        })
    },
    /**
     * @params 昨日加微量
     */
    addWechatYes(params) {
        return axios({
            url: `${API_EVERY}/addWechatYes`,
            method: 'GET',
            params: params
        })
    },
    /**
     * @params 昨日加微量--导出
     */
    addWechatYesExport() {
        return axios({
            url: `${API_EVERY}/addWechatYesExport`,
            method: 'GET',
            responseType: 'blob',
        })
    },
    /**
     * @params 昨日对话量--导出
     */
    staffTalkCountYesExport() {
        return axios({
            url: `${API_EVERY}/staffTalkCountYesExport`,
            method: 'GET',
            responseType: 'blob',
        })
    },
    /**
     * @params 昨日对话量
     */
    staffTalkCountYes(params) {
        return axios({
            url: `${API_EVERY}/staffTalkCountYes`,
            method: 'GET',
            params: params
        })
    }


}