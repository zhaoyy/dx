import axios from 'axios'
const removeEmpty = true

export default {
  /**
   * 企业数曲线图
   * @returns
   */
  getEnterpriseMessage(params) {
    return axios({
      url: '/oem/v1.0/index/home/enterprise/message',
      method: 'GET',
      params
    })
  },
  /**
   * 设备数据
   * @returns
   */
  getEquipmentMessage(params) {
    return axios({
      url: '/oem/v1.0/index/home/equipment/message',
      method: 'GET',
      params
    })
  },
  /**
   * 授权店铺
   * @returns
   */
  getShopAccredit(params) {
    return axios({
      url: '/oem/v1.0/index/home/shop/accredit',
      method: 'GET',
      params
    })
  },
  /**
   * 个人号数据
   * @returns
   */
  getWeChatMessage( params) {
    return axios({
      url: '/oem/v1.0/index/home/wet_chat/message',
      method: 'GET',
      params
    })
  },
  /**
   * 个人号总数
   * @returns
   */
  getWeChatTotal() {
    return axios({
      url: '/oem/v1.0/index/home/wet_chat/total',
      method: 'GET',
      loading: false
    })
  },

  // 数据总览  card
  getDataScreen(params) {
    return axios({
      url: '/oem/v2.1/index/home/top/',
      method: 'GET',
      params,
      // removeEmpty
    })
  },
  /**
   * 首页-好友地区统计
   *
   * @param {*} params
   * @returns
   */
  getProvinceNum(params) {
    return axios({
      url: '/oem/v2.1/index/home/dashboard/user/province',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-用户关注问题
   *
   * @param {*} params
   * @returns
   */
  getUserAttentionNum(params) {
    return axios({
      url: '/oem/v2.1/index/home/dashboard/key/word',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-好友性别统计
   *
   * @param {*} params
   * @returns
   */
  getSexNum(params) {
    return axios({
      url: '/oem/v2.1/index/home/dashboard/user/sex',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-个人号统计（好友人数）
   *
   * @param {*} params
   * @returns
   */
  getfriendTotal(params) {
    return axios({
      url: '/oem/v2.1/index/home/account/statistics',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-个人号统计（收发信息）
   *
   * @param {*} params
   * @returns
   */
  getSendRecieve(params) {
    return axios({
      url: '/oem/v2.1/index/home/send/get/message',
      method: 'GET',
      params,
      removeEmpty
    })
  },

  // 筛选企业
  getEnterprises() {
    return axios({
      url: '/oem/v2.1/index/home/enterprises',
      method: 'GET'
    })
  },

}
