const applicationList = [
  {
    key: '腾讯应用宝',
    val: 'com.tencent.android.qqdownloader'
  },
  {
    key: '91手机助手',
    val: 'com.dragon.android.pandaspace'
  },
  {
    key: '安智应用商店',
    val: 'com.hiapk.marketpho'
  },
  {
    key: '应用汇',
    val: 'com.yingyonghui.market'
  },
  {
    key: 'QQ手机管家',
    val: 'com.tencent.qqpimsecure'
  },
  {
    key: '机锋应用市场',
    val: 'com.mappn.gfan'
  },
  {
    key: 'PP手机助手',
    val: 'com.pp.assistant'
  },
  {
    key: 'OPPO应用商店',
    val: 'com.oppo.market'
  },
  {
    key: 'GO市场',
    val: 'cn.goapk.market'
  },
  {
    key: '中兴应用商店',
    val: 'zte.com.market'
  },
  {
    key: '宇龙Coolpad应用商店',
    val: 'com.yulong.android.coolmart'
  },
  {
    key: '联想应用商店',
    val: 'com.lenovo.leos.appstore'
  },
  {
    key: 'cool市场',
    val: 'com.coolapk.market'
  },
  {
    key: '360手机助手',
    val: 'com.qihoo.appstore'
  },
  {
    key: '360卫士',
    val: 'com.qihoo360.mobilesafe'
  },
  {
    key: '360清理大师',
    val: 'com.qihoo.cleandroid_cn'
  },
  {
    key: '百度手机助手',
    val: 'com.baidu.appsearch'
  },
  {
    key: '豌豆荚',
    val: 'com.wandoujia.phoenix2'
  },
  {
    key: '小米应用商店',
    val: 'com.xiaomi.market'
  },
  {
    key: '华为应用商店',
    val: 'com.huawei.appmarket'
  },
  {
    key: 'R.E.管理器',
    val: 'com.speedsoftware.rootexplorer'
  },
  {
    key: '联想应用商店',
    val: 'com.lenovo.leos.appstore'
  },

  {
    key: '微 信',
    val: 'com.tencent.mm'
  }
]

export default {
  applicationList
}
