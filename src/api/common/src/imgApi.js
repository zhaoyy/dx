import axios from 'axios'

export default {
  /**
   * 获取验证码
   *
   * @returns
   */
  verification() {
    return axios({
      url: '/common/v1.0/img/verification',
      method: 'GET',
      responseType: 'json'
    })
  }
}
