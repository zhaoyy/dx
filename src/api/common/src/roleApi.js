import axios from 'axios'
import {
  handle401
} from '@/plugins/src/vmaAssist/src/interceptor/src/handleError'

export default {
  /**
   * 登录状态检查
   */
  checkStatus() {
    return axios({
      url: '/common/v1.0/role/check_status',
      method: 'GET',
      loading: false,
      errorHandle: false
    }).catch(error => {
      if (error.response && error.response.status === 401) {
        handle401(error.response.data || {})
      }
    })
  }
}
