import axios from 'axios'

export default {
  /**
   * 存储缓存
   *
   * @param {*} data
   * @returns
   */
  saveCache(data) {
    return axios({
      url: '/common/v1.0/config/cache',
      method: 'PUT',
      data
    })
  },
  /**
   * 获取缓存
   *
   * @param {*} key
   * @returns
   */
  getCache(key) {
    return axios({
      url: `/common/v1.0/config/cache/${key}`,
      method: 'GET'
    })
  },
  /**
   * 获取系统配置
   *
   * @param {*} keys
   * @param {*} enterpriseId
   * @returns
   */
  getConfig(keys, enterpriseId = 0) {
    return axios({
      url: `/common/v1.0/config/${enterpriseId}/${Array.isArray(keys) ? keys.join(',') : keys}`,
      method: 'GET'
    })
  }
}
