import imgApi from './src/imgApi'
import applicationList from './src/applicationList'
import qiniuApi from './src/qiniuApi'
import homeApi from './src/homeApi'
import commonApi from './src/commonApi'
import configApi from './src/configApi'
import roleApi from './src/roleApi'

export {
  imgApi,
  applicationList,
  qiniuApi,
  homeApi,
  commonApi,
  configApi,
  roleApi
}
