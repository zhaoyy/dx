import customerServiceApi from './src/customerServiceApi'
import chatApi from './src/chatApi'
import quickReplyApi from './src/quickReplyApi'
import commonApi from './src/commonApi'
import userInfoApi from './src/userInfoApi'
import customerOrderApi from './src/customerOrderApi'
import chatLogApi from './src/chatLogApi'
import v2CustomerServiceApi from './src/v2CustomerServiceApi'
import xunfeiApi from './src/xunfeiApi'
export {
  customerServiceApi,
  chatApi,
  quickReplyApi,
  commonApi,
  userInfoApi,
  customerOrderApi,
  chatLogApi,
  v2CustomerServiceApi,
  xunfeiApi
}
