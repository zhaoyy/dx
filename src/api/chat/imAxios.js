import axios from 'axios'
import {
  imApi
} from '@/config'
import interceptor from '@/plugins/src/vmaAssist/src/interceptor'

export default axios.createWithDefaults({
  defaults: {
    baseURL: imApi
  },
  interceptor
})
