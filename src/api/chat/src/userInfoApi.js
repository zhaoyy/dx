import axios from 'axios'
const API_MODULE = `/manager/v1.0/custom_service`

export default {
  /**
   * 获取好友用户资料
   * @return {[type]} [description]
   */
  getChatFriendUserInfo(id) {
    return axios({
      url: `${API_MODULE}/wechat_user/${id}`,
      method: 'GET',
      responseType: 'json'
    })
  },
  /**
   * 获取群聊资料
   * @return {[type]} [description]
   */
  getChatGroupInfo(id) {
    return axios({
      url: `${API_MODULE}/wechat_group/${id}`,
      method: 'GET',
      responseType: 'json'
    })
  },
  /**
   * 获取群聊成员
   * @return {[type]} [description]
   */
  getChatGroupMembers(params, loading = true, errorHandle = true) {
    return axios({
      url: `${API_MODULE}/wechat_group/user`,
      method: 'GET',
      params,
      errorHandle,
      loading,
      responseType: 'json'
    })
  },
  /**
   * 修改聊天的好友用户信息
   * @return {[type]} [description]
   */
  editChatFriendUserInfo(data, removeEmpty = false) {
    return axios({
      url: `${API_MODULE}/change/user_remark`,
      method: 'PUT',
      data,
      responseType: 'json',
      removeEmpty: false
    })
  },
  /**
   * 获取个人号的所有标签数据
   * @return {[type]} [description]
   */
  // getAccountAllLabels(id) {
  //   return axios({
  //     url: `${API_MODULE}/wechat_account/${id}`,
  //     method: 'GET',
  //     responseType: 'json'
  //   })
  // },
  /**
   * 修改好友的标签
   * @return {[type]} [description]
   */
  saveWechtFriendLabels(data) {
    return axios({
      url: `${API_MODULE}/wechat_user/label`,
      method: 'POST',
      data,
      responseType: 'json'
    })
  },
  /**
   * @好友发送接口
   */
  sendAtMsg(data) {
    return axios({
      url: `${API_MODULE}/send/atmsg`,
      method: 'POST',
      data,
      responseType: 'json'
    })
  }
}
