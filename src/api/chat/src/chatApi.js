import imAxios from '../imAxios'

export default {
  /**
   * 获取微客服对应的腾讯IM账号的userSig
   */
  getCustomerServiceUserSig(params) {
    return imAxios({
      url: `/tencentCloud/usersig`,
      method: 'GET',
      params,
      responseType: 'json'
    })
  },

  /**
   * 获取个人号最近聊天过的好友/群组列表
   */
  getPersonalAccountRecentChatsList(params) {
    return imAxios({
      url: `/messageCloud/recentFriend`,
      method: 'GET',
      params,
      responseType: 'json',
      loading: false
    })
  },
  /**
   * 获取个人号最近聊天过的好友/群组列表，集中对话
   */
  getRecentChatsListFouce(data) {
    return imAxios({
      url: `/messageCloud/recentFriendFocus`,
      method: 'POST',
      data,
      responseType: 'json',
      loading: false
    })
  },
  /**
   * 获取个人号最近好友/群聊天记录
   */
  getChatHistoryMsgList(params) {
    return imAxios({
      url: `/messageCloud/message`,
      method: 'GET',
      params,
      responseType: 'json',
      loading: false
    })
  },
  /**
   * 根据员工账号id,获取IM在线状态
   */
  getOnLineStatusByIds(data) {
    return imAxios({
      url: `/tencentCloud/accountStatus`,
      method: 'POST',
      data,
      responseType: 'json'
    })
  },
  /**
   * 转接好友，接收方/转出方微客服拉取转接信息
   *
   * @param {*} key
   * @param {*} type
   * @returns
   */
  transferFirendPull(key, type) {
    return imAxios({
      url: '/noticeCloud/pullSwitchFriendMsg',
      method: 'GET',
      params: {
        key,
        type
      },
      loading: false
    })
  }
}
