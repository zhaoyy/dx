import axios from 'axios'
const API_MODULE = `/common/v1.0`

export default {
  /**
   * 太保免登陆接
   * @params {type = 4,token：token,domain：domain}
   */
  taibaoLogin(params) {
    return axios({
      url: `${API_MODULE}/home/taibaoLogin`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 获取im登录信息
   * @return {[type]} [description]
   */
  getImLoginInfo() {
    return axios({
      url: `${API_MODULE}/home/imlogin`,
      method: 'POST',
      responseType: 'json'
    })
  },
  /**
   * 个人号微信好友分组
   * @return {[type]} [description]
   */
  getAccountFriendGroupList(id) {
    return axios({
      url: `${API_MODULE}/search/friend_group/${id}`,
      method: 'GET'
    })
  },
  /**
   * 个人号微信群分组
   * @return {[type]} [description]
   */
  getAccountTeamGroupList(id) {
    return axios({
      url: `${API_MODULE}/search/group_group/${id}`,
      method: 'GET'
    })
  },
  /**
   * 获取微跳转商品信息列表
   * @return {[type]} [description]
   */
  getRecommendGoodsList(params) {
    return axios({
      url: `${API_MODULE}/shop_order/micro_skip/goods/list`,
      method: 'GET',
      params
    })
  },
  /**
   * 购物订单查询
   * @return {[type]} [description]
   */
  getShopOrderList(params) {
    return axios({
      url: `${API_MODULE}/shop_order/friend`,
      method: 'GET',
      params
    })
  },
  /**
   * 购物订单查询
   * @return {[type]} [description]
   */
  getShopOrderDetail(orderNo) {
    return axios({
      url: `${API_MODULE}/shop_order/order/${orderNo}`,
      method: 'GET'
    })
  },
  /**
   * 获取省份/城市树列表
   * @return {[type]} [description]
   */
  getCityTreeList() {
    return axios({
      url: `${API_MODULE}/search/province_city`,
      method: 'GET'
    })
  },
  /**
   * 获取个人号所有的标签
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  getAccountLabels(data, loading) {
    return axios({
      url: `${API_MODULE}/search/wechat_account/label`,
      method: 'POST',
      data,
      responseType: 'json',
      loading
    })
  },
  /**
   * 根据个人号id来获取所有的标签
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  getAccountLabelsByIds(data, loading = false) {
    return axios({
      url: `${API_MODULE}/search/wechat_accounts/label`,
      method: 'POST',
      data,
      responseType: 'json'
    })
  },
  /**
   * 根据部门来获取员及其在线状态
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  getStaffListByDepartment(params) {
    return axios({
      url: `${API_MODULE}/search/get_staff/list`,
      method: 'GET',
      params,
      responseType: 'json'
    })
  },
  /**
   * 根据部门来获取员及其在线状态
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  updateStaffOnline(data) {
    return axios({
      url: `${API_MODULE}/search/update_staff/online`,
      method: 'PUT',
      data,
      responseType: 'json'
    })
  },
  getChatFriend() {
    return axios({
      url: '/manager/v1.0/custom_service/syncfriend',
      method: 'GET'
    })
  },
  getlive(params) {
    return axios({
      url: `${API_MODULE}/home/getlive`,
      method: 'GET',
      params,
      responseType: 'json'
    })
  }
}
