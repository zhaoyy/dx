import axios from 'axios'
const API_MODULE = `/manager/v1.0/wechat`
const API_MODULE_TALK = `/manager/v1.0`
const API_MODULE_COMMON = `/common/v1.0`

export default {
  /**
   * 获取个人号好友聊天记录
   */
  getChatFriendLog(params, errorHandle = true) {
    return axios({
      url: `${API_MODULE}/account/chat/friends/log`,
      method: 'GET',
      params,
      errorHandle,
      responseType: 'json',
      loading: false
    })
  },
  /**
   * 获取个人号群聊聊天记录
   */
  getChatGroupsLog(params, errorHandle = true) {
    return axios({
      url: `${API_MODULE}/account/chat/groups/log`,
      method: 'GET',
      params,
      errorHandle,
      responseType: 'json',
      loading: false
    })
  },
  /**
   * 微客服
   */
  commonData(data, loading) {
    return axios({
      url: `${API_MODULE_TALK}/quick/reply/commonData`,
      method: 'POST',
      data,
      loading: false,
      responseType: 'json'
    })
  },
  /**
   *ocr修改保存
   */
  bindCustomerAndImageOcr(data, loading) {
    return axios({
      url: `${API_MODULE_COMMON}/xunfei/bindCustomerAndImageOcr`,
      method: 'POST',
      data,
      loading: false,
      responseType: 'json'
    })
  }

}
