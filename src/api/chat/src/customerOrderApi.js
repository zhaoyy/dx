import axios from 'axios'
const API_MODULE = `/manager/v1.0/custom_service`

export default {
  /**
   * 获取客户订单绑定状态
   * @return {[type]} [description]
   */
  getWechatUserBindingStatus(id, loading = false) {
    return axios({
      url: `${API_MODULE}/wechat_user_binding_status/${id}`,
      method: 'GET',
      loading,
      responseType: 'json'
    })
  },
  /**
   * 修改客户绑定状态
   * @return {[type]} [description]
   */
  saveWechatUserBindAccount(data) {
    return axios({
      url: `${API_MODULE}/wechat_user_binding_status`,
      method: 'PUT',
      data,
      responseType: 'json'
    })
  }
}
