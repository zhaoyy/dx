import axios from 'axios'

const API_MODULE = `/manager/v2.1/custom_service`

export default {
    /**
     * 微客服获取该个人号下的所有客户列表
     */
    getPersonalAccountCustomerList(params) {
        return axios({
            url: `${API_MODULE}/chat`,
            method: 'GET',
            params,
            loading: false,
            removeEmpty: true
        })
    },
    // 根据分组id获取微客服对应个人号下分组中的客户
    getAccountCustomerListUser(params) {
        return axios({
            url: `${API_MODULE}/chat/group/user`,
            method: 'GET',
            params,
            loading: false,
            removeEmpty: true
        })
    },
    /**
     * 置顶好友接口
     */
    getUserTop(params) {
        return axios({
            url: `${API_MODULE}/user/top`,
            method: 'PUT',
            params,
            responseType: 'json',
            loading: false
        })
    },
}