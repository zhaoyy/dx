import riskManagementApi from './src/riskManagementApi'
import operationLogApi from './src/operationLogApi'
import ecommercePlatformApi from './src/ecommercePlatformApi'
import staffManageApi from './src/staffManageApi'
import indexApi from './src/indexApi'
import personalNumManageApi from './src/personalNumManageApi'
import deviceManageApi from './src/deviceManageApi'
import warehouseManageApi from './src/warehouseManageApi'
import marketPlatformApi from './src/marketPlatformApi'
import screenApi from './src/screenApi'
import commonApi from './src/commonApi'
import powderManageApi from './src/powderManageApi'
import noticeApi from './src/noticeApi'
import noticeStaffApi from './src/noticeStaffApi'
import walletApi from './src/walletApi'
import v2marketPlatformApi from './src/v2/v2marketPlatformApi'
import v2communityManage from './src/v2/v2communityManage'
import v2indexApi from './src/v2/v2indexApi'
import v2condifApi from './src/v2/v2configApi'
import v2riskManagementApi from './src/v2/v2riskManagementApi'
import v3indexApi from './src/v3/v3indexApi.js'
import v4RiskManagementApi from './src/v4/v4RiskManagementApi'

export {
  riskManagementApi,
  operationLogApi,
  ecommercePlatformApi,
  staffManageApi,
  indexApi,
  personalNumManageApi,
  deviceManageApi,
  marketPlatformApi,
  screenApi,
  commonApi,
  powderManageApi,
  noticeApi,
  noticeStaffApi,
  v2indexApi,
  walletApi,
  v2marketPlatformApi,
  v2communityManage,
  v2condifApi,
  v2riskManagementApi,
  v3indexApi,
  v4RiskManagementApi,
  warehouseManageApi
}
