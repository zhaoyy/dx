import axios from 'axios'

const removeEmpty = true

export default {
  /**
   * 首页-数据总览
   *
   * @param {*} params
   * @returns
   */
  getDataScreen(params) {
    return axios({
      url: '/manager/v2.1/index/home/top',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-个人号统计（好友人数）
   *
   * @param {*} params
   * @returns
   */
  getfriendTotal(params) {
    return axios({
      url: '/manager/v2.1/index/home/account/statistics',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-个人号统计（收发信息）
   *
   * @param {*} params
   * @returns
   */
  getSendRecieve(params) {
    return axios({
      url: '/manager/v2.1/index/home/send/get/message',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-好友性别统计
   *
   * @param {*} params
   * @returns
   */
  getSexNum(params) {
    return axios({
      url: '/manager/v2.1/index/home/dashboard/user/sex',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-好友地区统计
   *
   * @param {*} params
   * @returns
   */
  getProvinceNum(params) {
    return axios({
      url: '/manager/v2.1/index/home/dashboard/user/province',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-用户关注问题
   *
   * @param {*} params
   * @returns
   */
  getUserAttentionNum(params) {
    return axios({
      url: '/manager/v2.1/index/home/dashboard/key/word',
      method: 'GET',
      params,
      removeEmpty
    })
  }
}
