import axios from 'axios'
const API_MODULE = `/manager/v2.1`

export default {
  /**
   * 获取企业下风控敏感词,群敏感词,群自动回复 限制个数
   *
   * @param {*} params
   * @returns
   */
  getEnterpriseConfig(params) {
    return axios({
      url: `${API_MODULE}/enterpriseConfig`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 修改企业下风控敏感词,群敏感词,群自动回复 限制个数
   *
   * @params {*} params
   * @returns
   */
  setEnterpriseConfig(params) {
    return axios({
      url: `${API_MODULE}/enterpriseConfig`,
      method: 'PUT',
      data: params
    })
  }
}
