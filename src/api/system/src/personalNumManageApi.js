import axios from 'axios'
const API_MODULE = `/manager/v1.0/wechat/`
const API_MODULE_V = `/manager/v1.0/`
const removeEmpty = true
export default {
  /**
   * 个人号管理--微信好友列表--设置分组
   */
  getFriendsGroup(key) {
    let params = key ? `/${key}` : ''
    let url = `/common/v1.0/search/friend_group` + params
    return axios({
      url: url,
      method: 'GET'
    })
  },
  /**
   * 个人号管理--微信群查看--设置分组
   */
  getWechatGroup(key) {
    let params = key ? `/${key}` : ''
    let url = `/common/v1.0/search/group_group` + params
    return axios({
      url: url,
      method: 'GET'
    })
  },
  /**
   * 获取个人号管理--个人号列表
   *
   * @param {*} params
   * @returns
   */
  accountList(params) {
    return axios({
      url: `${API_MODULE}account`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 获取个人号管理--修改所属分组
   *
   * @param {*} params
   * @returns
   */
  accountGroup(params) {
    return axios({
      url: `${API_MODULE}account/group_info`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 获取个人号管理--修改微信备注
   *
   * @param {*} params
   * @returns
   */
  accountRemark(params) {
    return axios({
      url: `${API_MODULE}account/remarks`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 获取个人号管理--二维码名片--发指令手机上传
   *
   * @param {*} params
   * @returns
   */
  accountCodeMobile(params) {
    return axios({
      url: `${API_MODULE}account/card/${params}`,
      method: 'PUT'
    })
  },
  /**
   * 获取个人号管理--二维码名片--手动上传
   *
   * @param {*} params
   * @returns
   */
  accountCodeByHand(params) {
    return axios({
      url: `${API_MODULE}account/card`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 获取个人号管理--删除个人号
   *
   * @param {*} params
   * @returns
   */
  accountDelete(params) {
    return axios({
      url: `${API_MODULE_V}wechat/account/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 获取个人号管理--好友列表
   *
   * @param {*} params
   * @returns
   */
  accountFriends(params) {
    return axios({
      url: `${API_MODULE}account/chat/friends`,
      method: 'GET',
      errorHandle: false,
      params: params,
      removeEmpty
    })
  },
  /**
   * 获取个人号管理--好友列表-聊天记录
   *
   * @param {*} params
   * @returns
   */
  accountFriendsLog(params) {
    return axios({
      url: `${API_MODULE}account/chat/friends/log`,
      method: 'GET',
      errorHandle: false,
      params: params,
      removeEmpty
    })
  },
  /**
   * 获取个人号管理--群列表
   *
   * @param {*} params
   * @returns
   */
  accountGroups(params) {
    return axios({
      url: `${API_MODULE}account/chat/groups`,
      method: 'GET',
      errorHandle: false,
      params: params,
      removeEmpty
    })
  },
  /**
   * 获取个人号管理--群列表-聊天记录
   *
   * @param {*} params
   * @returns
   */
  accountGroupsLog(params) {
    return axios({
      url: `${API_MODULE}account/chat/groups/log`,
      method: 'GET',
      errorHandle: false,
      params: params,
      removeEmpty
    })
  },
  /**
   * 获取个人号管理--群列表-好友列表
   *
   * @param {*} params
   * @returns
   */
  accountGroupsFriends(params) {
    return axios({
      url: `${API_MODULE}account/chat/groups/member`,
      method: 'GET',
      errorHandle: false,
      params: params,
      removeEmpty
    })
  },
  /**
   * 微信好友列表--获取微信好友列表
   *
   * @param {*} params
   * @returns
   */
  friendsListGetList(params) {
    return axios({
      url: `${API_MODULE}user`,
      method: 'GET',
      params: params,
      removeEmpty,
      timeout: 180000
    })
  },
  /**
   * 微信好友列表--获取微信好友列表
   *
   * @param {*} params
   * @returns
   */
  recordNoticeNum(params) {
    return axios({
      url: `${API_MODULE}account/chat/count`,
      method: 'GET',
      errorHandle: false,
      params: params,
      removeEmpty
    })
  },
  /**
   * 微信好友列表--修改用户分组
   *
   * @param {*} params
   * @returns
   */
  friendsListModifyGroup(params) {
    return axios({
      url: `${API_MODULE}user/change/groups`,
      method: 'PUT',
      data: params,
      removeEmpty
    })
  },
  /**
   * 微信好友列表--分配用户给员工
   *
   * @param {*} params
   * @returns
   */
  friendsListStaffUsers(params) {
    return axios({
      url: `${API_MODULE}user/change/staff`,
      // url: `${API_MODULE}user/change/staff/batch`,
      method: 'PUT',
      data: params,
      removeEmpty
    })
  },
  /**
   * 微信好友列表--分配用户给员工--一键分配
   *
   * @param {*} params
   * @returns
   */
  friendsListStaffUsersKey(params) {
    return axios({
      url: `${API_MODULE}user/change/staff/batch`,
      method: 'PUT',
      data: params,
      removeEmpty,
      timeout: 180000
    })
  },
  /**
   * 微信好友列表--获取微信号好友聊天记录
   *
   * @param {*} params
   * @returns
   */
  // friendsListGetLog(params) {
  //   return axios({
  //     url: `${API_MODULE}user/chat/friends/log`,
  //     method: 'POST',
  //     data: params
  //   })
  // },
  /**
   * 微信好友列表--聊天记录导出
   *
   * @param {*} params
   * @returns
   */
  friendsListExportLog(params) {
    return axios({
      url: `${API_MODULE}user/export`,
      method: 'GET',
      responseType: 'blob',
      params: params,
      removeEmpty
    })
  },
  /**
   * 微信好友列表--获取对应客户信息
   *
   * @param {*} params
   * @returns
   */
  friendsListGetuserInfo(params) {
    return axios({
      url: `${API_MODULE}user/${params}`,
      method: 'GET'
    })
  },
  /**
   * 微信群查看--获取微信群列表
   *
   * @param {*} params
   * @returns
   */
  wechatGroupListGet(params) {
    return axios({
      url: `${API_MODULE}group`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 微信群查看--分配群聊给员工
   *
   * @param {*} params
   * @returns
   */
  wechatGroupListAssignStaff(params) {
    return axios({
      url: `${API_MODULE}group/change/staff`,
      // url: `${API_MODULE}group/change/staff/batch`,
      method: 'PUT',
      data: params,
      removeEmpty
    })
  },
  /**
   * 微信群查看--分配群聊给员工--一键分配
   *
   * @param {*} params
   * @returns
   */
  wechatGroupListAssignStaffKey(params) {
    return axios({
      url: `${API_MODULE}group/change/staff/batch`,
      method: 'PUT',
      data: params,
      removeEmpty,
      timeout: 120000
    })
  },
  /**
   * 微信群查看--修改群聊分组
   *
   * @param {*} params
   * @returns
   */
  wechatGroupListModifyGroup(params) {
    return axios({
      url: `${API_MODULE}group/change/groups`,
      method: 'PUT',
      data: params,
      removeEmpty
    })
  },
  /**
   * 微信群查看--聊天记录导出
   *
   * @param {*} params
   * @returns
   */
  wechatGroupListExportLog(params) {
    return axios({
      url: `${API_MODULE}group/export`,
      method: 'GET',
      responseType: 'blob',
      params: params,
      removeEmpty
    })
  },
  /**
   * 微信群查看--查看群聊成员
   *
   * @param {*} params
   * @returns
   */
  wechatGroupListGroupMembers(params) {
    return axios({
      url: `${API_MODULE}group/chat/groups/user`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 分组管理--获取列表
   *
   * @param {*} params
   * @returns
   */
  groupManageList(params) {
    return axios({
      url: `${API_MODULE_V}group/list`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 分组管理--新增
   *
   * @param {*} params
   * @returns
   */
  groupManageAdd(params) {
    return axios({
      url: `${API_MODULE_V}group`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 分组管理--修改
   *
   * @param {*} params
   * @returns
   */
  groupManageModify(params) {
    return axios({
      url: `${API_MODULE_V}group`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 分组管理--删除
   *
   * @param {*} params
   * @returns
   */
  groupManageDelete(params) {
    return axios({
      url: `${API_MODULE_V}group/${params}`,
      method: 'DELETE'
    })
  },
  /**
   * 分配记录--列表获取
   *
   * @param {*} params
   * @returns
   */
  dRecord(params) {
    return axios({
      url: `${API_MODULE_V}assign/log`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 群发记录--列表获取
   *
   * @param {*} params
   * @returns
   */
  groupRecord(params) {
    return axios({
      url: `${API_MODULE_V}push/history`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 群发记录--群发明细--列表获取
   *
   * @param {*} params
   * @returns
   */
  groupRecordDetail(params) {
    return axios({
      url: `${API_MODULE_V}push/history/logs`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  }
}
