import axios from 'axios'
const API_MODULE = `/manager/v1.0`

export default {
  // 操作日志
  /**
   * 获取操作日志列表列表
   *
   * @param {*} params
   * @returns
   */
  getOperationLogList(params) {
    return axios({
      url: `${API_MODULE}/log`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 操作日志列表导出列表
   *
   * @param {*} params
   * @returns
   */
  operationLogExport(params){
    return axios({
      url: `${API_MODULE}/log/export`,
      method: 'GET',
      params: params,
      responseType: 'blob'
    })
  }
}
