import axios from 'axios'
const API_MODULE = `/common/v1.0/`

export default {
  /**
   * 设备分组
   *
   * @param {*} data
   * @returns
   */
  getEqGroup(data) {
    return axios({
      url: `${API_MODULE}search/eq_group`,
      method: 'GET',
      params: data
    })
  },
  /**
   * 员工列表
   *
   * @param {*} data
   * @returns
   */
  getStaff(data) {
    return axios({
      url: `${API_MODULE}search/staff`,
      method: 'GET',
      params: data
    })
  },
  /**
   * 手机号加好友-获取个人号数据
   *
   * @param {*} data
   * @returns
   */
  accountList(data) {
    return axios({
      url: `${API_MODULE}search/wechat_account`,
      method: 'GET'
    })
  },
  /**
   * 手机号加好友-获取个人号数据（新）
   *
   * @param {*} data
   * @returns
   */
  accountListNew(data) {
    return axios({
      url: '/manager/v2.0/wechat/account/data/add_friend',
      method: 'GET'
    })
  },
  /**
   * 手机号加好友-获取个人号标签数据
   *
   * @param {*} data
   * @returns
   */
  accountLabelList(data) {
    return axios({
      url: `${API_MODULE}search/wechat_account/label`,
      method: 'POST',
      data: data
    })
  }
}
