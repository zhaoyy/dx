import axios from 'axios'
const API_MODULE = `/manager/v1.0`

export default {
  // 电商平台
  /**
   * 获取店面列表
   *
   * @param {*} params
   * @returns
   */
  getStoreList(params) {
    return axios({
      url: `${API_MODULE}/shop`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取店铺订单列表
   * @param {*} params
   */
  getStoreOrderList(params) {
    return axios({
      url: `${API_MODULE}/shop_order`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取订单导入记录列表
   * @param {*} params
   */
  getOrderRecordList(params) {
    return axios({
      url: `${API_MODULE}/order_record`,
      removeEmpty: true,
      method: 'GET',
      params: params
    })
  },
  /**
   * 获取店铺订单详情
   */
  getOrderDetail(id) {
    return axios({
      url: `${API_MODULE}/shop_order/${id}`,
      method: 'GET'
    })
  },
  /**
   * 获取店铺类目列表
   */
  getStoreCategory() {
    return axios({
      url: `${API_MODULE}/shop/category`,
      method: 'GET',
      removeEmpty: true
    })
  },
  /**
   * 新增订单导入记录
   */
  addOrderImport(params) {
    return axios({
      url: `${API_MODULE}/order_record/${params.shopId}`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      timeout: 120000,
      data: params.formData
    })
  },
  // 获取淘宝订单Execl模板
  getTaobaoTemplate(params) {
    return axios({
      url: `/common/v1.0/config/${params.enterprisId}/${params.key}`,
      method: 'GET'
    })
  },
  // 添加店铺
  addStore(params) {
    return axios({
      url: `${API_MODULE}/shop`,
      method: 'POST',
      data: params
    })
  },
  // 获取失败订单
  getOrderRecordFail(params) {
    return axios({
      url: `${API_MODULE}/order_record_fail`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  // 获取新增店铺信息
  getAddShopInfo() {
    return axios({
      url: `${API_MODULE}/shop/add_shop_info`,
      method: 'GET'
    })
  },
  // 删除店铺
  deleteStore(params) {
    return axios({
      url: `${API_MODULE}/shop/${params}`,
      method: 'DELETE'
    })
  },
  // 编辑店铺
  updateStore(params) {
    return axios({
      url: `${API_MODULE}/shop`,
      method: 'PUT',
      data: params
    })
  },
  // 店铺授权
  storeAuth(params) {
    return axios({
      url: `${API_MODULE}/shop/auth`,
      method: 'POST',
      data: params
    })
  }
}
