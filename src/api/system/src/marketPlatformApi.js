import axios from 'axios'
const API_MODULE = `/manager/v1.0`
const API_MODULEVV = `/manager/v2.0`

export default {
  // 营销平台=>红包营销
  /**
   * 获取红包充值列表
   *
   * @param {*} params
   * @returns
   */
  getPacketsRechargeList(params) {
    return axios({
      url: `${API_MODULE}/redpage_recharge`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 获取红包领取记录列表
   */
  getPacketsRecordList(params) {
    return axios({
      url: `${API_MODULE}/redpage_record`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 新增红包充值记录
   */
  newPacketsRecharge(params) {
    return axios({
      url: `${API_MODULE}/redpage_recharge`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 红包充值获取企业余额
   */
  getMoney(params) {
    return axios({
      url: `${API_MODULE}/redpage_recharge/money`,
      method: 'GET',
      params: params
    })
  },
  // 营销平台=>短信营销
  /**
   * 获取短信充值记录列表
   *
   * @param {*} params
   * @returns
   */
  getSmsRechargeList(params) {
    return axios({
      url: `${API_MODULE}/msg_charge_log`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 新增短信充值记录
   */
  newRechargeSms(params) {
    return axios({
      url: `${API_MODULE}/msg_charge_log/charge_info`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 获取短信群发列表
   */
  getGroupSendList(params) {
    return axios({
      url: `${API_MODULE}/msg_group_send`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 短信群发->获取微信好友手机列表
   */
  getWxFriendList(params) {
    return axios({
      url: `${API_MODULE}/msg_group_send/wechat_phone/list`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 短信群发->获取短信模板信息
   */
  msgtemplateList() {
    return axios({
      url: `${API_MODULE}/msg_group_send/msgtemplate`,
      method: 'POST'
    })
  },
  /**
   * 短信群发->点击短信群发
   */
  sendGroupMsg(params) {
    return axios({
      url: `${API_MODULE}/msg_group_send/wechat_phone/send`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 短信群发->手机信息excel文件导入
   */
  importPhones(params) {
    return axios({
      url: `${API_MODULE}/msg_group_send/import_phone`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: params
    })
  },
  /**
   * 短信群发->查看详情
   */
  viewGroupSend(id) {
    return axios({
      url: `${API_MODULE}/msg_group_send/${id}`,
      method: 'GET'
    })
  },
  /**
   * 短信群发->微信好友获取搜索信息
   */
  searchConditionWxFriend() {
    return axios({
      url: `${API_MODULE}/msg_group_send/search_condition`,
      method: 'GET'
    })
  },
  /**
   * 短信群发->微信好友根据个人号id获取分组信息
   */
  searchGroupsWxFriend(id) {
    return axios({
      url: `${API_MODULE}/msg_group_send/search_groups/${id}`,
      method: 'GET'
    })
  },
  /**
   * 获取短信发送记录列表
   */
  getSendRecordList(params) {
    return axios({
      url: `${API_MODULE}/msg_send_log`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 获取短信模版列表
   */
  getHistoryTemplateList(params) {
    return axios({
      url: `${API_MODULE}/message/template`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 新增短信模版
   */
  newTemplate(params) {
    return axios({
      url: `${API_MODULE}/message/template`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 编辑短信模版
   */
  editTemplate(params) {
    return axios({
      url: `${API_MODULE}/message/template`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 获取短信模版详情
   */
  getTemplateDetail(id) {
    return axios({
      url: `${API_MODULE}/message/template/${id}`,
      method: 'GET'
    })
  },
  /**
   * 删除短信模版信息
   */
  delTemplate(id) {
    return axios({
      url: `${API_MODULE}/message/template/${id}`,
      method: 'DELETE'
    })
  },
  // 营销平台=>晒图活动
  /**
   * 获取晒图活动列表
   *
   * @param {*} params
   * @returns
   */
  getShowPictureList(params) {
    return axios({
      url: `${API_MODULE}/activity_pic`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 下线晒图活动
   */
  offActivityRecord(id) {
    return axios({
      url: `${API_MODULE}/activity_pic/${id}`,
      method: 'PUT'
    })
  },
  /**
   * 活动统计数据
   */
  statisticsActivity(id) {
    return axios({
      url: `${API_MODULE}/activity_pic/statistics/${id}`,
      method: 'GET'
    })
  },
  /**
   * 删除晒图活动信息
   */
  delActivity(id) {
    return axios({
      url: `${API_MODULE}/activity_pic/${id}`,
      method: 'DELETE'
    })
  },
  /**
   * 新增晒图活动
   */
  newActivity(params) {
    return axios({
      url: `${API_MODULE}/activity_pic`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 预览晒图活动
   *
   * @param {*} params
   * @returns
   */
  previewActivity(params) {
    return axios({
      url: `${API_MODULE}/activity_pic/preview`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 编辑晒图活动
   */
  editActivity(params) {
    return axios({
      url: `${API_MODULE}/activity_pic`,
      method: 'PUT',
      data: params
    })
  },
  /**
   * 获取晒图活动详情
   */
  getActivityDetail(id) {
    return axios({
      url: `${API_MODULE}/activity_pic/${id}`,
      method: 'GET'
    })
  },
  /**
   * 获取晒图活动记录列表
   */
  getActivityRecordList(params) {
    return axios({
      url: `${API_MODULE}/activity_pic_record/spending`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 获取晒图活动活动审核列表
   */
  getActivityAuditList(params) {
    return axios({
      url: `${API_MODULE}/activity_pic_record`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 审核（申请详情）
   */
  activityPicRecordDetail(id) {
    return axios({
      url: `${API_MODULE}/activity_pic_record/detail/${id}`,
      method: 'GET'
    })
  },
  /**
   * 审核记录
   */
  auditRecord(params) {
    return axios({
      url: `${API_MODULE}/activity_pic_record`,
      method: 'PUT',
      data: params
    })
  },
  // 营销平台=>朋友圈营销
  /**
   * 获取定时发朋友圈列表
   */
  getRegularCircleList(params) {
    return axios({
      url: `${API_MODULE}/friend/publish`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 发朋友圈
   */
  newCircle(params) {
    return axios({
      url: `${API_MODULE}/friend/publish`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 查看朋友圈
   */
  viewCircle(id) {
    return axios({
      url: `${API_MODULE}/friend/publish/friend/${id}`,
      method: 'GET'
    })
  },
  /**
   * 获取定时发朋友圈详情
   */
  getCircleDetail(id) {
    return axios({
      url: `${API_MODULE}/friend/publish/${id}`,
      method: 'GET'
    })
  },
  /**
   * 删除朋友圈
   */
  delCircle(id) {
    return axios({
      url: `${API_MODULE}/friend/publish/${id}`,
      method: 'DELETE'
    })
  },
  /**
   * 获取个人号列表
   */
  getPersonalNoList(params) {
    return axios({
      url: `${API_MODULE}/friend/publish/personal`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 获取标签列表
   */
  getVisibleLabelList(params) {
    return axios({
      url: `${API_MODULE}/friend/publish/label`,
      method: 'POST',
      data: params
    })
  },
  // 营销平台=>微跳转
  /**
   * 获取微跳转列表
   */
  getMicroJumpList(params) {
    return axios({
      url: `${API_MODULE}/micro_skip`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 删除微跳转信息
   */
  delMicroJump(id) {
    return axios({
      url: `${API_MODULE}/micro_skip/${id}`,
      method: 'DELETE'
    })
  },
  /**
   * 生成微跳转
   */
  newMicroJump(params) {
    return axios({
      url: `${API_MODULE}/micro_skip/add_microskip`,
      method: 'POST',
      data: params
    })
  },
  /**
   * 发圈记录查询
   */
  friendPubSelectlist(params) {
    return axios({
      url: `${API_MODULE}/friend/publish/selectlist`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 发圈记录导出
   */
  friendPubExportCircle(params) {
    return axios({
      url: `${API_MODULE}/friend/publish/exportCircle`,
      method: 'GET',
      params: params,
      responseType: 'blob'
    })
  },
  /**
   * 发圈记录查询
   */
  checkMessage(params) {
    return axios({
      url: `${API_MODULEVV}/check_message`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 发圈记录导出
   */
  exportCheckMessage(params) {
    return axios({
      url: `${API_MODULEVV}/check_message/exportCheckMessage`,
      method: 'GET',
      params
    })
  }
}
