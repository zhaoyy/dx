import axios from 'axios'

const API_MODULE = `/manager/v1.0`

export default {
  /**
   * 获取通知列表
   *
   * @param {*} params
   * @returns
   */
  getNoticeStaffList(params) {
    return axios({
      url: `${API_MODULE}/notificationStaff`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取通知详情
   *
   * @param {*} params
   * @returns
   */
  getNoticeStaffDetail(id) {
    return axios({
      url: `${API_MODULE}/notificationStaff/${id}`,
      method: 'GET'
    })
  },
/**
 * 获得未读记录数
 */
getNoticeStaffNum()
{
  return axios({
    url: `${API_MODULE}/notificationStaff/num`,
    method: 'GET'
  })
}
}


