import axios from 'axios'
const API_MODULE_V = `/manager/v1.0/`
const removeEmpty = true
export default{
  /**
   * 入库管理--筛选运营商
   *
   * @param {*} params
   * @returns
   */
  getproviderList(params) {
    return axios({
      url: `${API_MODULE_V}equ/warehouse/provider`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 入库管理--筛选套餐
   *
   * @param {*} params
   * @returns
   */
  getpackageList(params) {
    return axios({
      url: `${API_MODULE_V}equ/warehouse/package`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
    /**
   * 入库管理--获取列表
   *
   * @param {*} params
   * @returns
   */
  warehouseList(params) {
    return axios({
      url: `${API_MODULE_V}equ/warehouse`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },

   /**
   * 入库管理--修改套餐和运营商
   *    *
   * @param {*} params
   * @returns
   */
  updateproAndPacApi(params) {
    return axios({
      url: `${API_MODULE_V}equ/warehouse/update`,
      method: 'PUT',
      params: params,
      removeEmpty
    })
  },
  // 导出
  warehouseExport(params){
    return axios({
      url: `${API_MODULE_V}equ/warehouse/export`,
      method: 'GET',
      params: params,
      responseType: 'blob'
    })
  }

}