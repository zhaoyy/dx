import axios from 'axios'

const removeEmpty = true

export default {
  /**
   * 首页-方块格数据
   *
   * @param {*} params
   * @returns
   */
  getOverview(params) {
    return axios({
      url: '/manager/v3.0/index/overview',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-数据总览
   *
   * @param {*} params
   * @returns
   */
  getDataScreen(params) {
    return axios({
      url: '/manager/v3.0/index/home/top',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-好友统计/好友性别统计
   *
   * @param {*} params
   * @returns
   */
  getUserSex(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/sex',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-好友统计/好友地区统计
   *
   * @param {*} params
   * @returns
   */
  getUserProvince(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/province',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-敏感操作统计
   *
   * @param {*} params
   * @returns
   */
  getSensitiveAction(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/action',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-进粉统计
   *
   * @param {*} params
   * @returns
   */
  getUserIncreasePassive(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/increase/passive',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  // 首页进粉统计导出
  getUserFansState(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/increase/passive/export',
      method: 'GET',
      params: params,
      responseType: 'blob'
    })
  },
  /**
   * 首页-好友增长统计
   *
   * @param {*} params
   * @returns
   */
  getUserIncreaseCount(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/increase/count',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-好友增长排行
   *
   * @param {*} params
   * @returns
   */
  getUserIncreaseRank(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/increase/rank',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-好友分配统计
   *
   * @param {*} params
   * @returns
   */
  getUserAssignCount(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/assign/count',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-接待人数统计
   *
   * @param {*} params
   * @returns
   */
  getStaffJiedaiCount(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/staff/jiedai/count',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-接待人数统计-员工明细
   *
   * @param {*} params
   * @returns
   */
  getStaffJiedaiCountDetail(params) {
    return axios({
      url: `/manager/v3.0/index/dashboard/staff/jiedai/detail`,
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-客服对话统计
   *
   * @param {*} params
   * @returns
   */
  getStaffTalkCount(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/staff/talk/count',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-客服对话统计-明细
   *
   * @param {*} params
   * @returns
   */
  getStaffTalkCountDetail(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/staff/talk/count/detail',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-好友未通过统计
   *
   * @param {*} params
   * @returns
   */
  getUserUnpass(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/pass',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-微信群统计
   *
   * @param {*} params
   * @returns
   */
  getChatroom(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/chatroom',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-微信群统计-明细
   *
   * @param {*} params
   * @returns
   */
  getChatroomDetail(params) {
    return axios({
      url: `/manager/v3.0/index/dashboard/chatroom/${params.type}/${params.id}`,
      method: 'GET',
      removeEmpty
    })
  },
  /**
   * 首页-好友响应时间统计/按客服
   *
   * @param {*} params
   * @returns
   */
  getStaffResponse(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/staff/response',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  // 客服-好友响应时间统计导出
  getStaffReport(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/staff/response/export',
      method: 'GET',
      params: params,
      responseType: 'blob'
    })
  },
  /**
   * 首页-好友响应时间统计/按客服-明细
   *
   * @param {*} id
   * @returns
   */
  getStaffResponseDetail(id) {
    return axios({
      url: `/manager/v3.0/index/dashboard/staff/response/${id}`,
      method: 'GET'
    })
  },
  /**
   * 首页-好友响应时间统计/按微信号
   *
   * @returns
   */
  getChatResponse(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/chat/response',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  // 微信-好友响应时间统计导出
  getChatReport(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/chat/response/export',
      method: 'GET',
      params: params,
      responseType: 'blob'
    })
  },
  /**
   * 首页-好友响应时间统计/按微信号-明细
   *
   * @param {*} id
   * @returns
   */
  getChatResponseDetail(id) {
    return axios({
      url: `/manager/v3.0/index/dashboard/chat/response/${id}`,
      method: 'GET',
      removeEmpty
    })
  },
  /**
   * 首页-微信群活跃统计
   *
   * @param {*} params
   * @returns
   */
  getRoomTalk(params) {
    return axios({
      url: `/manager/v3.0/index/dashboard/room/${params.groupId}/talk/count`,
      method: 'GET',
      removeEmpty
    })
  },
  // 微信群活跃统计导出
  wechatGAExport(params) {
    return axios({
      url: `/manager/v3.0/index/dashboard/room/${params.groupId[1]}/talk/count/export`,
      method: 'GET',
      params: params,
      responseType: 'blob'
    })
  },
  /**
   * 首页-个人号下拉框数据
   *
   * @param {*} params
   * @returns
   */
  getWechatList(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/wechat/list',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-通话记录统计
   *
   * @param {*} params
   * @returns
   */
  getCallList(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/call/page',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 首页-好友增长排行-导出
   *
   * @param {*} params
   * @returns
   */
  exportUserIncreaseRank(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/increase/rank/export',
      method: 'GET',
      params,
      removeEmpty,
      responseType: 'string'
    })
  },
  /**
   * 首页-好友未通过统计-导出
   *
   * @param {*} params
   * @returns
   */
  exportUserUnpass(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/pass/export',
      method: 'GET',
      params,
      removeEmpty,
      responseType: 'blob'
    })
  },
  /**
   * 首页-好友分配统计-导出
   *
   * @param {*} params
   * @returns
   */
  exportUserAssignCount(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/user/assign/count/export',
      method: 'GET',
      params,
      removeEmpty
      // responseType: 'blob'
    })
  },
  /**
   * 首页-客服对话统计-导出
   *
   * @param {*} params
   * @returns
   */
  exportStaffTalkCount(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/staff/talk/count/export',
      method: 'GET',
      params,
      removeEmpty
      // responseType: 'blob'
    })
  },
  /**
   * 首页-接待人数统计-导出
   *
   * @param {*} params
   * @returns
   */
  exportStaffJiedaiCount(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/staff/jiedai/count/export',
      method: 'GET',
      params,
      removeEmpty
      // responseType: 'blob'
    })
  },
  /**
   * 微信群统计-导出
   *
   * @param {*} params
   * @returns
   */
  exportChatroom(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/chatroom/export',
      method: 'GET',
      params,
      removeEmpty,
      responseType: 'blob'
    })
  },
  /**
   * 通话记录统计-导出
   *
   * @param {*} params
   * @returns
   */
  exportCallAll(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/call/page/export/all',
      method: 'GET',
      params,
      responseType: 'blob'
    })
  },
  /**
   * 通话记录统计-导出
   *
   * @param {*} params
   * @returns
   */
  exportCall(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/call/page/export',
      method: 'PUT',
      data: params,
      removeEmpty,
      responseType: 'blob'
    })
  },
  /**
   * 消息推送查询
   */
  autoTaskMsgStatistics(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/auto_task_msg_statistics',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 一键加微查询
   */
  taskStatistics(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/task_statistics',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 消息推送导出
   */
  taskStatisticsExport(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/auto_task_msg_statistics_export',
      method: 'GET',
      params,
      removeEmpty,
      responseType: 'blob'
    })
  },
  /**
   * 一键加微导出
   */
  tautoTaskstatisticsExport(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/task_statistics_export',
      method: 'GET',
      params,
      removeEmpty,
      responseType: 'blob'
    })
  },
  /**
   * 首页-客服在线统计查询
   *
   * @param {*} params
   * @returns
   */
  equipmentOnline(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/equipment_online',
      method: 'GET',
      params,
      removeEmpty
    })
  },
  /**
   * 客服在线统计导出
   */
  equipmentOnlineExport(params) {
    return axios({
      url: '/manager/v3.0/index/dashboard/equipment_online_export',
      method: 'GET',
      params,
      removeEmpty,
      responseType: 'blob'
    })
  },
  /**
   * 首页-/在线好友时长-明细
   *
   * @param {*} id
   * @returns
   */
  equipmentOnlineDetail(id) {
    return axios({
      url: `/manager/v3.0/index/dashboard/equipment_online/${id}`,
      method: 'GET'
    })
  }

}
