import axios from 'axios'
const API_MODULE_V = `/manager/v1.0/`
const removeEmpty = true
export default {
    /**
     * 设备管理--获取列表
     *
     * @param {*} params
     * @returns
     */
    equipmentList(params) {
        return axios({
            url: `${API_MODULE_V}equipement`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    // 手机管理导出
    equipmentExport(params) {
        return axios({
            url: `${API_MODULE_V}equipement/export`,
            method: 'GET',
            params: params,
            responseType: 'blob'
        })
    },
    /**
     * 设备管理--修改公用
     *
     * @param {*} params
     * @returns
     */
    equipmentModify(params) {
        return axios({
            url: `${API_MODULE_V}equipement/update`,
            method: 'POST',
            data: params
        })
    },
    /**
     * 设备管理--设备历史使用人记录
     *
     * @param {*} params
     * @returns
     */
    equipmentUserPer(params) {
        return axios({
            url: `${API_MODULE_V}equipement/history/${params}`,
            method: 'GET'
                // data: params
        })
    },
    /**
     * 设备管理--启用/禁用
     *
     * @param {*} params
     * @returns
     */
    toggleOpen(params) {
        return axios({
            url: `${API_MODULE_V}equipement/update/status`,
            method: 'POST',
            data: params
        })
    },
    /**
     * 设备管理--启用/禁用
     *
     * @param {*} params
     * @returns
     */
    modifyDeviceRemark(params) {
        return axios({
            url: `${API_MODULE_V}equipement/update/remark`,
            method: 'POST',
            data: params
        })
    },
    /**
     * 设备管理--新增
     *
     * @param {*} params
     * @returns
     */
    equipmentAdd(params) {
        return axios({
            url: `${API_MODULE_V}equipement`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 设备管理--删除
     *
     * @param {*} params
     * @returns
     */
    equipmentDel(params) {
        return axios({
            url: `${API_MODULE_V}equipement/${params}`,
            method: 'DELETE'
        })
    },
    /**
     * 设备管理--风控权限--获取所有
     *
     * @param {*} params
     * @returns
     */
    equipmentAllRisk(params) {
        return axios({
            url: `${API_MODULE_V}equipement/risk`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 设备管理--风控权限--获取已有
     *
     * @param {*} params
     * @returns
     */
    equipmentPartRisk(params) {
        return axios({
            url: `${API_MODULE_V}equipement/${params}`,
            method: 'GET'
        })
    },
    /**
     * 设备管理--风控权限--保存
     *
     * @param {*} params
     * @returns
     */
    equipmentRiskSave(params) {
        return axios({
            url: `${API_MODULE_V}equipement/update/risk`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 设备管理--获取所有app名称
     *
     * @param {*} params
     * @returns
     */
    equipmentAppNames(params) {
        return axios({
            url: `${API_MODULE_V}equipement/app`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 设备管理--获取已设置app名称
     *
     * @param {*} params
     * @returns
     */
    equipmentGetInsatallApp(params) {
        return axios({
            url: `${API_MODULE_V}equipement/app/${params}`,
            method: 'GET'
        })
    },
    /**
     * 设备管理--已设置app名称--保存
     *
     * @param {*} params
     * @returns
     */
    equipmentAppSave(params) {
        return axios({
            url: `${API_MODULE_V}equipement/app/update`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 设备管理--接收员工部门-获取
     *
     * @param {*} params
     * @returns
     */
    equipmentGetDepartment(params) {
        return axios({
            url: `${API_MODULE_V}equipement/staff/depaetment/chil`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 设备管理--接收员工部门-部门员工获取
     *
     * @param {*} params
     * @returns
     */
    equipmentGetDepartmentStaff(params) {
        return axios({
            url: `${API_MODULE_V}equipement/staff/dep/${params}`,
            method: 'GET'
        })
    },
    /**
     * 设备管理--APP更新
     *
     * @param {*} params
     * @returns
     */
    updateApp() {
        return axios({
            url: `${API_MODULE_V}equipement/click/update`,
            method: 'GET',
            removeEmpty
        })
    },
    /**
     * 设备管理--检查更新
     *
     * @param {*} params
     * @returns
     */
    equipmentUpdate(params) {
        return axios({
            url: `${API_MODULE_V}equipement/check`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 设备管理--excel文件导入
     *
     * @param {*} params
     * @returns
     */
    equipmentUploadFile(data, params) {
        return axios({
            url: `${API_MODULE_V}equipement/import/${params.type}/${params.id}`,
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            data
        })
    },
    /**
     * 设备管理--下载excel模板
     *
     * @param {*} params
     * @returns
     */
    equipmentDownFile(params) {
        return axios({
            url: `${API_MODULE_V}equipement/excel/out`,
            method: 'GET',
            responseType: 'blob',
            params: params,
            removeEmpty
        })
    },
    /**
     * 设备管理--联系人电话导入
     *
     * @param {*} params
     * @returns
     */
    equipmentConcatesUpload(params) {
        return axios({
            url: `${API_MODULE_V}equipement/import/link`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 设备分组--获取列表
     *
     * @param {*} params
     * @returns
     */
    eGroupList(params) {
        return axios({
            url: `${API_MODULE_V}equ/group`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 设备分组--新增
     *
     * @param {*} params
     * @returns
     */
    eGroupAdd(params) {
        return axios({
            url: `${API_MODULE_V}equ/group`,
            method: 'POST',
            data: params
        })
    },
    /**
     * 设备分组--编辑
     *
     * @param {*} params
     * @returns
     */
    eGroupEdit(params) {
        return axios({
            url: `${API_MODULE_V}equ/group`,
            method: 'PUT',
            data: params
        })
    },
    /**
     * 设备分组--删除
     *
     * @param {*} params
     * @returns
     */
    eGroupDelete(params) {
        return axios({
            url: `${API_MODULE_V}equ/group/${params}`,
            method: 'DELETE'
        })
    },
    /**
     * 分配记录--获取列表
     *
     * @param {*} params
     * @returns
     */
    deviceDistributionList(params) {
        return axios({
            url: `${API_MODULE_V}equ/record`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    // 分配记录导出
    deviceDisExport(params) {
        return axios({
            url: `${API_MODULE_V}equ/record/export`,
            method: 'GET',
            params: params,
            responseType: 'blob'
        })
    },

    /**
     * 微信设置--获取信息
     *
     * @param {*} params
     * @returns
     */
    wSetGetinfo(params) {
        return axios({
            url: `${API_MODULE_V}equ/config`,
            method: 'GET'
        })
    },
    /**
     * 微信设置--获取信息
     *
     * @param {*} params
     * @returns
     */
    wSetModifyinfo(params) {
        return axios({
            url: `${API_MODULE_V}equ/config`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--左侧--快捷回复分组--新增
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayLeftGroupAdd(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply/group`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--左侧--快捷回复分组--详情
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayLeftGroupDetail(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply/group/${params}`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--左侧--快捷回复分组--编辑
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayLeftGroupModify(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply/group`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--左侧--快捷回复分组--删除
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayLeftGroupDelete(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply/group/${params}`,
            method: 'DELETE'
        })
    },
    /**
     * 快捷回复--左侧--公共快捷语
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayLeftCommon(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply/allgroup`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--左侧--私人快捷语
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayLeftPrivate(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply/group`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--左侧--公司库材料
     *
     * @param {*} params
     * @returns
     */
     qReplayReplayleftInfoCompany(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply/allComPanygroup`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--快捷回复列表--获取
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayListGet(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--快捷回复详情--获取
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayListDetailGet(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply/${params}`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--快捷回复--新增
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayListAdd(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--快捷回复--编辑
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayListModify(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--批量删除快捷回复信息
     *
     * @param {*} params
     * @returns
     */
    qReplayReplayListDelete(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply/del`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--上移下移
     *
     * @param {*} params
     * @returns
     */
    qReplayReplaySort(params) {
        return axios({
            url: `${API_MODULE_V}quick/reply/move`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 快捷回复--选择素材--弹窗--获取素材列表
     *
     * @param {*} params
     * @returns
     */
    materialSourceGet(params) {
        return axios({
            url: `${API_MODULE_V}friend/publish/material`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 下属快捷回复--获取列表
     *
     * @param {*} params
     * @returns
     */
    subquikRelayGetList(params) {
        return axios({
            url: `${API_MODULE_V}subordinate/reply`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 下属快捷回复--删除
     *
     * @param {*} params
     * @returns
     */
    subquikRelayDeleteList(params) {
        return axios({
            url: `${API_MODULE_V}subordinate/reply/${params}`,
            method: 'DELETE'
        })
    },
    /**
     * 下属快捷回复--所属下属
     *
     * @param {*} params
     * @returns
     */
    subquikRelayGetOwnCustomer() {
        return axios({
            url: `${API_MODULE_V}subordinate/reply/staff`,
            method: 'GET'
        })
    },
    /**
     * 自动通过好友--列表获取
     *
     * @param {*} params
     * @returns
     */
    autoPassFriendsGetList(params) {
        return axios({
            url: `${API_MODULE_V}pass/user`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 自动通过好友--编辑
     *
     * @param {*} params
     * @returns
     */
    autoPassFriendsModify(params) {
        return axios({
            url: `${API_MODULE_V}pass/user`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 全局自动回复--好友自动回复列表--获取
     *
     * @param {*} params
     * @returns
     */
    globalReplayFriendsListGet(params) {
        return axios({
            url: `${API_MODULE_V}global/reply`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 全局自动回复--好友自动回复列表--新增
     *
     * @param {*} params
     * @returns
     */
    globalReplayFriendsListAdd(params) {
        return axios({
            url: `${API_MODULE_V}global/reply`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 全局自动回复--好友自动回复列表--编辑
     *
     * @param {*} params
     * @returns
     */
    globalReplayFriendsListModify(params) {
        return axios({
            url: `${API_MODULE_V}global/reply`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 全局自动回复--好友自动回复列表--删除
     *
     * @param {*} params
     * @returns
     */
    globalReplayFriendsListDelete(params) {
        return axios({
            url: `${API_MODULE_V}global/reply/${params}`,
            method: 'DELETE'
        })
    },
    /**
     * 全局自动回复--消息自动回复列表--获取
     *
     * @param {*} params
     * @returns
     */
    globalReplayMsgListGet(params) {
        return axios({
            url: `${API_MODULE_V}global/reply/auto/reply`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 全局自动回复--消息自动回复列表--新增
     *
     * @param {*} params
     * @returns
     */
    globalReplayMsgListAdd(params) {
        return axios({
            url: `${API_MODULE_V}global/reply/auto/reply`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 全局自动回复--消息自动回复列表--详情
     *
     * @param {*} params
     * @returns
     */
    globalReplayMsgListDetail(params) {
        return axios({
            url: `${API_MODULE_V}global/reply/auto/reply/${params}`,
            method: 'GET'
        })
    },
    /**
     * 全局自动回复--消息自动回复列表--编辑
     *
     * @param {*} params
     * @returns
     */
    globalReplayMsgListModify(params) {
        return axios({
            url: `${API_MODULE_V}global/reply/auto/reply`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 全局自动回复--消息自动回复列表--删除
     *
     * @param {*} params
     * @returns
     */
    globalReplayMsgListDelete(params) {
        return axios({
            url: `${API_MODULE_V}global/reply/auto/reply/${params}`,
            method: 'DELETE'
        })
    },
    /**
     * 个人号自动回复--列表--获取
     *
     * @param {*} params
     * @returns
     */
    pAutoReplayListGet(params) {
        return axios({
            url: `${API_MODULE_V}person/reply`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 个人号自动回复--列表详情--获取
     *
     * @param {*} params
     * @returns
     */
    pAutoReplayListDetailGet(params) {
        return axios({
            url: `${API_MODULE_V}person/reply/${params}`,
            method: 'GET'
        })
    },
    /**
     * 个人号自动回复--列表详情--编辑
     *
     * @param {*} params
     * @returns
     */
    pAutoReplayListModify(params) {
        return axios({
            url: `${API_MODULE_V}person/reply`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--左侧--公共素材--获取
     *
     * @param {*} params
     * @returns
     */
    mManageGroupCommonGet(params) {
        return axios({
            url: `${API_MODULE_V}material/allgroup`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--左侧--获取全部部门素材分组--获取
     *
     * @param {*} params
     * @returns
     */
    mManageGroupAllGroupGet(params) {
        return axios({
            url: `${API_MODULE_V}material/alldepartment`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 部门数获取
     *
     * @param {*} params
     * @returns
     */
    mManageGroupDepartGet(params) {
        return axios({
            url: `/common/v1.0/search`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--左侧--部门素材--获取
     *
     * @param {*} params
     * @returns
     */
    mManageDepartTreeGet(params) {
        return axios({
            url: `/manager/v1.0/material/department`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--左侧--素材分组--新增
     *
     * @param {*} params
     * @returns
     */
    mManageGroupAdd(params) {
        return axios({
            url: `${API_MODULE_V}material/group`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--左侧--素材分组--编辑
     *
     * @param {*} params
     * @returns
     */
    mManageGroupModify(params) {
        return axios({
            url: `${API_MODULE_V}material/group`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--左侧--素材分组--删除
     *
     * @param {*} params
     * @returns
     */
    mManageGroupDelete(params) {
        return axios({
            url: `${API_MODULE_V}material/group/${params}`,
            method: 'DELETE'
        })
    },
    /**
     * 素材管理--右侧--素材列表--获取
     *
     * @param {*} params
     * @returns
     */
    mManageListGet(params) {
        return axios({
            url: `${API_MODULE_V}material`,
            method: 'GET',
            params: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--右侧--素材列表--详情
     *
     * @param {*} params
     * @returns
     */
    mManageListDetail(params) {
        return axios({
            url: `${API_MODULE_V}material/${params}`,
            method: 'GET'
        })
    },
    /**
     * 素材管理--右侧--素材列表--新增
     *
     * @param {*} params
     * @returns
     */
    mManageLisAdd(params) {
        return axios({
            url: `${API_MODULE_V}material`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--右侧--素材列表--编辑
     *
     * @param {*} params
     * @returns
     */
    mManageLisModify(params) {
        return axios({
            url: `${API_MODULE_V}material`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--右侧--素材列表--删除
     *
     * @param {*} params
     * @returns
     */
    mManageLisDelete(params) {
        return axios({
            url: `${API_MODULE_V}material/del`,
            method: 'POST',
            data: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--右侧--素材列表--排序
     *
     * @param {*} params
     * @returns
     */
    mManageLisSort(params) {
        return axios({
            url: `${API_MODULE_V}material/move`,
            method: 'PUT',
            data: params,
            removeEmpty
        })
    },
    /**
     * 素材管理--右侧--素材列表--打印日志
     *
     * @param {*} params
     * @returns
     */
    uploadLogMsg(params) {
        return axios({
            url: `${API_MODULE_V}equipement/realTimeLog/update`,
            method: 'POST',
            data: params,
        })
    },
    /**
     * 素材管理--右侧--素材列表--批量打印日志
     *
     * @param {*} params
     * @returns
     */
    batchUploadLogMsg(params) {
        return axios({
            url: `${API_MODULE_V}equipement/realTimeLog/update`,
            method: 'POST',
            data: params,
        })
    }
}