import axios from 'axios'
const API_MODULE_V = `/manager/v2.0/`
const API_MODULE_A = `/manager/v1.0/`
const API_MODULE_C = `/common/v1.0/`
const removeEmpty = true
export default {
  /**
   * 钱包--账户设置--微码微控账户--获取红包费率
   *
   * @param {*} params
   * @returns
   */
  getredPage(params) {
    return axios({
      url: `${API_MODULE_C}config/0/redpage`,
      method: 'GET'
    })
  },
  /**
   * 钱包--账户设置--微码微控账户--获取企业余额
   *
   * @param {*} params
   * @returns
   */
  getAccountPrice(params) {
    return axios({
      url: `${API_MODULE_A}redpage_recharge/money`,
      method: 'GET'
    })
  },
  /**
   * 钱包--账户设置--微码微控账户--获取公众号列表
   *
   * @param {*} params
   * @returns
   */
  getAccountList(params) {
    return axios({
      url: `${API_MODULE_V}wxpay`,
      method: 'GET',
      params: params
    })
  },
  /**
   * 钱包--账户设置--配置支付账户--校验文件名
   *
   * @param {*} params
   * @returns
   */
  checkFileName(params) {
    return axios({
      url: `${API_MODULE_V}wxpay/upload_file`,
      method: 'PUT',
      data: params,
      removeEmpty
    })
  },
  /**
   * 钱包--账户设置--配置支付账户--上传证书
   *
   * @param {*} params
   * @returns
   */
  certificateUploadFile(data, params) {
    return axios({
      url: `${API_MODULE_V}wxpay/upload_cer/${params.id}`,
      method: 'PUT',
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      data
    })
  },
  /**
   * 钱包--账户设置--配置支付账户--提交配置信息
   *
   * @param {*} params
   * @returns
   */
  submitAccountInfo(params) {
    return axios({
      url: `${API_MODULE_V}wxpay/setting`,
      method: 'PUT',
      data: params,
      removeEmpty
    })
  },
  /**
   * 钱包--账户设置--配置支付账户--检测是否配置成功
   *
   * @param {*} params
   * @returns
   */
  checkWxpayAccountInfo(params) {
    return axios({
      url: `${API_MODULE_V}wxpay/check/${params.id}/${params.uuid}`,
      method: 'GET',
      removeEmpty
    })
  },
  /**
   * 钱包--账户设置--配置支付账户--切换支付方式
   *
   * @param {*} params
   * @returns
   */
  changePayMethod(params) {
    return axios({
      url: `${API_MODULE_V}wxpay/switch_pay_type`,
      method: 'POST',
      data: params,
      removeEmpty
    })
  },
  /**
   * 钱包--账户设置--新增公众号
   *
   * @param {*} params
   * @returns
   */
  addWechatAccount(params) {
    return axios({
      url: `${API_MODULE_V}wxpay`,
      method: 'POST',
      data: params,
      removeEmpty
    })
  },
  /**
   * 钱包--账户设置--修改公众号
   *
   * @param {*} params
   * @returns
   */
  editWechatAccount(params) {
    return axios({
      url: `${API_MODULE_V}wxpay`,
      method: 'PUT',
      data: params,
      removeEmpty
    })
  },
  /**
   * 钱包--账户设置--消费记录
   *
   * @param {*} params
   * @returns
   */
  getWalletRecords(params) {
    return axios({
      url: `${API_MODULE_V}wallet/records`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 钱包--获取账户余额列表
   *
   * @param {*} params
   * @returns
   */
  getWalletBalance(params) {
    return axios({
      url: `${API_MODULE_V}wallet/balance`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 钱包--短信余额--短信充值--获取短信充值记录列表
   *
   * @param {*} params
   * @returns
   */
  getSmsRecords(params) {
    return axios({
      url: `${API_MODULE_V}message_recharge`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 钱包--短信余额--短信条数
   *
   * @param {*} params
   * @returns
   */
  getSmsNum(params) {
    return axios({
      url: `${API_MODULE_V}message_recharge/residue`,
      method: 'GET'
    })
  },
  /**
   * 钱包--短信余额--短信充值--获取短信充值套餐列表
   *
   * @param {*} params
   * @returns
   */
  getMealList(params) {
    return axios({
      url: `${API_MODULE_V}message_recharge/message_pageage`,
      method: 'GET',
      params: params,
      removeEmpty
    })
  },
  /**
   * 钱包--短信余额--短信充值--确定充值
   *
   * @param {*} params
   * @returns
   */
  confirAddSmsMeal(params) {
    return axios({
      url: `${API_MODULE_V}message_recharge/charge_info`,
      method: 'POST',
      data: params,
      removeEmpty
    })
  }
}
