import axios from 'axios'
const API_MODULE = `/manager/v1.0`

export default {
  /**
   * 获取通知列表
   *
   * @param {*} params
   * @returns
   */
  getNoticeList(params) {
    return axios({
      url: `${API_MODULE}/notification`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取通知详情
   *
   * @param {*} params
   * @returns
   */
  getNoticeDetail(id) {
    return axios({
      url: `${API_MODULE}/notification/${id}`,
      method: 'GET'
    })
  },
  /**
   * 获得未读记录数
   */
  getNoticeNum() {
    return axios({
      url: `${API_MODULE}/notification/num`,
      method: 'GET'
    })
  }
}
