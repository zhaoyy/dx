import axios from 'axios'
const API_MODULE_V = `/manager/v4.0/`
const removeEmpty = true

export default {
  /**
   * 风控--风控权限--使用管理--静默安装/卸载--安装
   *
   * @param {*} params
   * @returns
   */
  installApp(params) {
    return axios({
      url: `${API_MODULE_V}equipement_app/install`,
      method: 'POST',
      data: params,
      removeEmpty
    })
  },
  /**
   * 风控--风控权限--使用管理--静默安装/卸载--设备列表
   *
   * @param {*} params
   * @returns
   */
  getAppList(params) {
    return axios({
      url: `${API_MODULE_V}equipement_app`,
      method: 'PUT',
      data: params.params,
      removeEmpty
    })
  },
  /**
   * 风控--风控权限--使用管理--静默安装/卸载--卸载
   *
   * @param {*} params
   * @returns
   */
  unInstallAppList(params) {
    return axios({
      url: `${API_MODULE_V}equipement_app/uninstall`,
      method: 'POST',
      data: params,
      removeEmpty
    })
  },
  /**
   * 风控--风控权限--使用管理--静默安装/卸载--禁止卸载
   *
   * @param {*} params
   * @returns
   */
  forbiddenUnInstallAppList(params) {
    return axios({
      url: `${API_MODULE_V}equipement_app/donotuninstall`,
      method: 'POST',
      data: params,
      removeEmpty
    })
  }
}
