import axios from 'axios'
const API_MODULE_V = `/proxy/v1.0`

export default {
  /**
   * 获取消息通知列表
   * @param {*} params
   */
  getNoticeList(params) {
    return axios({
      url: `${API_MODULE_V}/index/notification`,
      method: 'GET',
      removeEmpty: true,
      params: params
    })
  },
  /**
   * 获取消息通知详情
   * @param {*} params
   */
  getNoticeDetails(params) {
    return axios({
      url: `${API_MODULE_V}/index/notification/${params}`,
      method: 'GET'
    })
  },
  /**
   * 获得未读记录数
   */
  getNoticeNum() {
    return axios({
      url: `${API_MODULE_V}/index/notification/num`,
      method: 'GET'
    })
  }
}
