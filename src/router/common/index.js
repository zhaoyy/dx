import * as types from './types'
import commonRouter from './router'
import router from '../router'

// 是否为公共页面的路由
export const isCommonRouter = routerName => Object.keys(types).some(key => types[key] === routerName)

// 重定向到维护中页面
export const redirectToCommonMaintain = () => {
  router.replace({
    name: types.MAINTAIN
  })
}

export default commonRouter
