const PREFIX = 'COMMON'

// 404
export const PAGE404 = `${PREFIX}/PAGE404`
// 菜单列表
export const MENU = `${PREFIX}/MENU`
// 维护中
export const MAINTAIN = `${PREFIX}/MAINTAIN`
