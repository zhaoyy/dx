import {
  isCommonRouter,
  redirectToCommonMaintain
} from './common'
import {
  isLoginRouter,
  redirectToSmsLogin,
  redirectToOmsLogin,
  redirectToPmsLogin,
  redirectToSystemLogin,
  redirectToChatLogin
} from './login'
import {
  isSmsRouter,
  redirectToSmsHome
} from './sms'
import {
  isOmsRouter,
  redirectToOmsHome
} from './oms'
import {
  isPmsRouter,
  redirectToPmsHome
} from './pms'
import {
  isSystemRouter,
  redirectToSystemHome
} from './system'
import {
  isChatRouter,
  redirectToChatHome
} from './chat'
import {
  adminInfoSession,
  loginTypeLocal
} from '@/storage'
import router, {
  isMainRouter
} from './router'
import store from '@/store'
import * as mutationsType from '@/store/mutation-types'
import {
  homeApi
} from '@/api/common'
import {
  commonApi
} from '@/api/chat'
import {
  isDev
} from '@/config'
import {
  LOGIN_TYPE
} from '@/constants'
import {
  Notify
} from 'vma-vue-element'

// 路由前置钩子
router.beforeEach((to, from, next) => {
  // 1、开发环境 ui 页面放行
  // 2、common页面放行
  // 3、后台框架页放行
  if (!to.name || (isDev && /^\/ui/.test(to.fullPath)) || isCommonRouter(to.name) || isMainRouter(to.name)) {
    return next()
  }

  /**
   * 重定向到登录页或进入下一个路由
   *
   * @returns
   */
  function redirectToLoginOrNext() {
    // 用户未登录
    if (shouldRedirectToLogin(to.name)) {
      return redirectToLoign()
    } else {
      next()
    }
  }
  if (to.query.token) {
    let getToken = to.query.token
    let params = {
      type: 4,
      domain: location.hostname,
      token: getToken,
      isLogin: false
    }
    commonApi.taibaoLogin(params).then(res => {
      let taibaoData = {
        accountId: res.accountId,
        id: res.wxUserId,
        wxId: res.wxId,
        talkerWxId: res.talkerWxId
      }
      // 无论在不在微客服页面都重定向到微客服页面
      redirectToChatHome()
      store.commit(mutationsType.SAVE_LOGIN_INFO, taibaoData)
    })
  }
  // 用户已登录
  if (adminInfoSession.hasData()) {
    // 需要重定向到后台首页
    // 1、前往的是登录页
    if (isLoginRouter(to.name)) {
      return redirectToHome()
    }
    const adminInfo = adminInfoSession.getJSON()
    // 2、没有菜单权限
    // 3、前往的后台地址和当前登录类型不匹配
    if (!hasMenuPermission(to.name, adminInfo.resourceMenuList) ||
      !isRouterExists(to.name)) {
      Notify.warn('没有当前菜单权限')
      return redirectToHome()
    }
    return next()
  } else {
    // 用户未登录
    // 同步一次服务端用户数据，确认是否未登录，以服务端用户数据为准
    homeApi.getCurrent().then(data => {
      if (data && data.macKey) {
        store.dispatch('afterLogin', data)
        if (isLoginRouter(to.name)) {
          return redirectToHome()
        }
        return next()
      } else {
        return redirectToLoginOrNext()
      }
    }, err => {
      redirectToLoginOrNext()
      Promise.reject(err)
    })
  }
})

// 路由后置钩子
router.afterEach(to => {
  window.scrollTo(0, 0)
})

/**
 * 根据路由名称是否需要重定向到登录页
 * @param routerName
 */
export const shouldRedirectToLogin = routerName => {
  return routerName && !isCommonRouter(routerName) && !isLoginRouter(routerName)
}

/**
 * 重定向到登录页
 */
export const redirectToLoign = () => {
  store.dispatch('afterLogout')
  const loginType = loginTypeLocal.getOrDefault()
  // 根据账号类型重定向到不同的后台登录页
  switch (loginType) {
    case LOGIN_TYPE.SYSTEM:
      redirectToSystemLogin()
      break
    case LOGIN_TYPE.CHAT:
      redirectToChatLogin()
      break
    case LOGIN_TYPE.OMS:
      redirectToOmsLogin()
      break
    case LOGIN_TYPE.SMS:
      redirectToSmsLogin()
      break
    case LOGIN_TYPE.PMS:
      redirectToPmsLogin()
      break
    default:
      redirectToSystemLogin()
  }
}

/**
 * 重定向到首页
 */
export const redirectToHome = () => {
  const loginType = loginTypeLocal.getOrDefault()
  // 根据账号类型重定向到不同的后台首页
  switch (loginType) {
    case LOGIN_TYPE.SYSTEM:
      redirectToSystemHome()
      break
    case LOGIN_TYPE.CHAT:
      redirectToChatHome()
      break
    case LOGIN_TYPE.OMS:
      redirectToOmsHome()
      break
    case LOGIN_TYPE.SMS:
      redirectToSmsHome()
      break
    case LOGIN_TYPE.PMS:
      redirectToPmsHome()
      break
    default:
      redirectToSystemHome()
  }
}

/**
 * 是否拥有菜单权限
 * @param {*} routerName
 * @param {*} params
 */
const excludedMenuList = []
export const hasMenuPermission = (routerName, params) => {
  if (routerName &&
    !isCommonRouter(routerName) &&
    !isChatRouter(routerName) &&
    !excludedMenuList.includes(routerName) &&
    params && params.length) {
    return params.some(menu => {
      if (menu.url === routerName) {
        return true
      } else if (menu.node && menu.node.length) {
        return hasMenuPermission(routerName, menu.node)
      }
    })
  }
  return true
}

/**
 * 路由页面是否存在
 * @param {*} routerName
 */
export const isRouterExists = routerName => {
  if (routerName) {
    const loginType = Number(loginTypeLocal.getOrDefault())
    switch (loginType) {
      case LOGIN_TYPE.SYSTEM:
        return isSystemRouter(routerName)
      case LOGIN_TYPE.CHAT:
        return isChatRouter(routerName)
      case LOGIN_TYPE.OMS:
        return isOmsRouter(routerName)
      case LOGIN_TYPE.SMS:
        return isSmsRouter(routerName)
      case LOGIN_TYPE.PMS:
        return isPmsRouter(routerName)
    }
  }
  return false
}

export {
  redirectToSmsLogin,
  redirectToOmsLogin,
  redirectToPmsLogin,
  redirectToSystemLogin,
  redirectToChatLogin,
  redirectToSmsHome,
  redirectToOmsHome,
  redirectToPmsHome,
  redirectToSystemHome,
  redirectToChatHome,
  redirectToCommonMaintain
}

export default router
