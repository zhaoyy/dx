import * as types from './types'
import {
  resolveRouterComponent
} from '@/utils'

export default [{
  path: 'home',
  name: types.HOME,
  component: () => resolveRouterComponent(import('@/pages/sms/home/home.vue'))
}, {
  path: 'oemList',
  name: types.OEM_LIST,
  component: () => resolveRouterComponent(import('@/pages/sms/customerManage/oemList/oemList.vue'))
}, {
  path: 'phoneList',
  name: types.PHONE_LIST,
  component: () => resolveRouterComponent(import('@/pages/sms/customerManage/phoneList.vue'))
}, 
{
  path: 'newOem',
  name: types.NEW_OEM,
  component: () => resolveRouterComponent(import('@/pages/sms/customerManage/oemList/newOem.vue'))
}, {
  path: 'enterpriseList',
  name: types.ENTERPRISE_LIST,
  component: () => resolveRouterComponent(import('@/pages/sms/customerManage/enterpriseList.vue'))
}, {
  path: 'omsList',
  name: types.OMS_LIST,
  component: () => resolveRouterComponent(import('@/pages/sms/customerManage/omsList.vue'))
}, {
  path: 'smsTemplateAudit',
  name: types.SMS_TEMPLATE_AUDIT,
  component: () => resolveRouterComponent(import('@/pages/sms/smsTemplateAudit/smsTemplateAudit.vue'))
}, {
  path: 'packetsRecharge',
  name: types.PACKETS_RECHARGE,
  component: () => resolveRouterComponent(import('@/pages/sms/rechargeDetails/packetsDetails/packetsRecharge.vue'))
}, {
  path: 'packetsReceive',
  name: types.PACKETS_RECEIVE,
  component: () => resolveRouterComponent(import('@/pages/sms/rechargeDetails/packetsDetails/packetsReceive.vue'))
}, {
  path: 'showPicture',
  name: types.SHOW_PICTURE,
  component: () => resolveRouterComponent(import('@/pages/sms/rechargeDetails/packetsDetails/showPicture.vue'))
}, {
  path: 'smsRecharge',
  name: types.SMS_RECHARGE,
  component: () => resolveRouterComponent(import('@/pages/sms/rechargeDetails/smsDetails/smsRecharge.vue'))
}, {
  path: 'smsSend',
  name: types.SMS_SEND,
  component: () => resolveRouterComponent(import('@/pages/sms/rechargeDetails/smsDetails/smsSend.vue'))
}, {
  path: 'systemSettings',
  name: types.SYSTEM_SETTINGS,
  component: () => resolveRouterComponent(import('@/pages/sms/systemSettings/systemSettings.vue'))
}, {
  path: 'appManage',
  name: types.APP_MANAGE,
  component: () => resolveRouterComponent(import('@/pages/sms/systemSettings/appManage.vue'))
}, {
  path: 'packageManage',
  name: types.PACKAGE_MANAGE,
  component: () => resolveRouterComponent(import('@/pages/sms/systemSettings/packageManage.vue'))
}, {
  path: 'domainConfig',
  name: types.DOMAIN_CONFIG,
  component: () => resolveRouterComponent(import('@/pages/sms/systemSettings/domainConfig.vue'))
}]
