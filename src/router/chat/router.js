import * as types from './types'
import {
    resolveRouterComponent
} from '@/utils'

export default [{
    path: 'home',
    name: types.HOME,
    component: () => resolveRouterComponent(
        import ('@/pages/chat/home/home.vue'))
}]