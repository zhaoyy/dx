import * as types from './types'
import router from '../router'
import {
  loginTypeLocal
} from '@/storage'
import pmsRouter from './router'

// 是否为运营商后台的路由
export const isPmsRouter = routerName => Object.keys(types).some(key => types[key] === routerName)

// 重定向到首页
export const redirectToPmsHome = () => {
  loginTypeLocal.pms()
  router.replace({
    name: types.MAIN
  })
}

export default pmsRouter
