import * as types from './types'
import {
  resolveRouterComponent
} from '@/utils'

export default [{
  path: 'home',
  name: types.HOME,
  component: () => resolveRouterComponent(import('@/pages/pms/home/home.vue'))
}, {
  path: 'notice',
  name: types.NOTICE,
  component: () => resolveRouterComponent(import('@/pages/pms/home/notice.vue'))
}, {
  path: 'businessList',
  name: types.BUSINESS_LIST,
  component: () => resolveRouterComponent(import('@/pages/pms/businessManage/business/businessList.vue'))
}]
