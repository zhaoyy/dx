const PREFIX = 'LOGIN'

export const LOGIN = `${PREFIX}/LOGIN`
export const LOGIN_SMS = `${PREFIX}/LOGIN_SMS`
export const LOGIN_OMS = `${PREFIX}/LOGIN_OMS`
export const LOGIN_PMS = `${PREFIX}/LOGIN_PMS`
export const LOGIN_SYSTEM = `${PREFIX}/LOGIN_SYSTEM`
