import * as types from './types'
import {
  resolveRouterComponent
} from '@/utils'

export default [{
  path: '/sms/login',
  name: types.LOGIN_SMS,
  component: () => resolveRouterComponent(import('@/pages/login/smsLogin'))
}, {
  path: '/oms/login',
  name: types.LOGIN_OMS,
  component: () => resolveRouterComponent(import('@/pages/login/omsLogin'))
}, {
  path: '/pms/login',
  name: types.LOGIN_PMS,
  component: () => resolveRouterComponent(import('@/pages/login/pmsLogin'))
}, {
  path: '/system/login',
  name: types.LOGIN_SYSTEM,
  component: () => resolveRouterComponent(import('@/pages/login/systemLogin'))
}, {
  path: '/',
  redirect: {
    name: types.LOGIN_SYSTEM
  }
}]
