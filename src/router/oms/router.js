import * as types from './types'
import {
    resolveRouterComponent
} from '@/utils'

export default [{
    path: 'home',
    name: types.HOME,
    component: () => resolveRouterComponent(
        import ('@/pages/oms/home/home.vue'))
}, {
    path: 'notice',
    name: types.NOTICE,
    component: () => resolveRouterComponent(
        import ('@/pages/oms/home/notice.vue'))
}, {
    path: 'businessList',
    name: types.BUSINESS_LIST,
    component: () => resolveRouterComponent(
        import ('@/pages/oms/businessManage/business/businessList.vue'))
}, {
    path: 'agentList',
    name: types.AGENT_LIST,
    component: () => resolveRouterComponent(
        import ('@/pages/oms/businessManage/agent/agentList.vue'))
}, {
    path: 'agentAdd',
    name: types.AGENT_ADD,
    component: () => resolveRouterComponent(
        import ('@/pages/oms/businessManage/agent/agentAdd.vue'))
}, {
    path: 'operate',
    name: types.WX_OPERATE,
    component: () => resolveRouterComponent(
        import ('@/pages/oms/wxOperates/tableList/tableData.vue'))
}]