const PREFIX = 'OMS'

// 框架页
export const MAIN = `${PREFIX}/MAIN`
    // 首页
export const HOME = `${PREFIX}/HOME`
    // 通知
export const NOTICE = `${PREFIX}/NOTICE`
    // 企业列表
export const BUSINESS_LIST = `${PREFIX}/BUSINESS_LIST`
    // 代理商列表
export const AGENT_LIST = `${PREFIX}/AGENT_LIST`
    // 新增代理商
export const AGENT_ADD = `${PREFIX}/AGENT_ADD`
    //微信运营报表
export const WX_OPERATE = `${PREFIX}/WX_OPERATE`