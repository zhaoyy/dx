import * as types from './types'
import router from '../router'
import {
  loginTypeLocal
} from '@/storage'
import omsRouter from './router'

// 是否为代理商后台的路由
export const isOmsRouter = routerName => Object.keys(types).some(key => types[key] === routerName)

// 重定向到首页
export const redirectToOmsHome = () => {
  loginTypeLocal.oms()
  router.replace({
    name: types.MAIN
  })
}

export default omsRouter
