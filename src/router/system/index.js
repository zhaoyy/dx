import * as types from './types'
import router from '../router'
import {
  loginTypeLocal
} from '@/storage'
import systemRouter from './router'

// 是否为system路由
export const isSystemRouter = routerName => Object.keys(types).some(key => types[key] === routerName)

// 重定向到首页
export const redirectToSystemHome = () => {
  loginTypeLocal.system()
  router.replace({
    name: types.MAIN
  })
}

export default systemRouter
