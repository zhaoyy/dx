import * as types from './types'
import {
  resolveRouterComponent
} from '@/utils'

export default [{
    path: 'home',
    name: types.HOME,
    component: () => resolveRouterComponent(
      import('@/pages/system/homePage/home.vue'))
  },
  {
    path: 'dataCenter',
    name: types.DATA_CENTER,
    component: () => resolveRouterComponent(
      import('@/pages/system/home/home/index.vue'))
  },
  {
    path: 'notice',
    name: types.NOTICE,
    component: () => resolveRouterComponent(
      import('@/pages/system/home/notice.vue'))
  },
  {
    path: 'noticeStaff',
    name: types.NOTICESTAFF,
    component: () => resolveRouterComponent(
      import('@/pages/system/home/noticeStaff.vue'))
  },
  {
    path: 'sensitiveWordSet',
    name: types.SENSITIVE_WORD_SET,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/weChatWindControl/sensitiveWordSet.vue'))
  },
  {
    path: 'sensitiveWordGroup',
    name: types.SENSITIVE_WORD_GROUP,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/weChatWindControl/sensitiveWordGroup.vue'))
  },
  {
    path: 'sensitiveOperation',
    name: types.SENSITIVE_OPERATION,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/weChatWindControl/sensitiveOperation.vue'))
  },
  {
    path: 'sensitiveNotice',
    name: types.SENSITIVE_NOTICE,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/weChatWindControl/sensitiveNotice.vue'))
  }, {
    path: 'friendStatistics',
    name: types.FRIEND_STATISTICS,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/weChatWindControl/friendStatistics.vue'))
  }, {
    path: 'groupStatistics',
    name: types.GROUP_STATISTICS,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/weChatWindControl/groupStatistics.vue'))
  }, {
    path: 'friendExport',
    name: types.FRIEND_EXPORT,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/weChatWindControl/friendExport.vue'))
  }, {
    path: 'financialStatistics',
    name: types.FINANCIAL_STATISTICS,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/weChatWindControl/financialStatistics.vue'))
  }, {
    path: 'callRecording',
    name: types.CALL_RECORDING,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/phoneWindControl/callRecording.vue'))
  }, {
    path: 'smsSensitiveWord',
    name: types.SMS_SENSITIVE_WORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/phoneWindControl/smsSensitiveWord.vue'))
  }, {
    path: 'phoneSms',
    name: types.PHONE_SMS,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/phoneWindControl/phoneSms.vue'))
  }, {
    path: 'sensitiveSms',
    name: types.SENSITIVE_SMS,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/phoneWindControl/sensitiveSms.vue'))
  }, {
    path: 'phoneSensitiveNotice',
    name: types.PHONE_SENSITIVE_NOTICE,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/phoneWindControl/phoneSensitiveNotice.vue'))
  }, {
    path: 'appSettings',
    name: types.APP_SETTINGS,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/phoneWindControl/appSettings.vue'))
  }, {
    path: 'authorityManage',
    name: types.AUTHORITY_MANAGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/riskManagement/phoneWindControl/authorityManage.vue'))
  }, {
    path: 'personNumList',
    name: types.PERSON_NUM_LIST,
    component: () => resolveRouterComponent(
      import('@/pages/system/personalNumManage/personNumList.vue'))
  }, {
    path: 'friendsList',
    name: types.FRIENDS_LIST,
    component: () => resolveRouterComponent(
      import('@/pages/system/personalNumManage/friendsList.vue'))
  }, {
    path: 'groupView',
    name: types.GROUP_VIEW,
    component: () => resolveRouterComponent(
      import('@/pages/system/personalNumManage/groupView.vue'))
  }, {
    path: 'groupRecords',
    name: types.GROUP_RECORDS,
    component: () => resolveRouterComponent(
      import('@/pages/system/personalNumManage/groupRecords.vue'))
  }, {
    path: 'groupManage',
    name: types.GROUP_MANAGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/personalNumManage/groupManage.vue'))
  }, {
    path: 'distributionRecord',
    name: types.DISTRIBUTION_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/personalNumManage/distributionRecord.vue'))
  }, {
    path: 'deviceManage',
    name: types.DEVICE_MANAGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/deviceManage.vue'))
  }, {
    path: 'warehouseManage',
    name: types.WAREHOUSE_MANAGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/warehouseManage.vue'))
  }, {
    path: 'deviceGroup',
    name: types.DEVICE_GROUP,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/deviceGroup.vue'))
  }, {
    path: 'deviceDistribution',
    name: types.DEVICE_DISTRIBUTION,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/deviceDistribution.vue'))
  }, {
    path: 'wechatSetting',
    name: types.WECHAT_SETTING,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/wechatSetting.vue'))
  }, {
    path: 'quickReply',
    name: types.QUICK_REPLY,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/quickReply.vue'))
  }, {
    path: 'subordinatesReply',
    name: types.SUBORDINATES_REPLY,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/subordinatesReply.vue'))
  }, {
    path: 'autoFriends',
    name: types.AUTO_FRIENDS,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/autoFriends.vue'))
  }, {
    path: 'globalReply',
    name: types.GLOBAL_REPLY,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/globalReply.vue'))
  }, {
    path: 'personalNumAutoReply',
    name: types.PERSONAL_NUM_AUTO_REPLY,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/personalNumAutoReply.vue'))
  }, {
    path: 'materialManage',
    name: types.MATERIAL_MANAGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/deviceManage/materialManage.vue'))
  }, {
    path: 'packetsRecharge',
    name: types.PACKETS_RECHARGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/redPacketMarket/packetsRecharge.vue'))
  }, {
    path: 'packetsRecord',
    name: types.PACKETS_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/redPacketMarket/packetsRecord.vue'))
  }, {
    path: 'showPicture',
    name: types.SHOW_PICTURE,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/showPicture/showPicture.vue'))
  }, {
    path: 'activityAudit',
    name: types.ACTIVITY_AUDIT,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/showPicture/activityAudit.vue'))
  }, {
    path: 'activityRecord',
    name: types.ACTIVITY_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/showPicture/activityRecord.vue'))
  }, {
    path: 'smsRecharge',
    name: types.SMS_RECHARGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/smsMarket/smsRecharge.vue'))
  }, {
    path: 'groupSend',
    name: types.GROUP_SEND,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/smsMarket/groupSend.vue'))
  }, {
    path: 'historyTemplate',
    name: types.HISTORY_TEMPLATE,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/smsMarket/historyTemplate.vue'))
  }, {
    path: 'sendRecord',
    name: types.SEND_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/smsMarket/sendRecord.vue'))
  }, {
    path: 'regularCircle',
    name: types.REGULAR_CIRCLE,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/regularCircle.vue'))
  },
  {
    path: 'hairBandsRecord',
    name: types.RECORD_CIRCLE,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/hairBandsRecord/hairBandsRecord.vue'))
  },
  {
    path: 'messageCheck',
    name: types.MESSAGE_CHECK,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/hairBandsRecord/messageCheck.vue'))
  },
  {
    path: 'microJump',
    name: types.MICRO_JUMP,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/microJump.vue'))
  }, {
    path: 'autoPraise',
    name: types.AUTO_PRAISE,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/autoPraise.vue'))
  }, {
    path: 'firstBindList',
    name: types.FIRST_BIND_LIST,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/firstBindCourtesy/firstBindList.vue'))
  }, {
    path: 'firstBindRecord',
    name: types.FIRST_BIND_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/firstBindCourtesy/firstBindRecord.vue'))
  }, {
    path: 'reviewList',
    name: types.REVIEW_LIST,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/reviewCourtesy/reviewList.vue'))
  }, {
    path: 'reviewActivityAudit',
    name: types.REVIEW_ACTIVITY_AUDIT,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/reviewCourtesy/activityAudit.vue'))
  }, {
    path: 'reviewRecord',
    name: types.REVIEW_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/reviewCourtesy/reviewRecord.vue'))
  }, {
    path: 'addPurchaseList',
    name: types.ADD_PURCHASE_LIST,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/addPurchaseCourtesy/addPurchaseList.vue'))
  }, {
    path: 'addActivityAudit',
    name: types.ADD_ACTIVITY_AUDIT,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/addPurchaseCourtesy/activityAudit.vue'))
  }, {
    path: 'addPurchaseRecord',
    name: types.ADD_PURCHASE_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/addPurchaseCourtesy/addPurchaseRecord.vue'))
  }, {
    path: 'collectionList',
    name: types.COLLECTION_LIST,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/collectionCourtesy/collectionList.vue'))
  }, {
    path: 'collectionActivityAudit',
    name: types.COLLECTION_ACTIVITY_AUDIT,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/collectionCourtesy/activityAudit.vue'))
  }, {
    path: 'collectionRecord',
    name: types.COLLECTION_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/collectionCourtesy/collectionRecord.vue'))
  }, {
    path: 'newVotingList',
    name: types.NEW_VOTING_LIST,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/newVoting/newVotingList.vue'))
  }, {
    path: 'newVotingActivityAudit',
    name: types.NEW_VOTING_ACTIVITY_AUDIT,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/newVoting/activityAudit.vue'))
  }, {
    path: 'newVotingRecord',
    name: types.NEW_VOTING_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/newVoting/newVotingRecord.vue'))
  }, {
    path: 'buyingShowList',
    name: types.BUYING_SHOW_LIST,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/buyingShow/buyingShowList.vue'))
  }, {
    path: 'buyingActivityAudit',
    name: types.BUYING_ACTIVITY_AUDIT,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/buyingShow/activityAudit.vue'))
  }, {
    path: 'buyingShowRecord',
    name: types.BUYING_SHOW_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/marketPlatform/buyingShow/buyingShowRecord.vue'))
  }, {
    path: 'phonePlus',
    name: types.PHONE_PLUS,
    component: () => resolveRouterComponent(
      import('@/pages/system/powderManage/phonePlus.vue'))
  }, {
    path: 'timingPlus',
    name: types.TIMING_PLUS,
    component: () => resolveRouterComponent(
      import('@/pages/system/powderManage/timingPlus.vue'))
  }, {
    path: 'microCode',
    name: types.MICRO_CODE,
    component: () => resolveRouterComponent(
      import('@/pages/system/powderManage/microCode.vue'))
  }, {
    path: 'storePlus',
    name: types.STORES_PLUS,
    component: () => resolveRouterComponent(
      import('@/pages/system/powderManage/storePlus.vue'))
  }, {
    path: 'departmentManage',
    name: types.DEPARTMENT_MANAGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/staffManage/departmentManage.vue'))
  }, {
    path: 'roleManage',
    name: types.ROLE_MANAGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/staffManage/roleManage.vue'))
  }, {
    path: 'storeList',
    name: types.STORE_LIST,
    component: () => resolveRouterComponent(
      import('@/pages/system/ecommercePlatform/storeList.vue'))
  }, {
    path: 'storeOrder',
    name: types.STORE_ORDER,
    component: () => resolveRouterComponent(
      import('@/pages/system/ecommercePlatform/storeOrder.vue'))
  }, {
    path: 'operationLog',
    name: types.OPERATION_LOG,
    component: () => resolveRouterComponent(
      import('@/pages/system/operationLog/operationLog.vue'))
  }, {
    path: 'importOrder',
    name: types.IMPORT_ORDER,
    component: () => resolveRouterComponent(
      import('@/pages/system/ecommercePlatform/importOrder.vue'))
  }, {
    path: 'walletAccountSetting',
    name: types.ACCOUNT_SETTINGS,
    component: () => resolveRouterComponent(
      import('@/pages/system/wallet/balanceAccount/accountSettings.vue'))
  }, {
    path: 'mControlAccountBalance',
    name: types.M_CONTROL_ACCOUNT_BALANCE,
    component: () => resolveRouterComponent(
      import('@/pages/system/wallet/balanceAccount/mControlAccountBalance.vue'))
  }, {
    path: 'mCustomerRedBagRecord',
    name: types.M_CUSTOMER_RED_RECORD,
    component: () => resolveRouterComponent(
      import('@/pages/system/wallet/balanceAccount/mCustomerRedBagRecord.vue'))
  }, {
    path: 'walletSmsRecharge',
    name: types.WALLET_SMS_RECHARGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/wallet/balanceSms/walletSmsRecharge.vue'))
  }, {
    path: 'automaticPassing',
    name: types.AUTOMATIC_PASSING,
    component: () => resolveRouterComponent(
      import('@/pages/system/communityManage/drainage/automaticPassing.vue'))
  }, {
    path: 'automaticRecovery',
    name: types.AUTOMATIC_RECOVERY,
    component: () => resolveRouterComponent(
      import('@/pages/system/communityManage/drainage/automaticRecovery.vue'))
  }, {
    path: 'greetingGroup',
    name: types.GREETING_GROUP,
    component: () => resolveRouterComponent(
      import('@/pages/system/communityManage/drainage/greetingGroup.vue'))
  }, {
    path: 'groupActivity',
    name: types.GROUP_ACTIVITY,
    component: () => resolveRouterComponent(
      import('@/pages/system/communityManage/drainage/groupActivity.vue'))
  }, {
    path: 'announcement',
    name: types.ANNOUNCEMENT,
    component: () => resolveRouterComponent(
      import('@/pages/system/communityManage/manage/announcement/index.vue'))
  }, {
    path: 'behaviorManage',
    name: types.BEHAVIOR_MANAGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/communityManage/manage/behaviorManage.vue'))
  }, {
    path: 'groupsManage',
    name: types.GROUPS_MANAGE,
    component: () => resolveRouterComponent(
      import('@/pages/system/communityManage/manage/groupsManage.vue'))
  }, {
    path: 'groupsStatistics',
    name: types.GROUPS_STATISTICS,
    component: () => resolveRouterComponent(
      import('@/pages/system/communityManage/statistics/groupsStatistics/index.vue'))
  }, {
    path: 'interactiveStatistics',
    name: types.INTERACTIVE_STATISTICS,
    component: () => resolveRouterComponent(
      import('@/pages/system/communityManage/statistics/interactiveStatistics/home/index.vue'))
  }, {
    path: 'singleGroupStatistics',
    name: types.SINGLE_GROUP_STATISTICS,
    component: () => resolveRouterComponent(
      import('@/pages/system/communityManage/statistics/singleGroupStatistics/index.vue'))
  }, {
    path: 'systemConfig',
    name: types.SYSTEM_CONFIG,
    component: () => resolveRouterComponent(
      import('@/pages/system/config/systemConfig.vue'))
  }
]
