import * as types from './types'

export default [{
  path: 'login',
  name: types.LOGIN,
  component: resolve => require(['@/pages/ui/login'], resolve)
}]
