const PREFIX = 'UI/SYSTEM'

export const DEMO = `${PREFIX}/DEMO`
// 登录
export const LOGIN = `${PREFIX}/LOGIN`
// 首页
export const HOME = `${PREFIX}/HOME`
// 首页(v2.2.1版本)
export const HOME_V221 = `${PREFIX}/HOME_v221`
// 通知
export const NOTICE = `${PREFIX}/NOTICE`
// 通知详情
export const NOTICE_DETAIL = `${PREFIX}/NOTICE_DETAIL`
// 设备管理-设备管理
export const DEVICE_MANAGE = `${PREFIX}/DEVICE_MANAGEMENT`
// 设备分组
export const DEVICE_GROUP = `${PREFIX}/DEVICE_GROUP`
// 分配记录
export const DEVICE_DISTRIBUTION = `${PREFIX}/DEVICE_DISTRIBUTION`
// 微信设置
export const WECHAT_SETTING = `${PREFIX}/WECHAT_SETTING`
// 快捷回复
export const QUICK_REPLY = `${PREFIX}/QUICK_REPLY`
// 下属快捷回复
export const SUBORDINATES_REPLY = `${PREFIX}/SUBORDINATES_REPLY`
// 自动通过好友
export const AUTO_FRIENDS = `${PREFIX}/AUTO_FRIENDS`
// 全局自动回复
export const GLOBAL_REPLY = `${PREFIX}/GLOBAL_REPLY`
// 个人号自动回复
export const PERSONAL_NUM_AUTO_REPLY = `${PREFIX}/PERSONAL_NUM_AUTO_REPLY`
// 素材管理
export const MATERIAL_MANAGE = `${PREFIX}/MATERIAL_MANAGE`
// 电商平台-店面列表
export const STORE_LIST = `${PREFIX}/STORE_LIST`
// 店面订单
export const STORE_ORDER = `${PREFIX}/STORE_ORDER`
// 导入订单
export const IMPORT_ORDER = `${PREFIX}/IMPORT_ORDER`
// 营销平台-定时发朋友圈
export const REGULAR_CIRCLE = `${PREFIX}/REGULAR_CIRCLE`
// 微跳转
export const MICRO_JUMP = `${PREFIX}/MICRO_JUMP`
// 朋友圈自动加好友
export const AUTO_PRAISE = `${PREFIX}/AUTO_PRAISE`
// 红包充值
export const PACKETS_RECHARGE = `${PREFIX}/PACKETS_RECHARGE`
// 红包记录
export const PACKETS_RECORD = `${PREFIX}/PACKETS_RECORD`
// 短信充值
export const SMS_RECHARGE = `${PREFIX}/SMS_RECHARGE`
// 晒图活动
export const SHOW_PICTURE = `${PREFIX}/SHOW_PICTURE`
// 新增活动
export const NEW_ACTIVITY = `${PREFIX}/NEW_ACTIVITY`
// 手机H5展示
export const H5_SHOW = `${PREFIX}/H5_SHOW`
// 活动审核
export const ACTIVITY_AUDIT = `${PREFIX}/ACTIVITY_AUDIT`
// 活动支出记录
export const ACTIVITY_RECORD = `${PREFIX}/ACTIVITY_RECORD`
// 短信群发
export const GROUP_SEND = `${PREFIX}/GROUP_SEND`
// 短信历史模板
export const HISTORY_TEMPLATE = `${PREFIX}/HISTORY_TEMPLATE`
// 短信发送记录
export const SEND_RECORD = `${PREFIX}/SEND_RECORD`
// 操作日志
export const OPERATION_LOG = `${PREFIX}/OPERATION_LOG`
// 个人号管理-个人号列表
export const PERSON_NUM_LIST = `${PREFIX}/PERSON_NUM_LIST`
// 微信好友列表
export const FRIENDS_LIST = `${PREFIX}/FRIENDS_LIST`
// 微信群查看
export const GROUP_VIEW = `${PREFIX}/GROUP_VIEW`
// 分组管理
export const GROUP_MANAGE = `${PREFIX}/GROUP_MANAGE`
// 分配记录
export const DISTRIBUTION_RECORD = `${PREFIX}/DISTRIBUTION_RECORD`
// 加粉管理-手机号加好友
export const PHONE_PLUS = `${PREFIX}/PHONE_PLUS_FRIEND`
// 定时加好友
export const TIMING_PLUS = `${PREFIX}/TIMING_PLUS_FRIEND`
// 微活码
export const MICRO_CODE = `${PREFIX}/MICRO_CODE`
// 店铺自动加好友
export const STORES_PLUS = `${PREFIX}/STORES_PLUS`
// 风控管理-敏感词设置
export const SENSITIVE_WORD_SET = `${PREFIX}/SENSITIVE_WORD_SET`
// 敏感操作
export const SENSITIVE_OPERATION = `${PREFIX}/SENSITIVE_OPERATION`
// 敏感通知
export const SENSITIVE_NOTICE = `${PREFIX}/SENSITIVE_NOTICE`
// 重复好友统计
export const FRIEND_STATISTICS = `${PREFIX}/FRIEND_STATISTICS`
// 重复群统计
export const GROUP_STATISTICS = `${PREFIX}/GROUP_STATISTICS`
// 微信好友导出
export const FRIEND_EXPORT = `${PREFIX}/FRIEND_EXPORT`
// 微信财务统计
export const FINANCIAL_STATISTICS = `${PREFIX}/FINANCIAL_STATISTICS`
// 通话录音
export const CALL_RECORDING = `${PREFIX}/CALL_RECORDING`
// 短信敏感词
export const SMS_SENSITIVE_WORD = `${PREFIX}/SMS_SENSITIVE_WORD`
// 手机短信
export const PHONE_SMS = `${PREFIX}/PHONE_SMS`
// 敏感短信
export const SENSITIVE_SMS = `${PREFIX}/SENSITIVE_SMS`
// 敏感通知
export const PHONE_SENSITIVE_NOTICE = `${PREFIX}/PHONE_SENSITIVE_NOTICE`
// APP设置
export const APP_SETTINGS = `${PREFIX}/APP_SETTINGS`
// 权限管理
export const AUTHORITY_MANAGE = `${PREFIX}/AUTHORITY_MANAGE`
// 部门管理
export const DEPARTMENT_MANAGE = `${PREFIX}/DEPARTMENT_MANAGE`
// 角色管理
export const ROLE_MANAGE = `${PREFIX}/ROLE_MANAGE`
// 钱包--账户余额--账户设置
export const ACCOUNT_SETTINGS = `${PREFIX}/ACCOUNT_SETTINGS`
// 钱包--账户余额--微码微控账户余额
export const M_CONTROL_ACCOUNT_BALANCE = `${PREFIX}/M_CONTROL_ACCOUNT_BALANCE`
// 钱包--账户余额--微客服代发红包记录
export const M_CUSTOMER_RED_RECORD = `${PREFIX}/M_CUSTOMER_RED_RECORD`
// 钱包--短信余额--短信充值
export const WALLET_SMS_RECHARGE = `${PREFIX}/WALLET_SMS_RECHARGE`

// 弹窗
export const DIALOGS = `${PREFIX}/DIALOGS`
// 菜单管理
export const MENU = `${PREFIX}/MENU`

// 首绑
export const BINDING_GIFT = `${PREFIX}/BINDING_GIFT`

export const BINDING_RECORDING = `${PREFIX}/BINDING_RECORDING`
// 追评
export const ADD_EVA_GIFT = `${PREFIX}/ADD_EVA_GIFT`

export const ADD_EVA_AUDIT = `${PREFIX}/ADD_EVA_AUDIT`

export const ADD_EVA_OUTLAY = `${PREFIX}/ADD_EVA_OUTLAY`
// 加购
export const ADD_CART_GIFT = `${PREFIX}/ADD_CART_GIFT`

export const ADD_CART_AUDIT = `${PREFIX}/ADD_CART_AUDIT`

export const ADD_CART_OUTLAY = `${PREFIX}/ADD_CART_OUTLAY`
// 收藏
export const COLLECTION_GIFT = `${PREFIX}/COLLECTION_GIFT`

export const COLLECTION_AUDIT = `${PREFIX}/COLLECITON_AUDIT`

export const COLLECTION_OUTLAY = `${PREFIX}/COLLECTION_OUTLAY`
// 投票
export const NEWVOTE = `${PREFIX}/NEWVOTE`

export const NEWVOTE_AUDIT = `${PREFIX}/NEWVOTE_AUDIT`

export const NEWVOTE_OUTLAY = `${PREFIX}/NEWVOTE_OUTLAY`
// 买家秀
export const BUYER_SHOW = `${PREFIX}/BUYER_SHOW`

export const BUYER_SHOW_AUDIT = `${PREFIX}/BUYER_SHOW_AUDIT`

export const BUYER_SHOW_OUTLAY = `${PREFIX}/BUYER_SHOW_OUTLAY`
