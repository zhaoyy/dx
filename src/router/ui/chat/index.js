import * as types from './types'

export default [
  {
    path: 'home',
    name: types.HOME,
    component: resolve => {
      require(['@/pages/ui/chat/home/home.vue'], resolve)
    }
  },
  {
    path: 'emoji',
    name: types.EMOJI,
    component: resolve => {
      require(['@/pages/ui/chat/emoji.vue'], resolve)
    }
  }
]
