const PREFIX = 'UI/CHAT'

export const DEMO = `${PREFIX}/DEMO`

// 聊天主页
export const HOME = `${PREFIX}/HOME`

// emoji
export const EMOJI = `${PREFIX}/EMOJI`
