import chatRouter from './chat'
import systemRouter from './system'
import loginRouter from './login'
import smsRouter from './sms'

export default [...loginRouter, {
  path: 'system',
  component: resolve => require(['@/pages/ui/system/mainNew'], resolve),
  children: systemRouter
}, {
  path: 'chat',
  component: resolve => require(['@/pages/ui/chat/main'], resolve),
  children: chatRouter
}, {
  path: 'sms',
  component: resolve => require(['@/pages/ui/sms/main'], resolve),
  children: smsRouter
}]
