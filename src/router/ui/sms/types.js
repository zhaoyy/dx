const PREFIX = 'UI/SMS'

// 首页
export const HOME = `${PREFIX}/HOME`
// 贴牌商列表
export const OEM_LIST = `${PREFIX}/OEM_LIST`
// 新增贴牌商
export const NEW_OEM = `${PREFIX}/NEW_OEM`
// 企业列表
export const ENTERPRISE_LIST = `${PREFIX}/ENTERPRISE_LIST`
// 代理商列表
export const OMS_LIST = `${PREFIX}/OMS_List`
// 短信模板审核
export const SMS_TEMPLATE_AUDIT = `${PREFIX}/SMS_TEMPLATE_AUDIT`
// 红包充值记录
export const PACKETS_RECHARGE = `${PREFIX}/PACKETS_RECHARGE`
// 红包领取记录
export const PACKETS_RECEIVE = `${PREFIX}/PACKETS_RECEIVE`
// 晒图活动记录
export const SHOW_PICTURE = `${PREFIX}/SHOW_PICTURE`
// 短信充值记录
export const SMS_RECHARGE = `${PREFIX}/SMS_RECHARGE`
// 短信发送记录
export const SMS_SEND = `${PREFIX}/SMS_SEND`
// 系统设置
export const SYSTEM_SETTINGS = `${PREFIX}/SYSTEM_SETTINGS`
