import * as types from './types'

export default [
  {
    path: 'home',
    name: types.HOME,
    component: resolve => require(['@/pages/ui/sms/home/home.vue'], resolve)
  },
  {
    path: 'oemList',
    name: types.OEM_LIST,
    component: resolve =>
      require(['@/pages/ui/sms/customerManage/oemList/oemList.vue'], resolve)
  },
  {
    path: 'newOem',
    name: types.NEW_OEM,
    component: resolve =>
      require(['@/pages/ui/sms/customerManage/oemList/newOem.vue'], resolve)
  },
  {
    path: 'enterpriseList',
    name: types.ENTERPRISE_LIST,
    component: resolve =>
      require(['@/pages/ui/sms/customerManage/enterpriseList.vue'], resolve)
  },
  {
    path: 'omsList',
    name: types.OMS_LIST,
    component: resolve =>
      require(['@/pages/ui/sms/customerManage/omsList.vue'], resolve)
  },
  {
    path: 'smsTemplateAudit',
    name: types.SMS_TEMPLATE_AUDIT,
    component: resolve =>
      require(['@/pages/ui/sms/smsTemplateAudit/smsTemplateAudit.vue'], resolve)
  },
  {
    path: 'packetsRecharge',
    name: types.PACKETS_RECHARGE,
    component: resolve =>
      require([
        '@/pages/ui/sms/rechargeDetails/packetsDetails/packetsRecharge.vue'
      ], resolve)
  },
  {
    path: 'packetsReceive',
    name: types.PACKETS_RECEIVE,
    component: resolve =>
      require([
        '@/pages/ui/sms/rechargeDetails/packetsDetails/packetsReceive.vue'
      ], resolve)
  },
  {
    path: 'showPicture',
    name: types.SHOW_PICTURE,
    component: resolve =>
      require([
        '@/pages/ui/sms/rechargeDetails/packetsDetails/showPicture.vue'
      ], resolve)
  },
  {
    path: 'smsRecharge',
    name: types.SMS_RECHARGE,
    component: resolve =>
      require([
        '@/pages/ui/sms/rechargeDetails/smsDetails/smsRecharge.vue'
      ], resolve)
  },
  {
    path: 'smsSend',
    name: types.SMS_SEND,
    component: resolve =>
      require(['@/pages/ui/sms/rechargeDetails/smsDetails/smsSend.vue'], resolve)
  },
  {
    path: 'systemSettings',
    name: types.SYSTEM_SETTINGS,
    component: resolve =>
      require(['@/pages/ui/sms/systemSettings/systemSettings.vue'], resolve)
  }
]
