// 音频时长
export default (value, len = 1) => {
  value = value || 0
  if (value) {
    if (typeof value === 'string') {
      value = Number(value)
    }
    return Number((value / 1000).toFixed(len)) + '"'
  }
  return value
}
