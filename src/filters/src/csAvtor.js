import {
  defaultCustomerServiceAvtor
} from '@/config'

// 客服默认头像
export default value => {
  return value || defaultCustomerServiceAvtor
}
