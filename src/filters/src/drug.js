import {
  drug
} from '@/utils'

export default (value, beginIndex = 3, size = 4) => {
  return drug(value, beginIndex, size)
}
