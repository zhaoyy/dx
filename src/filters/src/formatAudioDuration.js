// 计算音频的时长
export default value => {
  // 分钟
  var minute = value / 60
  var minutes = parseInt(minute)
  if (minutes < 10) {
    minutes = '0' + minutes
  }
  // 秒
  var second = value % 60
  var seconds = Math.floor(second)
  if (seconds < 10) {
    seconds = '0' + seconds
  }
  // 总共时长的秒数
  return parseInt(minutes * 60 + seconds) || '0'
}
