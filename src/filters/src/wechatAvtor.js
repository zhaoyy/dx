import {
  defautWechatAvtor
} from '@/config'

// 微信默认头像
export default value => {
  return value || defautWechatAvtor
}
