// 微信默认头像
export default value => {
  let mobile
  if (value) {
    mobile = '--'
  } else {
    mobile = value.slice(0, 3) + '******'
  }
  return mobile
}
