// 微信默认头像
export default (value, length = 2, showChat = '--') => {
  let newVal
  let num = Number(value)
  if (isNaN(num) || value === null) {
    newVal = showChat
  } else {
    newVal = Number(value).toFixed(length)
  }
  return newVal
}
