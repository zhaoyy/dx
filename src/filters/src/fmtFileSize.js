import { changeFileSize } from '@/utils'

// 文件大小
export default (value, len) => {
  return changeFileSize(value, len)
}
