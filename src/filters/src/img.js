import {
  defaultNoImg
} from '@/config'

// 无图默认图片
export default value => {
  return value || defaultNoImg
}
