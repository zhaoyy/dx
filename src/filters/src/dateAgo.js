import {
  dateAgo
} from '@/utils'

export default value => {
  if (value) {
    return dateAgo(Number(value))
  }
  return value
}
