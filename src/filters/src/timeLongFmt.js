import {
  timeLongFmt
} from '@/utils'

export default (value, type) => {
  return timeLongFmt(value, type)
}
