// import '@static/js/recorder/recorder.mp3.min.js'
import '@static/js/recorder/recorder.wav.min.js'
import {
  noop
} from '@/utils'

const RecorderPls = window.Recorder
const defaultOptions = {
  // 输出类型：mp3,wav等，使用一个类型前需要先引入对应的编码引擎
  type: 'wav',
  // 比特率 wav:16或8位，MP3：8kbps 1k/s，16kbps 2k/s 录音文件很小
  bitRate: 16,
  // 采样率，wav格式大小=sampleRate*时间；mp3此项对低比特率有影响，高比特率几乎无影响。
  // wav任意值，mp3取值范围：48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000
  sampleRate: 24000,
  // AudioContext缓冲大小
  // 取值256, 512, 1024, 2048, 4096, 8192, or 16384，会影响onProcess调用速度
  bufferSize: 8192,
  // 接收到录音数据时的回调函数：fn(buffer, powerLevel, bufferDuration)
  // buffer=[缓冲数据,...]，powerLevel：当前缓冲的音量级别0-100，bufferDuration：已缓冲时长
  // 如果需要绘制波形之类功能，需要实现此方法即可，使用以计算好的powerLevel可以实现音量大小的直观展示，使用buffer可以达到更高级效果
  onProcess: noop
}

const STATE = {
  PENDING: 'pending',
  OPEN: 'open',
  START: 'start',
  STOP: 'stop',
  PAUSE: 'pause',
  CLOSE: 'close'
}

export default class Recorder {
  options = {}
  state = STATE.PENDING
  recorderPlsInstance = null
  constructor(options) {
    this.options = Object.assign({}, defaultOptions, options)
    this.recorderPlsInstance = RecorderPls(this.options)
  }
  /**
   * 由于Recorder持有的录音资源是全局唯一的，可通过此方法检测是否有Recorder已调用过open打开了录音功能
   */
  static isOpen() {
    return RecorderPls.IsOpen()
  }
  /**
   * 请求麦克风授权
   */
  open() {
    return new Promise((resolve, reject) => {
      this.recorderPlsInstance.open(() => {
        this.state = STATE.OPEN
        // 打开麦克风授权获得相关资源
        resolve()
      }, (errMsg) => {
        // 授权失败
        reject(errMsg)
      })
    })
  }
  /**
   * 关闭释放录音资源
   */
  close() {
    return new Promise((resolve) => {
      this.recorderPlsInstance.close(() => {
        this.state = STATE.CLOSE
        resolve()
      })
    })
  }
  /**
   * 开始录音，需先调用open；如果不支持、错误，不会有任何提示，因为stop时自然能得到错误
   */
  start() {
    this.recorderPlsInstance.start()
    this.state = STATE.START
    return this
  }
  /**
   * 结束录音并返回录音数据blob对象，拿到blob对象就可以为所欲为了，不限于立即播放、上传
   * 提示：stop时会进行音频编码，音频编码可能会很慢，10几秒录音花费2秒左右算是正常，编码并未使用Worker方案(文件多)，内部采取的是分段编码+setTimeout来处理，界面卡顿不明显
   */
  stop() {
    return new Promise((resolve, reject) => {
      this.recorderPlsInstance.stop((blob, duration) => {
        this.state = STATE.STOP
        resolve({
          // 录音数据audio/mp3|wav...格式
          blob,
          // 录音时长，单位毫秒
          duration
        })
      }, (errMsg) => {
        reject(errMsg)
      })
    })
  }
  /**
   * 暂停录音
   */
  pause() {
    this.recorderPlsInstance.pause()
    this.state = STATE.PAUSE
    return this
  }
  /**
   * 恢复继续录音
   */
  resume() {
    this.recorderPlsInstance.resume()
    this.state = STATE.START
    return this
  }
}
