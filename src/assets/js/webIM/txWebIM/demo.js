import vmaWebIM from './vmaWebIm.js'
import { customerServiceApi } from '@/api/chat'

function initDemo() {
	let myIM
  let imLoginInfo = await customerServiceApi.getImLoginInfo()
  let loginInfo = {
    sdkAppID: imLoginInfo.sdkAppId,
    appIDAt3rd: imLoginInfo.sdkAppId,
    identifier: imLoginInfo.identifier,
    userSig: imLoginInfo.userSig,
    accountType: imLoginInfo.accountType
  }

  // 设置连接状态监听
  VmaWebIM.setOnConnNotifyCallback(connectStatusOn)
  // 实例化
  myIM = new VmaWebIM({
    loginInfo: {
      sdkAppID: imLoginInfo.sdkAppId,
      appIDAt3rd: imLoginInfo.sdkAppId,
      identifier: imLoginInfo.identifier,
      userSig: imLoginInfo.userSig,
      accountType: imLoginInfo.accountType
    },
    listenersOptions: {
      onKickedEventCall: that.onKickedEventCall
    },
    onC2CMsgNotify: that.onC2CMsgNotify,
    onGroupMsgNotify: that.onGroupMsgNotify,
    onConnNotifyCallback: {
      connectStatusOn: that.connectStatusOn,
      connectStatusOff: that.connectStatusOff,
      connectStatusReconnect: that.connectStatusReconnect,
      connectStatusUnknow: that.connectStatusUnknow
    }
  })
  // 登录
  myIM.webimLogin().then((resp) => {
    // console.log('login')
  })
}

// 连接成功的回调
function connectStatusOn(resp) {
  // console.log('connectStatusOn')
  // 发送一条自定义消息
  myIM.onSendC2cCustomMsg({
    msgContent: {
      type: 'test',
      content: '123456789'
    },
    selToID: 'testuser1',
    identifier: imLoginInfo.identifier
  })
}

// 接收到c2c消息
function onC2CMsgNotify() {}

// 群消息
function onGroupMsgNotify() {}

// 连接断开
function connectStatusOff() {}

function connectStatusReconnect() {}

// 被其它实例踢下线
function onKickedEventCall() {}

// 连接状态未知
connectStatusUnknow() {}

initDemo()

