// 帐号模式，0-表示独立模式，1-表示托管模式
// let accountMode = 0

// 用户所属应用帐号类型，必填
let accountType = 0
// 用户所属应用id,必填
let sdkAppID = ''
// App 用户使用 OAuth 授权体系分配的 Appid，必填
let appIDAt3rd = ''
// 当前用户ID,必须是否字符串类型，必填
let identifier = ''
// 当前用户身份凭证，必须是字符串类型，必填
let userSig = ''
// 当前用户昵称，不用填写，登录接口会返回用户的昵称，如果没有设置，则返回用户的id
let identifierNick = ''
// 当前用户默认头像，选填，如果设置过头像，则可以通过拉取个人资料接口来得到头像信息
let headurl = ''

// 当前用户身份
let loginInfo = {
  sdkAppID,
  appIDAt3rd,
  identifier,
  accountType,
  userSig,
  identifierNick,
  headurl
}

// 每次请求的最近会话条数，业务可以自定义
// let reqRecentSessCount = 50
// 是否需要支持APP端已读回执的功能,默认为0。是：1，否：0。
// let isPeerRead = 1
// let reqMsgCount = 15 //每次请求的历史消息(c2c获取群)条数，仅demo用得到

// 是否访问正式环境，默认访问正式，选填
let isAccessFormalEnv = true
// 是否开启控制台打印日志,默认开启，选填
let isLogOn = false

// 初始化时，其他对象，选填
let options = {
  isAccessFormalEnv,
  isLogOn
}

export default {
  // accountMode,
  loginInfo,
  options
}
