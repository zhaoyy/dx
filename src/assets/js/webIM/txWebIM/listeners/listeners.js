// 监听事件
let listeners = {
  // 监听连接状态回调变化事件,必填
  onConnNotify: null,
  // IE9(含)以下浏览器用到的jsonp回调函数，
  jsonpCallback: null,
  // 监听新消息(私聊，普通群(非直播聊天室)消息，全员推送消息)事件，必填
  onMsgNotify: null,
  // 监听新消息(直播聊天室)事件，直播场景下必填
  onBigGroupMsgNotify: null,
  // 监听（多终端同步）群系统消息事件，如果不需要监听，可不填
  onGroupSystemNotifys: null,
  // 监听群资料变化事件，选填
  onGroupInfoChangeNotify: null,
  // 监听好友系统通知事件，选填
  onFriendSystemNotifys: null,
  // 监听资料系统（自己或好友）通知事件，选填
  onProfileSystemNotifys: null,
  // 被其他登录实例踢下线，选填
  onKickedEventCall: null,
  // 监听C2C系统消息通道，选填
  onC2cEventNotifys: null,
  // 申请文件/音频下载地址的回调
  onAppliedDownloadUrl: null,
  onLongPullingNotify: function (data) {
    // console.debug('onLongPullingNotify', data)
  }
}

export default listeners
