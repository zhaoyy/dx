// 监听连接状态回调变化事件
let onConnNotify = function(resp) {
  let info
  switch (resp.ErrorCode) {
    case webim.CONNECTION_STATUS.ON:
      webim.Log.warn('建立连接成功: ' + resp.ErrorInfo)
      TxWebIM.onConnNotifyCallback.connectStatusOn(resp.ErrorCode)
      break
    case webim.CONNECTION_STATUS.OFF:
      info = '连接已断开，无法收到新消息，请检查下你的网络是否正常: ' + resp.ErrorInfo
      // alert(info)
      webim.Log.warn(info)
      TxWebIM.connectStatusOff()
      break
    case webim.CONNECTION_STATUS.RECONNECT:
      info = '连接状态恢复正常: ' + resp.ErrorInfo
      // alert(info)
      webim.Log.warn(info)
      TxWebIM.connectStatusReconnect()
      break
    default:
      webim.Log.error('未知连接状态: =' + resp.ErrorInfo)
      TxWebIM.connectStatusUnknow(resp.ErrorInfo)
      break
  }
}

export default onConnNotify
