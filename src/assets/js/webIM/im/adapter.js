class Adapter {
  contructor(options) {}
  /**
   * 登录
   * @return {[type]} [description]
   */
  webimLogin(options) {}
  /**
   * 登出
   * @return {[type]} [description]
   */
  webimLoginOut(options) {}
  /**
   * 发送消息
   * @param  {[type]} options [description]
   * @return {[type]}         [description]
   */
  sendMsg(options) {}
  /**
   * 接收新消息
   */
  receivedNewMsg() {}
  /**
   * 获取历史消息
   */
  getHistoryMsgs() {}
  /**
   * 获取未读c2c消息
   */
  getNotReadC2CMsgs() {}
  /**
   * 获取最近联系过的人
   */
  // getHistoryContacts() {}
}