import TxWebIM from '../txWebIM/txWebIm'

class WebIM {
  /**
   * 构造函数
   * @param  {[type]} options.loginInfo            登录信息
   * @param  {[type]} options.onC2CMsgNotify       监听c2c消息
   * @param  {[type]} options.onGroupMsgNotify     监听群组消息
   * @param  {Object} options.onConnNotifyCallback 监听连接状态
   * @return {[type]}                              [description]
   */
  constructor({
    loginInfo,
    onC2CMsgNotify,
    onGroupMsgNotify,
    onConnNotifyCallback: {
      connectStatusOn,
      connectStatusOff,
      connectStatusReconnect,
      connectStatusUnknow
    },
    listenersOptions
  }) {
    this.im = new TxWebIM({
      loginInfo,
      onC2CMsgNotify,
      onGroupMsgNotify,
      onConnNotifyCallback: {
        connectStatusOn,
        connectStatusOff,
        connectStatusReconnect,
        connectStatusUnknow
      },
      listenersOptions
    })
  }

  // im实例
  im = null

  /**
   * 登录
   */
  webimLogin(options) {
    return this.im.webimLogin(options)
  }

  webimLogout() {
    return this.im.webimLogout()
  }

  /**
   * 发送消息
   * @param  {Object} options.msgData      消息对象
   * @param  {[type]} options.selToID      消息接收送者的IM系统的id
   * @param  {[type]} options.identifier   发送者的IM系统的id
   * @param  {[type]} options.sessionType  会话类型，c2c/群聊
   * @param  {String} options.msgSubType   消息类型，文本/图片等
   * @return {[type]}                      [description]
   */
  sendMessge({
    msgData,
    selToID,
    identifier,
    msgSubType,
    sessionType
  } = {}) {
    return this.im.sendMsg({
      msgData,
      selToID,
      identifier,
      msgSubType,
      sessionType
    })
  }

  /**
   * 获取所有未读消息
   * @param  {Object} options.sessionType 会话类型c2c/群聊
   * @return {[type]}                     [description]
   */
  getNotReadMsgs({
    sessionType
  } = {}) {
    return new Promise((resolve, reject) => {
      this.im.getNotReadMsgs({
        sessionType,
        cbOk: (resp) => {
          resolve(resp)
        },
        cbErr: (error) => {
          reject(error)
        }
      })
    })
  }

  /**
   * 获取最近的消息
   * @param  {[type]} options.sessionType 会话类型
   * @param  {[type]} options.selToID     对方id
   * @param  {Object} options.reqMsgCount 请求的消息数量
   * @return {[type]}                     [description]
   */
  // getLastHistoryMsgs(option) {
  //   return this.im.getLastHistoryMsgs(option)
  // }
  getLastC2CHistoryMsgs(option) {
    return this.im.getLastC2CHistoryMsgs(option)
  }

  getLastGroupHistoryMsgs(option) {
    return this.im.getLastGroupHistoryMsgs(option)
  }

  /**
   * 设置已读
   * @param {[type]}  options.selToID    [description]
   * @param {[type]}  options.selType    [description]
   * @param {Boolean} options.isAutoOn   将selSess的自动已读消息标志改为isOn，同时是否上报当前会话已读消息
   * @param {Boolean} options.isResetAll 是否重置所有会话的自动已读标志
   */
  setAlreadyRead(option) {
    return this.im.setAlreadyRead(option)
  }

  // onMsgNotify(newMsgList) {}

  /**
   * 连接成功时
   * @return {[type]} [description]
   */
  // connectStatusOn() {}
  /**
   * 连接断开
   * @return {[type]} [description]
   */
  // connectStatusOff() {}
  /**
   * 连接状态恢复正常
   * @return {[type]} [description]
   */
  // connectStatusReconnect() {}
  /**
   * 连接状态未知
   * @return {[type]} [description]
   */
  // connectStatusUnknow() {}
}

export default WebIM
