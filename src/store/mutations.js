import * as types from './mutation-types'
import {
  tagListSession,
  loginInfoLocal
} from '@/storage'

export default {
  // 添加tag标签记录
  [types.UPDATE_TAG_LIST](state, item) {
    // 获取左侧菜单数据
    let defaultVal = {
      defaultActive: 1,
      tagList: []
    }
    let sesionTagList = tagListSession.getJSON().tagList || defaultVal
    let tagList = sesionTagList.tagList
    let name = item.name
    let isHas = false
    for (let i = 0; i < tagList.length; i++) {
      if (name === tagList[i].name) {
        isHas = true
        break
      }
    }
    if (!isHas) {
      tagList.push(item)
    }
    state.tagList.tagList = tagList
    state.tagList.defaultActive = item.index
    tagListSession.setJSON({
      tagList: state.tagList
    })
  },
  // 设置左侧菜单选中项
  [types.UPDATE_DEFAULT_ACTIVE](state, val) {
    // 获取左侧菜单数据
    state.tagList.defaultActive = val
    tagListSession.setJSON({
      tagList: state.tagList
    })
  },
  // 删除某个标签
  [types.DELETE_TAG_LIST_ONE](state, obj) {
    let sesionTagList = tagListSession.getJSON().tagList
    let tagList = sesionTagList.tagList
    let name = obj.item.name
    let isCurrentRoute = false
    let currentIndex
    for (let i = 0; i < tagList.length; i++) {
      if (name === tagList[i].name) {
        currentIndex = i
        let defaultActive = sesionTagList.defaultActive
        if (obj.item.index === defaultActive) {
          isCurrentRoute = true
        }
      }
    }
    // 表示关闭的是当前页面--故堆栈最后一个
    tagList.splice(currentIndex, 1)
    if (isCurrentRoute) {
      let len = tagList.length - 1
      if (len >= 0) {
        obj.vm.$router.push({
          name: tagList[len].name
        })
        sesionTagList.defaultActive = tagList[len].index
      }
    }
    sesionTagList.tagList = tagList
    // 更新缓存
    tagListSession.setJSON({
      tagList: sesionTagList
    })
    // 更新vuex
    state.tagList = sesionTagList
  },
  /**
   * 设置标签栏
   *
   * @param {*} state
   * @param {*} data
   */
  [types.UPDATE_NAV_TAG](state, val) {
    // 获取左侧菜单数据
    state.navTagArr = val
  },
  /**
   * 设置左侧菜单栏
   *
   * @param {*} state
   * @param {*} data
   */
  [types.UPDATE_LEFT_MENU](state, val) {
    // 获取左侧菜单数据
    state.leftMenuArr = val
  },
  /**
   * 设置当前登录用户
   *
   * @param {*} state
   * @param {*} data
   */
  [types.SET_ADMIN_INFO](state, data) {
    state.adminInfo = Object.assign({}, data)
  },
  /**
   * 设置当前网页信息
   *
   * @param {*} state
   * @param {*} data
   */
  [types.UPDATE_LOGIN_INFO](state, data) {
    state.loginInfo = Object.assign({}, data)
    loginInfoLocal.setJSON(data)
  },

  /**
   * IM
   *
   * @param {*} state
   * @param {*} data
   */
  [types.CHAT_IM](state, data) {
    state.chatIM = data
  },
  /**
   * 登出IM
   *
   * @param {*} state
   * @param {*} data
   */
  [types.CHAT_IM_LOG_OUT](state) {
    state.chatIM && state.chatIM.webimLogout && typeof state.chatIM.webimLogout === 'function' && state.chatIM.webimLogout()
    state.activeChatAccountInfo = null
    state.activeChatFriendInfo = null
    state.chatFooterSwitchs = null
  },
  /**
   * 当前个人号的信息
   *
   * @param {*} state
   * @param {*} data
   */
  [types.ACTIVE_CHAT_ACCOUNT_INFO](state, data) {
    state.activeChatAccountInfo = data || null
  },
  /**
   * 当前选中聊天的好友/群组
   *
   * @param {*} state
   * @param {*} data
   */
  [types.ACTIVE_CHAT_FRIEND_INFO](state, data) {
    state.activeChatFriendInfo = data || null
  },
  /**
   * 聊天的footer开关状态
   *
   * @param {*} state
   * @param {*} data
   */
  [types.CHAT_FOOTER_SWITCHS](state, data) {
    state.chatFooterSwitchs = data || {}
  },
  /**
   * 用户信息的indexedDB链接
   *
   * @param {*} state
   * @param {*} data
   */
  [types.CHAT_USER_INFO_DB](state, data) {
    state.chatUserInfoDB = data || null
  },
  /**
   * 聊天的敏感词
   *
   * @param {*} state
   * @param {*} data
   */
  [types.CHAT_SENSITIVE_WORDS](state, data) {
    state.chatSensitiveWords = data || {}
  },
  /**
   * 设置resourceMenuMap
   *
   * @param {*} state
   * @param {*} data
   */
  [types.SET_RESOURCE_MENU_MAP](state, data) {
    state.resourceMenuMap = Object.assign({}, data)
  },
  /**
   * 设置消息通知
   *
   * @param {*} state
   * @param {*} data
   */
  [types.UPDATE_NOTICE_TIP](state, data) {
    state.noticeTip = data
  },
  /**
   * 获取太保登录信息
   *
   * @param {*} state
   * @param {*} data
   */
  [types.SAVE_LOGIN_INFO](state, data) {
    state.taibaoChatData = data
  }
}
