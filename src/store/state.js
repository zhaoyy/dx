import {
  systemLogo
} from '@/config'
import {
  tagListSession,
  adminInfoSession,
  loginInfoLocal
} from '@/storage'

// 获取左侧菜单数据
function getSessionTabList() {
  let defaultVal = {
    defaultActive: '1',
    tagList: []
  }
  let sesionTagList = tagListSession.getJSON().tagList || defaultVal
  return sesionTagList
}

export default {
  demo: 'demo',
  tagList: getSessionTabList(),
  // 主题相关
  theme: {
    systemLogo
  },
  // 当前网页相关信息
  loginInfo: loginInfoLocal.getJSON(),
  // 当前登录用户
  adminInfo: adminInfoSession.getJSON(),
  // 菜单映射map
  resourceMenuMap: {},
  // 未读消息提示
  noticeTip: false,
  // 微客服im实例
  chatIM: {},
  // 微客服当前选中的个人号
  activeChatAccountInfo: null,
  // 微客服当前选中的聊天好友/群聊
  activeChatFriendInfo: null,
  // 微客服footer的相关开关
  chatFooterSwitchs: {},
  // 新版--标签栏
  navTagArr: [],
  // 新版--左侧菜单栏
  leftMenuArr: [],
  // 敏感词
  chatSensitiveWords: {},
  // 用户信息的indexedDB链接
  chatUserInfoDB: null,
  // 群统计搜索内容
  groupSelectMsg: '',
  // 右击好友头像获取nickname
  groupRightAtName: '',
  // 太保免登陆的数据
  taibaoChatData: {}
}
