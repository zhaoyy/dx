import * as types from './mutation-types'
import {
  adminInfoSession,
  macKeyCookie,
  tagListSession,
  broadcastLogoutLocal,
  wechatInfoSession,
  wechatFooterSession,
  loginTypeLocal
} from '@/storage'
import {
  homeApi
} from '@/api/common'
import {
  redirectToLoign
} from '@/router'
import {
  Notify
} from 'vma-vue-element'

// 登录后处理
export const afterLogin = ({
  commit
}, data) => {
  console.log('登录后处理', data)
  commit(types.SET_ADMIN_INFO, data)
  adminInfoSession.setJSON(data)
  macKeyCookie.set(data.macKey)
}

// 登出后处理
export const afterLogout = ({
  commit
}) => {
  commit(types.SET_ADMIN_INFO, {})
  commit(types.UPDATE_TAG_LIST, {})
  commit(types.CHAT_IM_LOG_OUT)
  commit(types.CHAT_SENSITIVE_WORDS, {})
  commit(types.CHAT_USER_INFO_DB, null)
  adminInfoSession.remove()
  macKeyCookie.remove()
  tagListSession.remove()
  wechatInfoSession.remove()
  wechatFooterSession.remove()
}

// 登出
export const logout = ({
  dispatch
}, silence = false) => {
  dispatch('logoutBroadcast').then(() => {
    // 触发其他TAB的登出
    broadcastLogoutLocal.random()
  })
}

// 登出广播
export const logoutBroadcast = ({
  dispatch
}, silence = true) => {
  return homeApi.logout().then(() => {
    if (!silence) {
      Notify.success('登出成功')
    }
    dispatch('afterLogout')
    redirectToLoign()
  })
}

// 登出IM
export const webimLogout = ({
  commit
}) => {
  commit(types.CHAT_IM_LOG_OUT)
}
