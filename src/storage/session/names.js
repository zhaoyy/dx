import {
  appNamespace
} from '@/config'

export const DEMO = `${appNamespace}/DEMO`
export const TAGLIST = `${appNamespace}/TAGLIST`
export const ADMIN_INFO = `${appNamespace}/ADMIN_INFO`
export const CHAT_INFO = `${appNamespace}/CHAT_INFO`
export const CHAT_FOOTER_STATUS = `${appNamespace}/CHAT_FOOTER_STATUS`
export const BUSINESS = `${appNamespace}/BUSINESS`
export const TIMEPICK = `${appNamespace}/TIMEPICK`
// TODO AI用户问题临时存储
export const AI_USER_QUESTION = `${appNamespace}/AI_USER_QUESTION`
