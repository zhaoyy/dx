import {
  appNamespace
} from '@/config'

export const DEMO = `${appNamespace}/DEMO`
export const LOGIN_REMEMBER_ME = `${appNamespace}/LOGIN_REMEMBER_ME`
export const LOGIN_ACCOUNT = `${appNamespace}/LOGIN_ACCOUNT`
export const LOGIN_TYPE = `${appNamespace}/LOGIN_TYPE`
export const BROADCAST_LOGOUT = `${appNamespace}/BROADCAST_LOGOUT`
export const BROADCAST_LOGIN = `${appNamespace}/BROADCAST_LOGIN`
export const WECHAT_USER_DB_INFO = `${appNamespace}/WECHAT_USER_DB_INFO`
export const LOGIN_INFO = `${appNamespace}/LOGIN_INFO`
