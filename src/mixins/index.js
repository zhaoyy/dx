import tableMixin from './src/tableMixin'
import echartResizeMixin from './src/echartResizeMixin'
import indexMixin from './src/indexMixin'
import formMixin from './src/formMixin'
import detailMixin from './src/detailMixin'
import resourceMixin from './src/resourceMixin'
import menuRigthMixin from './src/menuRigthMixin'
import txtImgAudVideoInitDataMixin from './src/txtImgAudVideoInitDataMixin'
import msgTIAVSubmitMessage from './src/msgTIAVSubmitMessage'
import aiTxtImgAudVideoInitDataMixin from './src/aiTxtImgAudVideoInitDataMixin'
import aiMsgTIAVSubmitMessage from './src/aiMsgTIAVSubmitMessage'
import walletSetPayAccountStep from './src/walletSetPayAccountStep'
import marketPlatformMixins from './src/marketPlatformMixins'
import marketPlatformAddMixins from './src/marketPlatformAddMixins'
import setDocumentMixin from './src/setDocumentMixin'

export {
  tableMixin,
  echartResizeMixin,
  indexMixin,
  formMixin,
  detailMixin,
  resourceMixin,
  menuRigthMixin,
  txtImgAudVideoInitDataMixin,
  msgTIAVSubmitMessage,
  aiTxtImgAudVideoInitDataMixin,
  aiMsgTIAVSubmitMessage,
  walletSetPayAccountStep,
  marketPlatformMixins,
  marketPlatformAddMixins,
  setDocumentMixin
}
