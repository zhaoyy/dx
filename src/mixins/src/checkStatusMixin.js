import {
  roleApi
} from '@/api/common'

export default {
  data() {
    return {
      checkStatus: {
        timer: null,
        time: 30000
      }
    }
  },
  created() {
    // 定时校验登录状态
    this.checkStatus.timer = setInterval(() => {
      roleApi.checkStatus()
    }, this.checkStatus.time)
  },
  beforeDestroy() {
    clearInterval(this.checkStatus.timer)
  }
}
