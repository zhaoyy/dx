import {
  commonApi
} from '@/api/system'
export default {
  data() {
    const compareValidator = (rule, value, callback, source, options) => {
      let errors = []
      this.form.upLine = this.form.upLine ? this.form.upLine : this.form.upRedPackage
      if (value && value >= this.form.upLine) {
        errors.push('最小红包金额应小于最大红包金额')
      }
      if (!this.form.upLine) {
        errors.push('请输入最大红包金额')
      }
      callback(errors)
    }
    const dateValidator = (rule, value, callback, source, options) => {
      let errors = []
      if (value[0] <= Date.now()) {
        errors.push('活动开始时间必须大于当前时间')
      }
      if (value[1] <= value[0]) {
        errors.push('活动结束时间必须大于活动开始时间')
      }
      callback(errors)
    }
    return {
      shopList: [],
      codeList: [],
      date: [],
      pickerOptions: {
        disabledDate(time) {
          // 86400000 1天 ms
          return time.getTime() < (Date.now() - 86400000)
        }
      },
      form: {
        activityName: '',
        pageTitle: '',
        shareText: '',
        startTime: '',
        endTime: '',
        date: [],
        backgroundImg: '',
        activityType: 1, // 奖品类型 1现金红包
        redpackageType: 1 // 红包类型 1固定红包金额 2随机红包金额 3阶梯金额红包
      },
      rules: {
        activityName: [{
          required: true,
          message: '请输入活动名称'
        }, {
          max: 12,
          message: '活动名称不能超过12个字符'
        }],
        pageTitle: [{
          required: true,
          message: '请输入活动页面标题'
        }, {
          max: 8,
          message: '活动页面标题不能超过8个字符'
        }],
        shareText: [{
          required: true,
          message: '请输入活动分享文案'
        }],
        // 活动时间验证
        date: [{
          type: 'array',
          required: true,
          message: '请选择活动时间'
        }, {
          required: true,
          validator: dateValidator
        }],
        moneyNum: [{
          required: true,
          message: '请输入红包金额'
        }],
        // 随机金额红包
        floorLine: [{
          required: true,
          message: '请输入最小红包金额'
        }, {
          validator: compareValidator
        }],
        // 随机金额红包
        floorRedPackage: [{
          required: true,
          message: '请输入最小红包金额'
        }, {
          validator: compareValidator
        }],
        introduction: [{
          required: true,
          message: '请输入活动规则'
        }]
      },
      // h5预览地址
      viewUrl: ''
    }
  },
  filters: {
    // textarea输入字数计算
    shareTextFil(val) {
      let len = 0
      let max = 50
      if (val) {
        len = val.replace(/\n/g, 'aa').length
      }
      return len > max ? max : len
    },
    introductionFil(val) {
      let len = 0
      let max = 300
      if (val) {
        len = val.replace(/\n/g, 'aa').length
      }
      return len > max ? max : len
    }
  },
  methods: {
    inputExample(key) {
      this.form[key] = this.exampleWord.join('\r\n')
    },
    // 获取商店列表
    async getShopList() {
      this.shopList = await commonApi.storeList()
    },
    // 获取微活码列表
    async getCodeList() {
      this.codeList = await commonApi.microCodeList()
    },
    // 获取详情数据处理
    async getDetailValidate(res) {
      // 活动时间
      if (res.startTime) {
        res.date = [res.startTime, res.endTime]
      }
      // 红包类型
      switch (res.redpackageType) {
        case 1:
          delete res.floorLine
          delete res.upLine
          delete res.floorRedPackage // 收藏、新品
          delete res.upRedPackage
          break
        case 2:
          delete res.moneyNum
          // 追评有礼
          if (res.redpageConfigs && res.redpageConfigs.length > 0) {
            res.redpageConfig = {
              floorLine: res.redpageConfigs[0].floorLine,
              upLine: res.redpageConfigs[0].upLine
            }
          }
          break
        case 3: // 追评有礼
          delete res.moneyNum
          res.redpageConfigss = res.redpageConfigs
          break
      }
      // 适用订单时间
      if (res.orderRangeType === 2) {
        res.dates = [res.orderRangeStart, res.orderRangeEnd]
      }
      // 提交有效期
      if (res.submitValidity) {
        this.submitValidity = 2
      } else {
        delete res.submitValidity
      }
      // 订单购买间隔
      if (res.intervalTime) {
        this.intervalTime = 2
      } else {
        delete res.intervalTime
      }
      // 微信用户参与限制
      if (res.joinLimit) {
        this.joinLimit = 2
      } else {
        delete res.joinLimit
      }
      // 审核设置
      if (res.checkType === 2) {
        res.checkTime = ''
      }
      // 微活码
      if (res.drainage === 1) {
        if (res.drainageType === 1) {
          res.activityUrl = Number(res.activityUrl)
          res.microCode = Number(res.microCode) // 收藏、新品
          delete res.drainageImgUrl
          delete res.imgUrl // 收藏、新品
          delete res.drainageText
        } else {
          delete res.activityUrl
          delete res.microCode // 收藏、新品
        }
      } else {
        delete res.drainageImgUrl
        delete res.drainageText
        delete res.activityUrl
        delete res.imgUrl // 收藏、新品
        delete res.microCode // 收藏、新品
        res.drainageType = 1
      }
      return res
    },
    // 表单验证
    validateForm(formName, entry) {
      if (entry === 'newVoting') {
        // 设置商品
        if (this.form.goodIds.length < 2) {
          this.error('至少选择两件商品')
          return
        }
      }
      return new Promise((resolve, reject) => {
        this.$refs[formName].validate(async (valid) => {
          if (valid) {
            // 活动时间
            if (this.form.date && this.form.date.length) {
              this.form.startTime = this.form.date[0]
              this.form.endTime = this.form.date[1]
            }
            // 红包类型
            switch (this.form.redpackageType) {
              case 1:
                delete this.form.floorLine
                delete this.form.upLine
                delete this.form.redpageConfigs // 追评有礼
                delete this.form.floorRedPackage // 收藏、新品
                delete this.form.upRedPackage // 收藏、新品
                break
              case 2:
                this.form.redpageConfigs = [this.form.redpageConfig] // 追评有礼
                delete this.form.moneyNum
                break
              case 3: // 追评有礼
                this.form.redpageConfigs = this.form.redpageConfigss
                delete this.form.moneyNum
                break
            }
            // 适用订单时间
            if (this.form.orderRangeType === 2) {
              if (this.form.dates.length < 2) {
                this.error('请选择适用订单时间的指定日期')
                return
              }
              this.form.orderRangeStart = this.form.dates[0]
              this.form.orderRangeEnd = this.form.dates[1]
            } else {
              delete this.form.orderRangeStart
              delete this.form.orderRangeEnd
            }
            // 提交有效期
            if (this.submitValidity === 2 && !this.form.submitValidity) {
              this.error('请输入提交有效期天数')
              return
            } else if (this.submitValidity === 1) {
              this.form.submitValidity = ''
            }
            // 订单购买间隔
            if (this.intervalTime === 2 && !this.form.intervalTime) {
              this.error('请输入订单购买间隔天数')
              return
            } else if (this.intervalTime === 1) {
              this.form.intervalTime = ''
            }
            // 微信用户参与限制
            if (this.joinLimit === 2 && !this.form.joinLimit) {
              this.error('请输入微信用户参与限制天数')
              return
            } else if (this.joinLimit === 1) {
              this.form.joinLimit = ''
            }
            // 审核设置
            if (this.form.checkType === 1 && !this.form.checkTime) {
              this.error('请输入审核设置小时')
              return
            } else if (this.form.checkType === 2) {
              this.form.checkTime = ''
            }
            switch (entry) {
              case 'review':
                // 选择店铺
                this.form.shopIds = this.shopIds.join(',')
                break
              case 'newVoting':
                // 设置商品
                if (this.form.goodIds.length < 2) {
                  this.error('至少选择两件商品')
                  return
                }
                delete this.form.goodsList
                break
            }
            // 微活码
            if (this.form.drainage === 1) {
              if (this.form.drainageType === 1) {
                if (!this.form.activityUrl && !this.form.microCode) {
                  this.error('请选择微活码')
                  return
                }
              } else {
                if (!this.form.drainageImgUrl && !this.form.imgUrl) {
                  this.error('请上传二维码')
                  return
                } else if (!this.form.drainageText) {
                  this.error('请输入引流话术')
                  return
                }
              }
            }
            resolve()
          } else {
            this.error('请输入正确表单信息')
          }
        })
      })
    }
  }
}
