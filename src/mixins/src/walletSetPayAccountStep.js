export default {
  props: {
    dialogProp: {
      type: Object,
      default: function () {
        return {}
      }
    }
  },
  data() {
    return {
    }
  },
  mounted() {
  },
  methods: {
    nextOrPrevStep(step) {
      if (step > 0) {
        this.$refs.ruleForm.validate((valid) => {
          if (valid) {
            this.turnToNextOrPrevStep(step)
          } else {
            return false
          }
        })
      } else {
        this.turnToNextOrPrevStep(step)
      }
    },
    turnToNextOrPrevStep(margin) {
      let data = Object.assign(this.dialogProp.data, this.params)
      let info = Object.assign({}, this.dialogProp.info)
      let step = this.dialogProp.step + margin
      let infos = {
        step: step,
        data: data,
        info: info
      }
      this.$emit('nextStep', infos)
    },
    closeDialog() {
      this.$emit('closeDialog')
    }
  }
}
