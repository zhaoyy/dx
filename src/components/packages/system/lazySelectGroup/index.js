import DemoLazySelect from './components/demo'
import personNumSelectGroup from './components/personNum'
import personNumSecSelectGroup from './components/personNumSec'

export {
  DemoLazySelect,
  personNumSelectGroup,
  personNumSecSelectGroup // 群管理--一键分配，好友管理--一键分配
}
