import DemoLazyCascader from './components/demo'
import departmenTreeCascadert from './components/departmentTree'
import mManageDepartTreeCascadert from './components/mManageDepartTree'
import selectPersonNumCascadert from './components/selectPersonNum'

export {
  DemoLazyCascader,
  departmenTreeCascadert, // 获取部门树
  mManageDepartTreeCascadert, // 获取部门树--素材管理--新增素材分组
  selectPersonNumCascadert // 选择个人号级联
}
