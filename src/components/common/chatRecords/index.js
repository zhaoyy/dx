import chatRecordFriends from './components/chatRecordFriends'
import chatRecordFriendsList from './components/chatRecordFriendsList'
import chatRecordGroup from './components/chatRecordGroup'
import chatRecordList from './components/chatRecordList'
import chatRecordSearch from './components/chatRecordSearch'

export {
  chatRecordFriends,
  chatRecordFriendsList,
  chatRecordGroup,
  chatRecordList,
  chatRecordSearch
}
