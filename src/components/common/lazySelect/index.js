import provinceList from './components/provinceList'
import cityList from './components/cityList'
import areaList from './components/areaList'

export {
  provinceList,
  cityList,
  areaList
}
