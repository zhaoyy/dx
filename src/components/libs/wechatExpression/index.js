import './css/style.scss'
import WechatExpression from './main'
import xfChatemo from './xfChatemo'
import xfweChatVerbalTrick from './xfweChatVerbalTrick'
import {
  parseHTML,
  parseText
} from './libs/wechatExpress'
import Express from './libs/expression'

export {
  WechatExpression,
  xfChatemo,
  xfweChatVerbalTrick,
  Express,
  parseHTML,
  parseText
}
