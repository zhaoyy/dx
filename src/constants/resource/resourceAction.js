import {
  Constant
} from 'vma-vue-assist'

// 资源功能权限编码
export const RESOURCE_ACTION_MAP = new Constant({
  // 客服登录
  IM_LOGIN: '2001',

  // 删除微信
  WECHAT_DELETE: '2002',

  // 删除设备
  EQUIPMENT_DELETE: '2003',

  // 公共快捷语
  REPLY_COMMON: '2004',

  // 账户权限设置
  ACCOUNT_PERMISSION: '2005',

  // 显示/导出好友微信号,微信id,手机号
  WECHAT_EXPORT: '2006',

  // 新建&发送红包
  RED_PACKET_CREATE_OR_SEND: '2007',

  // 转移会话
  SESSION_TRANSFER: '2008',

  // 新建晒图活动
  PICTURE_CREATE: '2009',

  // 审核晒图活动
  PICTURE_AUDIT: '2010',

  // 删除店铺
  DELETE_STORE: '2011',

  // 增删改首绑有礼
  BIND_ACTIVITY_CUD: '2012',

  // 增删改追评有礼
  COMMENT_ACTIVITY_CUD: '2013',

  // 审核追评有礼
  COMMENT_ACTIVITY_AUDIT: '2014',

  // 增删改加购有礼
  BUY_AGAIN_ACTIVITY_CUD: '2015',

  // 审核加购有礼
  BUY_AGAIN_ACTIVITY_AUDIT: '2016',

  // 增删改收藏有礼
  COLLECTION_ACTIVITY_CUD: '2017',

  // 审核收藏有礼
  COLLECTION_ACTIVITY_AUDIT: '2018',

  // 增删改新品投票
  VOTE_ACTIVITY_CUD: '2019',

  // 审核新品投票
  VOTE_ACTIVITY_AUDIT: '2020',

  // 增删改买家秀
  BUY_SHOW_ACTIVITY_CUD: '2021',

  // 审核买家秀
  BUY_SHOW_ACTIVITY_AUDIT: '2022',

  // 新增公众号&配置支付账号
  ADD_MP_ACCOUNT: '2024',

  // 开启&关闭支付账户
  ACCOUNT_SETTING: '2025',

  // 好友统计
  STAT_FRIEND: '2051',

  // 敏感统计
  STAT_SENSITIVE: '2052',

  // 进粉统计
  STAT_FANS: '2053',

  // 好友增长统计
  STAT_FRIEND_INCEEASE: '2054',

  // 好友增长排行
  RANK_FRIEND_INCREASE: '2055',

  // 好友分配统计
  STAT_FRIEND_DISTRIBUTION: '2056',

  // 接待人数统计
  STAT_RECEPTION: '2057',

  // 客服对话统计
  STAT_CUSTOMER_SERVICE_TALK: '2058',

  // 好友未通过统计
  STAT_FRIEND_NOT_PASS: '2059',

  // 微信群统计
  STAT_WECHAT_GROUP: '2060',

  // 好友响应时间统计
  STAT_FRIEND_RESPONSE_TIME: '2061',
  // 好友在线时间统计
  STAT_FRIEND_ONLINE_TIME: '2076',

  // 微信群活跃统计
  STAT_WECHAT_GROUP_ACTIVE: '2062',

  // 通话录音统计
  STAT_CALL_RECORDING: '2063',

  // 总览
  OVERVIEW_OVIEW: '2064',

  // 红包
  OVERVIEW_RED_PACKET_COUNT: '2066',

  // 红包金额
  OVERVIEW_RED_PACKET_AMOUNT: '2067',

  // 转账
  OVERVIEW_TRANSFER_COUNT: '2068',

  // 转账金额
  OVERVIEW_TRANSFER_AMOUNT: '2069',

  // 昨日新增好友
  OVERVIEW_YESTERDAYNEW_FRIEND: '2070',

  // 短信条数
  OVERVIEW_SMS_COUNT: '2071',

  // 晒图活动
  OVERVIEW_BLUEPRINT_COUNT: '2072',

  // 晒图活动金额
  OVERVIEW_BLUEPRINT_AMOUNT: '2073',

  // 入库设备
  OVERVIEW_WAREHOUSE_COUNT: '2074',

  // 行为管理增删改
  GROUP_ACTION: '2025',

  // 违规行为记录查看
  ILLEGAL_VIEW: '2026',

  // 违规行为警告
  ILLEGAL_WARNING: '2027',

  // 违规记录踢人
  ILLEGAL_KICKING: '2028',

  // 黑名单移除移入
  BLACK_LIST: '2029',

  // 群活动增改
  GROUP_ACTIVITY: '2030',

  // 入群问候增删改
  GROUP_GREETING: '2031',

  // 公共自动回复增删改
  COMMON_AOTU_REPLY: '2032',

  // 自动通过设置
  AUTO_PASS_SETTING: '2033',

  // 群管理设置
  GROUP_SETTING: '2034',

  // 修改群公告
  GROUP_NOTICE_SETTING: '2035',

  // 一键分配
  ONE_KEY_DISTRIBUTION: '2036',

  // 自动通过好友增删改
  AUTO_PASS_FRIEND: '2137',

  // 新好友自动回复
  NEW_FRIEND_REPLY: '2138',

  // 新消息自动回复
  NEW_MESSAGE_REPLY: '2139',

  // 分配同部门微信好友
  DISTRIBUTION_DEPARTMENT_FRIEND: '2140',

  // 分配子部门微信好友
  DISTRIBUTION_SON_DEPARTMENT_FRIEND: '2141',

  // 创建公共分组
  CREATE_COMMON_GROUP: '2142',

  // 创建部门分组
  CREATE_DEPARTMENT_GROUP: '2143',

  // 删除同部门个人号
  DELETE_DEPARTMENT_WECHAT: '2144',

  // 删除子部门个人号
  DELETE_SON_DEPARTMENT_WECHAT: '2145',

  // 删除短信
  DELETE_MESSAGE: '2146',

  // 删除通话录音
  DELETE_CALL: '2147',

  // 设置设备权限
  EQUIPMENT_PERMISSIONS: '2148',

  // 导出同部门好友
  EXPORT_DEPARTMENT_WECHAT: '2149',

  // 导出子部门好友
  EXPORT_SON_DEPARTMENT_WECHAT: '2150',

  // 微客服截屏
  SCREENSHOTS: '2151',

  // 微客服发送文件
  SEND_FILE: '2152',

  // 消息推送
  MESSAGE_SEND: '2153',

  // 导出全部好友
  EXPORT_ALL: '1121',

  AUTOMATED_TASK: '2075'

})
