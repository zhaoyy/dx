import {MESSAGE_TPYE, MESSAGE_TPYE_KEY} from './messageTypeAndKey'
import QREPLAY_GROUP_TPYE from './qReplayGroupType'
import MMANAGE_GROUP_TPYE from './mManageGroupType'

export {
  MESSAGE_TPYE, // 消息类型
  MESSAGE_TPYE_KEY, // 消息类型-对应的键值
  QREPLAY_GROUP_TPYE, // 设备管理--快捷回复--新增快捷回复分组-分组类型
  MMANAGE_GROUP_TPYE // 设备管理--素材管理--新增素材分组-分组类型
}
