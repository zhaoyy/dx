import {
  Constant
} from 'vma-vue-assist'

const MMANAGE_GROUP_TPYE = new Constant([{
  value: 1,
  label: '公共素材'
}, {
  value: 2,
  label: '部门素材'
}])

export default MMANAGE_GROUP_TPYE.valueList()
