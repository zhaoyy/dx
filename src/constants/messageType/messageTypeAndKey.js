import {
  Constant
} from 'vma-vue-assist'

export const MESSAGE_TPYE = new Constant({
  1: '文本',
  2: '图片',
  3: '语音',
  4: '视频',
  5: '公众号图文',
  6: '小程序',
  7: '图文链接',
  8: '拉群口令'
})

export const MESSAGE_TPYE_KEY = new Constant({
  TXT: 1,
  IMG: 2,
  AUDIO: 3,
  VIDEO: 4,
  PUBLICK: 5,
  WECHAT: 6,
  IMGTXTLINK: 7,
  PULLGROUP: 8
})
