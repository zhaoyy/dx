import {
  Constant
} from 'vma-vue-assist'

const QREPLAY_GROUP_TPYE = new Constant([{
  value: 1,
  label: '私有快捷语'
}, {
  value: 2,
  label: '公共快捷语'
},
{
  value: 3,
  label: '公司库'
}])

export default QREPLAY_GROUP_TPYE.valueList()
