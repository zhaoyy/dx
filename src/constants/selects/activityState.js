import {
  Constant
} from 'vma-vue-assist'

// 活动状态
const ACTIVITY_ATATE = new Constant([{
  value: 0,
  label: '未开始'
}, {
  value: 1,
  label: '进行中'
}, {
  value: 2,
  label: '已结束'
}])

export default ACTIVITY_ATATE.valueList()
