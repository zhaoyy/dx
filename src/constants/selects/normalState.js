import {
  Constant
} from 'vma-vue-assist'

// 异常
const NORMAL_STATE = new Constant([{
  value: 0,
  label: '正常'
}, {
  value: 1,
  label: '异常'
}])

export default NORMAL_STATE.valueList()
