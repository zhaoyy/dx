import {
  Constant
} from 'vma-vue-assist'

// 审核类型
const AUDIT_STATUS = new Constant([{
  value: 1,
  label: '待审核'
}, {
  value: 2,
  label: '已通过'
}, {
  value: 3,
  label: '已拒绝'
}])

export default AUDIT_STATUS.valueList()
