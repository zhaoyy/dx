import {
  Constant
} from 'vma-vue-assist'

// 性别选择
const SELECT_SEX = new Constant([{
  value: 1,
  label: '男'
}, {
  value: 2,
  label: '女'
}, {
  value: 0,
  label: '未知'
}])

export default SELECT_SEX.valueList()
