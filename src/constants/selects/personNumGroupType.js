import {
  Constant
} from 'vma-vue-assist'

const PERSON_NUM_GROUP_TYPE = new Constant([
  { value: 3, label: '个人号分组' },
  { value: 1, label: '好友分组' },
  { value: 2, label: '群分组' }
])

export default PERSON_NUM_GROUP_TYPE.valueList()
