import {
  Constant
} from 'vma-vue-assist'

// 性别选择
const PAYOUT_TYPE = new Constant([{
  value: 1,
  label: '首绑有礼'
}, {
  value: 2,
  label: '追评有礼'
}, {
  value: 3,
  label: '加购有礼'
}, {
  value: 4,
  label: '收藏有礼'
}, {
  value: 5,
  label: '新品投票'
}, {
  value: 6,
  label: '买家秀'
}, {
  value: 7,
  label: '红包'
}])
export default PAYOUT_TYPE.valueList()
