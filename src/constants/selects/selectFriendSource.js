import {
  Constant
} from 'vma-vue-assist'

const SELECT_ALLOT = new Constant([
  /*
  { value: 1, label: '名片' },
  { value: 2, label: '二维码' },
  { value: 3, label: '群聊' },
  { value: 4, label: '微信号' },
  { value: 5, label: '手机号' },
  { value: 6, label: 'QQ号' }
  */
  { value: 1, label: '名片(主动)' },
  { value: 2, label: '二维码(主动)' },
  { value: 3, label: '群聊(主动)' },
  { value: 4, label: '微信号(主动)' },
  { value: 5, label: '手机号(主动)' },
  { value: 6, label: 'QQ号(主动)' },
  { value: 7, label: '手机通讯录(主动)' },
  { value: 101, label: '名片(被动)' },
  { value: 102, label: '二维码(被动)' },
  { value: 103, label: '群聊(被动)' },
  { value: 104, label: '微信号(被动)' },
  { value: 105, label: '手机号(被动)' },
  { value: 106, label: 'QQ号(被动)' },
  { value: 107, label: '手机通讯录(被动)' }
])

export default SELECT_ALLOT.valueList()
