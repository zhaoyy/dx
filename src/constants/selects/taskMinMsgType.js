import {
  Constant
} from 'vma-vue-assist'

const TSAKMINMSGTYPE = new Constant([{
    code: '01001',
    name: '自动化消息'
  },
  {
    code: '02001',
    name: '追加销售任务'
  },
  {
    code: '03001',
    name: '临牌变更任务'
  },
  {
    code: '03002',
    name: '首次自荐任务'
  },
  {
    code: '03003',
    name: '黏客回访任务'
  },
  {
    code: '03004',
    name: '节日关怀任务'
  },
  {
    code: '03005',
    name: '生日关怀任务'
  },
  {
    code: '03006',
    name: '理赔关怀任务'
  },
  {
    code: '03007',
    name: '服务关怀任务'
  },
  {
    code: '03008',
    name: '定时会员礼任务'
  },
  {
    code: '03009',
    name: '客户推介任务'
  },
  {
    code: '03010',
    name: '车辆年检提醒任务'
  },
  {
    code: '03011',
    name: '黏客活动任务'
  },
  {
    code: '03012',
    name: '送返修回访任务'
  },
  {
    code: '03013',
    name: '加微任务'
  },
  {
    code: '04001',
    name: '车生活用户提醒'
  },
  {
    code: '04002',
    name: '官微用户提醒'
  },
  {
    code: '04003',
    name: '销售消息提醒'
  }
])

export default TSAKMINMSGTYPE.valueList()
