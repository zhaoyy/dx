import {
  Constant
} from 'vma-vue-assist'

const PERSON_NUM_GROUP_OWER = new Constant([
  { value: 1, label: '公共分组' },
  { value: 2, label: '部门分组' },
  { value: 0, label: '个人分组' }
])

export default PERSON_NUM_GROUP_OWER.valueList()
