import {
  Constant
} from 'vma-vue-assist'

export default new Constant({
  // 1: { value: 1, label: '名片' },
  // 2: { value: 2, label: '二维码' },
  // 3: { value: 3, label: '群聊' },
  // 4: { value: 4, label: '微信号' },
  // 5: { value: 5, label: '手机号' },
  // 6: { value: 6, label: 'QQ号' }
  1: { value: 1, label: '名片(主动)' },
  2: { value: 2, label: '二维码(主动)' },
  3: { value: 3, label: '群聊(主动)' },
  4: { value: 4, label: '微信号(主动)' },
  5: { value: 5, label: '手机号(主动)' },
  6: { value: 6, label: 'QQ号(主动)' },
  7: { value: 7, label: '手机通讯录(主动)' },
  101: { value: 101, label: '名片(被动)' },
  102: { value: 102, label: '二维码(被动)' },
  103: { value: 103, label: '群聊(被动)' },
  104: { value: 104, label: '微信号(被动)' },
  105: { value: 105, label: '手机号(被动)' },
  106: { value: 106, label: 'QQ号(被动)' },
  107: { value: 107, label: '手机通讯录(被动)' }
})
