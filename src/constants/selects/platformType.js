import {
  Constant
} from 'vma-vue-assist'

// 平台类型
const PLATFORM_TYPE = new Constant([{
  value: 1,
  label: '淘宝'
}, {
  value: 2,
  label: '京东'
}, {
  value: 3,
  label: '拼多多'
}])

export default PLATFORM_TYPE.valueList()
