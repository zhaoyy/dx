import {
  Constant
} from 'vma-vue-assist'

const RISK_TYPE = new Constant([
  { value: 1, label: '阻止并预警' },
  { value: 2, label: '仅预警' },
  { value: 3, label: '客户调研词' },
  { value: 4, label: '坐席调研词' }
])

export default RISK_TYPE.valueList()
