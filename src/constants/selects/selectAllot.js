import {
  Constant
} from 'vma-vue-assist'

const SELECT_ALLOT = new Constant([{
  value: 1,
  label: '已分配'
}, {
  value: 0,
  label: '未分配'
}])

export default SELECT_ALLOT.valueList()
