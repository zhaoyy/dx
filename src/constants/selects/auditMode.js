import {
  Constant
} from 'vma-vue-assist'

// 活动状态
const AUDIT_MODE = new Constant([{
  value: 2,
  label: '自动审核'
}, {
  value: 1,
  label: '人工审核'
}])

export default AUDIT_MODE.valueList()
