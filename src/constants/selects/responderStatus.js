import {
  Constant
} from 'vma-vue-assist'

// 未设置
const RESPONDER_STATUS = new Constant([{
  value: 1,
  label: '未设置'
}, {
  value: 2,
  label: '异常'
}, {
  value: 0,
  label: '正常'
}])

export default RESPONDER_STATUS.valueList()
