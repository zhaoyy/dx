import {
  Constant
} from 'vma-vue-assist'

// 在线状态
const SELECT_ONLINE_STATE = new Constant([{
  value: 1,
  label: '在线'
}, {
  value: 0,
  label: '离线'
}])

export default SELECT_ONLINE_STATE.valueList()
