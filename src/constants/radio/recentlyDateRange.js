import {
  Constant
} from 'vma-vue-assist'

const RECENTLY_DATE_RANGE = new Constant({
  YESTERDAY: {
    value: 1,
    day: 1,
    label: '昨日'
  },
  LAST_SEVEN_DAYS: {
    value: 2,
    day: 7,
    label: '近7日'
  },
  LAST_THIRTY_DAYS: {
    value: 3,
    day: 30,
    label: '近30日'
  }
})

const RECENTLY_DATE_RANGE_LIST = RECENTLY_DATE_RANGE.valueList()

export {
  RECENTLY_DATE_RANGE,
  RECENTLY_DATE_RANGE_LIST
}
