import {
  Constant
} from 'vma-vue-assist'

export default new Constant({
  // 私聊
  C2C: 1,
  // 群聊
  GROUP: 2
})
