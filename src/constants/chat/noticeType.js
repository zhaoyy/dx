import {
  Constant
} from 'vma-vue-assist'

export default new Constant({
  // 好友上下线
  ON_LINE_STATUS: 1,
  // 转接好友
  TRANSFER_FRIEND: 2,
  // 好友被转接
  FRIEND_TRANSFERRED: 3,
  // 更新敏感词
  REFRESH_SENSITIVE_WORDS: 4,
  // 转接好友，接收方微客服拉取转接信息
  TRANSFER_FRIEND_RECEIVE_PULL: 5,
  // 转接好友，转出方微客服拉取转接信息
  TRANSFER_FRIEND_TURNOUT_PULL: 6,
  // 删除个人号
  DELETE_ACCOUNT: 7,
  // 删除好友/群聊
  DELETE_FREIND: 8,
  // 群聊新成员入群
  NEW_GROUP_MEMBER: 9,
  // 待办事项
  TODO_THINGS: 10,
  // 小程序
  MINI_Program:33
})
