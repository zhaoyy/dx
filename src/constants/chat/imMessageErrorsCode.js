import {
  Constant
} from 'vma-vue-assist'

export default new Constant({
  // 消息体内容过大
  MSG_TOO_LONG: 80002
})
