import RechargeRedpacket from './rechargeRedpacket'
import OffLine from './offLine'
import LinkCode from './linkCode'
import CodePay from './codePay'
import ApplyDetails from './applyDetails'
import RefuseApply from './refuseApply'
import OrderDetails from './orderDetails'
import NewCircle from './newCircle'
import ViewCircle from './viewCircle'
import SelectPersonalNum from './selectPersonalNum'
import ActivityDetails from './activityDetails'
import RechargeSms from './rechargeSms'
import EditTemplate from './editTemplate'
import NewTemplate from './newTemplate'
import NewGroupSend from './newGroupSend'
import ViewGroupSend from './viewGroupSend'
import NewMicroJump from './newMicroJump'
import MicroJumpCode from './microJumpCode'
import MiQrcode from './miQrcode'

export {
  RechargeRedpacket, // 红包营销 => 充值红包
  CodePay, // 充值红包 => 二维码付款
  OffLine, // 晒图活动 => 晒图活动 => 下线
  LinkCode, // 晒图活动 => 晒图活动 => 链接二维码
  ActivityDetails, // 晒图活动 =>晒图活动 => 统计（活动详情）
  ApplyDetails, // 晒图活动 =>活动审核 => 申请详情
  RefuseApply, // 晒图活动 =>活动审核 => 拒绝理由
  OrderDetails, // 晒图活动 =>活动审核 => 订单详情
  NewCircle, // 朋友圈营销 =>定时发送朋友圈 => 发朋友圈
  ViewCircle, // 朋友圈营销 =>定时发送朋友圈 => 查看
  SelectPersonalNum, // 朋友圈营销 =>定时发送朋友圈 => 选择个人号
  RechargeSms, // 短信营销 =>短信充值 => 充值
  EditTemplate, // 短信营销 =>短信历史模板 => 编辑
  NewTemplate, // 短信营销 =>短信历史模板 => 新增
  NewGroupSend, // 短信营销 =>短信群发 => 新增
  ViewGroupSend, // 短信营销 =>短信群发 => 查看
  NewMicroJump, // 微跳转 => 转换链接(相当于新增)
  MicroJumpCode, // 微跳转 => 转换后的二维码
  MiQrcode // 微跳转 => 列表展示所用
}
