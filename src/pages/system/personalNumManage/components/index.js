import pNumListDialogCode from './pNumListDialogCode'
import pNumListDialogSetGroup from './pNumListDialogSetGroup'
import pNumListDialogRemark from './pNumListDialogRemark'
import { chatRecordFriends, chatRecordGroup } from '@/components/common/chatRecords'
import pNumListDialogDelete from './pNumListDialogDelete'
import fListDialogSetGroup from './fListDialogSetGroup.vue'
import fListDialogAssignStaff from './fListDialogAssignStaff'
import fListGViewDialogAssignStaff from './fListGViewDialogAssignStaff'
import fListDialogExportLog from './fListDialogExportLog'
import gViewDialogSetGroup from './gViewDialogSetGroup.vue'
import gViewDialogAssignStaff from './gViewDialogAssignStaff'
import gViewDialogExportLog from './gViewDialogExportLog'
import gViewDialogGroupMenbers from './gViewDialogGroupMenbers'
import gManageDialogAdd from './gManageDialogAdd'
import gManageDialogDelete from './gManageDialogDelete'
import gRecordsDetail from './gRecordsDetail'

export {
  pNumListDialogCode, // 个人好列表 => 二维码名片
  pNumListDialogSetGroup, // 个人好列表 => 设置分组
  pNumListDialogRemark, // 个人好列表 => 修改微信备注
  chatRecordFriends, // 个人好列表 => 好友聊天记录
  chatRecordGroup, // 个人好列表 =>群聊天记录
  pNumListDialogDelete, // 个人好列表 => 删除个人号

  fListDialogSetGroup, // 微信好友列表 => 设置分组
  fListDialogAssignStaff, // 微信好友列表 => 分配员工
  fListDialogExportLog, // 微信好友列表 => 下载聊天记录

  gViewDialogSetGroup, // 微信群查看 => 设置分组
  gViewDialogAssignStaff, // 微信群查看 => 分配员工
  gViewDialogExportLog, // 微信群查看 => 下载聊天记录
  gViewDialogGroupMenbers, // 微信群查看 => 查看群成员

  fListGViewDialogAssignStaff, // 微信好友列表与微信群列表 => 一键分配员工

  gManageDialogAdd, // 分组管理 => 新增、修改
  gManageDialogDelete, // 分组管理 => 删除

  gRecordsDetail // 群发记录 => 群发明细
}
