export default {
  title: {
    text: ''
  },
  tooltip: {
    trigger: 'axis'
  },
  color: ['#1493E9'],
  // color: ['#1493E9', '#FEC870', '#36A4F1', '#FF786E'],
  legend: {
    orient: 'vertical',
    x: 'right',
    y: 'center',
    data: []
  },
  toolbox: {
    show: true,
    right: 90,
    feature: {
      mark: {
        show: true
      },
      // dataView: {
      //   show: true,
      //   readOnly: true,
      //   optionToContent
      // },
      // magicType: {
      //   show: true,
      //   type: ['line', 'bar']
      // },
      restore: {
        show: true
      },
      saveAsImage: {
        show: true
      }
    }
  },
  calculable: true,
  xAxis: [{
    type: 'category',
    boundaryGap: false,
    data: []
  }],
  // dataZoom: [{ type: 'inside' }],
  yAxis: [{
    type: 'value'
  }],
  series: [{
    type: 'line',
    data: []
  }]
}
