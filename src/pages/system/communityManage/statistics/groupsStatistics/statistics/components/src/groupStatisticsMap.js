const GROUP_STATISTICS_MAP = {
  GROUP_INCR: {
    title: '群净增',
    content: '当天累计的微信群数 - 前一天累计的微信群数',
    key: 'groupIncr'
  },
  // 群总数
  GROUP_SUM: {
    title: '群总数',
    content: '当天累计的微信群数',
    key: 'groupSum'
  },
  // 群成员净增
  GROUP_USER_INCR: {
    title: '群成员净增',
    content: '当天累计的群成员数 - 前一天累计的群成员数',
    key: 'groupUserIncr'
  },
  // 群成员总数
  GROUP_USER_SUM: {
    title: '群成员总数',
    content: '当天累计的群成员数',
    key: 'groupUserSum'
  },
  // 群成员去重总数
  GROUP_USER_DISTINCT_SUM: {
    title: '群成员去重总数',
    content: '群成员总数筛选去掉重复成员后的总数',
    key: 'groupUserDistinctSum'
  }
}

export default GROUP_STATISTICS_MAP
