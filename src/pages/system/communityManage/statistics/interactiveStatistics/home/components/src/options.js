export default {
    title: {
        text: ''
    },
    tooltip: {
        trigger: 'axis'
    },
    color: ['#1493E9'],
    legend: {
        orient: 'vertical',
        x: 'right',
        y: 'center',
        data: []
    },
    toolbox: {
        show: true,
        right: 30,
        feature: {
            mark: {
                show: true
            },
            // dataView: {
            //   show: true,
            //   readOnly: true,
            //   optionToContent
            // },
            // magicType: {
            //   show: true,
            //   type: ['line', 'bar']
            // },
            restore: {
                show: true
            },
            saveAsImage: {
                show: true
            }
        }
    },
    calculable: true,
    xAxis: [{
        type: 'category',
        boundaryGap: false,
        data: []
    }],
    // dataZoom: [{ type: 'inside' }],
    yAxis: [{
        type: 'value'
    }],
    series: [{
        type: 'line',
        data: []
    }]
}