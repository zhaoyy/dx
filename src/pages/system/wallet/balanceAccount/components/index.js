import accountSet from './accountSet'
import accountRecord from './accountRecord'
import RechargeRedpacket from './rechargeRedpacket'
import { CodePay } from '@/components/common/wallet'
import dialogAddWechat from './dialogAddWechat'
import stepList from './stepList'
import addAccountStepA from './addAccountStepA'
import addAccountStepB from './addAccountStepB'
import addAccountStepC from './addAccountStepC'
import addAccountStepD from './addAccountStepD'
import addAccountStepE from './addAccountStepE'
import addAccountStepF from './addAccountStepF'

export {
  accountSet, // 账户设置--账户设置
  accountRecord, // 账户设置--消费记录
  stepList, // 步骤
  addAccountStepA, // 配置支付账户--1
  addAccountStepB, // 配置支付账户--2
  addAccountStepC, // 配置支付账户--3
  addAccountStepD, // 配置支付账户--4
  addAccountStepE, // 配置支付账户--5
  addAccountStepF, // 配置支付账户--6
  RechargeRedpacket, // 红包营销 => 充值红包
  CodePay, // 充值红包 => 二维码付款
  dialogAddWechat // 账户设置 => 添加公众号
}
