export default {
  title: {
    text: '敏感操作统计',
    x: 'center',
    textStyle: {
      color: '#2897FF'
    }
  },
  color: ['#37a2da', '#31c5e9', '#67e0e3', '#9fe6b8', '#ffdb5d', '#ff9f7e', '#fb7293', '#e063ae', '#e791d1', '#e7bcf3',
    '#9d97f5', '#8378e9', '#96bfff'
  ],
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b} : {c} ({d}%)'
  },
  legend: {
    orient: 'vertical',
    x: 'left',
    y: 'bottom',
    left: '5%',
    data: []
  },
  calculable: true,
  series: [{
    name: '统计结果',
    type: 'pie',
    radius: '50%',
    center: ['60%', '50%'],
    itemStyle: {
      normal: {
        label: {
          show: true,
          formatter: '{b} : {c} ({d}%)'
        },
        labelLine: {
          show: true
        }
      }
    },
    data: []
  }]
}
