export default {
  title: {
    text: '好友性别统计',
    x: 'center',
    textStyle: {
      color: '#2897FF'
    }
  },
  color: ['#1F9FF6', '#44C89A', '#FEC870'],
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b} : {c} ({d}%)'
  },
  legend: {
    orient: 'vertical',
    x: 'left',
    y: 'bottom',
    data: ['未知', '男', '女']
  },
  calculable: true,
  series: [{
    name: '统计结果',
    type: 'pie',
    radius: '60%',
    center: ['50%', '60%'],
    itemStyle: {
      normal: {
        label: {
          show: true,
          formatter: '{b} : {c} ({d}%)'
        },
        labelLine: {
          show: true
        }
      }
    },
    data: []
  }]
}
