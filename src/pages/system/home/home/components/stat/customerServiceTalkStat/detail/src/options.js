import {
  optionToContent
} from '@/utils'

export default {
  title: {
    text: '',
    top: 6
  },
  tooltip: {
    trigger: 'axis'
  },
  color: ['#FF786E', '#44C89A', '#FEC870', '#36A4F1'],
  legend: {
    data: ['聊天总数', '与男性好友聊天总数', '与女性好友聊天总数', '群聊总数'],
    top: 6
  },
  toolbox: {
    show: true,
    top: 20,
    right: 20,
    feature: {
      mark: {
        show: true
      },
      dataView: {
        show: true,
        readOnly: false,
        optionToContent
      },
      magicType: {
        show: true,
        type: ['line', 'bar']
      },
      restore: {
        show: true
      },
      saveAsImage: {
        show: true
      }
    }
  },
  grid: {
    top: 40,
    right: 40,
    bottom: 30,
    left: 40
  },
  calculable: true,
  xAxis: [{
    type: 'category',
    boundaryGap: false,
    data: []
  }],
  yAxis: [{
    type: 'value'
  }],
  series: [{
    name: '聊天总数',
    type: 'line',
    data: []
  }, {
    name: '与男性好友聊天总数',
    type: 'line',
    data: []
  }, {
    name: '与女性好友聊天总数',
    type: 'line',
    data: []
  }, {
    name: '群聊总数',
    type: 'line',
    data: []
  }]
}
