import {
  optionToContent
} from '@/utils'

export default {
  title: {
    text: '',
    top: 6
  },
  tooltip: {
    trigger: 'axis'
  },
  color: ['#FF786E', '#44C89A', '#FEC870', '#36A4F1', '#B6A2DE'],
  legend: {
    data: ['聊天好友总数', '男性聊天好友总数', '女性聊天好友总数', '群聊总数', '未回复总数'],
    top: 6
  },
  toolbox: {
    show: true,
    top: 15,
    right: 20,
    feature: {
      mark: {
        show: true
      },
      dataView: {
        show: true,
        readOnly: true,
        optionToContent
      },
      magicType: {
        show: true,
        type: ['line', 'bar']
      },
      restore: {
        show: true
      },
      saveAsImage: {
        show: true
      }
    }
  },
  grid: {
    top: 40,
    right: 40,
    bottom: 30,
    left: 40
  },
  calculable: true,
  xAxis: [{
    type: 'category',
    boundaryGap: false,
    data: []
  }],
  yAxis: [{
    type: 'value'
  }],
  series: [{
    name: '聊天好友总数',
    type: 'line',
    data: []
  }, {
    name: '男性聊天好友总数',
    type: 'line',
    data: []
  }, {
    name: '女性聊天好友总数',
    type: 'line',
    data: []
  }, {
    name: '群聊总数',
    type: 'line',
    data: []
  }, {
    name: '未回复总数',
    type: 'line',
    data: []
  }]
}
