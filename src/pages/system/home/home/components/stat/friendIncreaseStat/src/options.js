import {
  optionToContent
} from '@/utils'

export default {
  title: {
    text: '好友增长趋势',
    top: 6
  },
  tooltip: {
    trigger: 'axis'
  },
  color: ['#44C89A', '#FEC870', '#36A4F1', '#FF786E'],
  legend: {
    data: ['男性', '女性', '未知', '全部'],
    top: 6
  },
  toolbox: {
    show: true,
    right: 30,
    feature: {
      mark: {
        show: true
      },
      dataView: {
        show: true,
        readOnly: true,
        optionToContent
      },
      magicType: {
        show: true,
        type: ['line', 'bar']
      },
      restore: {
        show: true
      },
      saveAsImage: {
        show: true
      }
    }
  },
  calculable: true,
  xAxis: [{
    type: 'category',
    boundaryGap: false,
    data: []
  }],
  yAxis: [{
    type: 'value'
  }],
  series: [{
    name: '男性',
    type: 'line',
    data: []
  }, {
    name: '女性',
    type: 'line',
    data: []
  }, {
    name: '未知',
    type: 'line',
    data: []
  }, {
    name: '全部',
    type: 'line',
    data: []
  }]
}
