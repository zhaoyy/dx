import {
  optionToContent
} from '@/utils'

export default {
  title: {
    text: '',
    top: 6
  },
  tooltip: {
    trigger: 'axis'
  },
  color: ['#FF786E', '#44C89A', '#FEC870', '#36A4F1'],
  legend: {
    data: ['在线时长(H)'],
    top: 6
  },
  toolbox: {
    show: true,
    top: 15,
    right: 20,
    feature: {
      mark: {
        show: true
      },
      dataView: {
        show: true,
        readOnly: true,
        optionToContent
      },
      magicType: {
        show: true,
        type: ['line', 'bar']
      },
      restore: {
        show: true
      },
      saveAsImage: {
        show: true
      }
    }
  },
  grid: {
    top: 40,
    right: 40,
    bottom: 30,
    left: 80
  },
  calculable: true,
  xAxis: [{
    type: 'category',
    boundaryGap: false,
    data: []
  }],
  yAxis: [{
    type: 'value'
  }],
  series: [{
    name: '在线时长(H)',
    type: 'line',
    data: []
  }]
}
