require('echarts-wordcloud')
export default {
  tooltip: {
    trigger: 'item',
    showDelay: 0,
    transitionDuration: 0.2
  },
  series: [{
    type: 'wordCloud',
    gridSize: 20,
    width: '94%',
    height: '94%',
    drawOutOfBound: false,
    sizeRange: [12, 36],
    rotationRange: [0, 0],
    left: 'center',
    top: 'center',
    textStyle: {
      normal: {
        fontFamily: 'sans-serif',
        fontWeight: 'normal',
        color: function() {
          return 'rgb(' + [
            Math.round(Math.random() * 160),
            Math.round(Math.random() * 160),
            Math.round(Math.random() * 160)
          ].join(',') + ')'
        }
      }
    },
    data: []
  }]
}
