export default {
  tooltip: {
    trigger: 'item',
    showDelay: 0,
    transitionDuration: 0.2
  },
  visualMap: {
    min: 0,
    max: 60000,
    itemWidth: 10, // 图形的宽度，即每个小块的宽度。
    itemHeight: 180, // 图形的高度，即每个小块的高度。
    inRange: {
      color: ['#01d4e7', '#7c56c1', '#59aac8', '#16b18b', '#fbd062', '#eeb311', '#f5791e', '#d92c28']

    },
    text: ['高', '低'],
    calculable: true
  },
  series: [{
    name: '中国地图',
    type: 'map',
    roam: false,
    map: 'china',
    top: '4%',
    left: '8%',
    right: '8%',
    bottom: '4%',
    zoom: 0.9,
    label: {
      normal: {
        show: false,
        color: '#454545',
        fontSize: 8
      },
      emphasis: {
        show: true
      }
    },
    itemStyle: {
      normal: {
        borderWidth: 1,
        borderColor: '#fff'
      },
      emphasis: {
        borderWidth: 2,
        borderColor: '#fff',
        areaColor: '#fed183'
      }
    }
  }]
}
