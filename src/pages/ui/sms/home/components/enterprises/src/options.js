export default {
  color: ['#44C89A', '#FEC870', '#36A4F1', '#FF786E'],
  legend: {
    data: ['男性', '女性', '未知', '全部']
  },
  xAxis: [{
    type: 'category',
    boundaryGap: false,
    data: ['2018-12-14 03', '2018-12-14 04', '2018-12-14 05', '2018-12-14 06', '2018-12-14 07', '2018-12-14 08', '2018-12-14 09', '2018-12-14 10']
  }],
  yAxis: [{
    type: 'value'
  }],
  series: [
    {
      name: '男性',
      type: 'line',
      data: [34, 43, 67, 45, 70, 46, 10]
    },
    {
      name: '女性',
      type: 'line',
      data: [56, 76, 67, 33, 70, 54, 10]
    },
    {
      name: '未知',
      type: 'line',
      data: [34, 65, 60, 45, 44, 33, 60]
    },
    {
      name: '全部',
      type: 'line',
      data: [32, 30, 80, 45, 90, 46, 90]
    }
  ]
}
