import basicInfo from '../components/basicInfo'
import equipmentDetails from '../components/equipmentDetails'
import authorizedShop from '../components/authorizedShop'
import personalNum from '../components/personalNum'

export {
  basicInfo,
  equipmentDetails,
  authorizedShop,
  personalNum
}
