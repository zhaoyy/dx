import * as types from '@/router/ui/system/types'
const params = [
  {
    deep: 2,
    index: '1',
    title: '首页',
    icon: 'el-icon-location',
    child: [
      {
        index: '1-1',
        title: '首页',
        icon: 'el-icon-location',
        name: types.HOME
      },
      {
        index: '1-2',
        title: '通知',
        icon: 'el-icon-location',
        name: types.NOTICE
      }
    ]
  },
  {
    deep: 2,
    index: '2',
    title: '个人号管理',
    icon: 'el-icon-location',
    child: [
      {
        index: '2-1',
        title: '个人号列表',
        icon: 'el-icon-location',
        name: types.PERSON_NUM_LIST
      },
      {
        index: '2-2',
        title: '微信好友列表',
        icon: 'el-icon-location',
        name: types.FRIENDS_LIST
      },
      {
        index: '2-3',
        title: '微信群查看',
        icon: 'el-icon-location',
        name: types.GROUP_VIEW
      },
      {
        index: '2-4',
        title: '分组管理',
        icon: 'el-icon-location',
        name: types.GROUP_MANAGE
      },
      {
        index: '2-5',
        title: '分配记录',
        icon: 'el-icon-location',
        name: types.DISTRIBUTION_RECORD
      }
    ]
  },
  {
    deep: 2,
    index: '3',
    title: '设备管理',
    icon: 'el-icon-location',
    child: [
      {
        index: '3-1',
        title: '设备管理',
        icon: 'el-icon-location',
        name: types.DEVICE_MANAGE
      },
      {
        index: '3-2',
        title: '设备分组',
        icon: 'el-icon-location',
        name: types.DEVICE_GROUP
      },
      {
        index: '3-3',
        title: '分配记录',
        icon: 'el-icon-location',
        name: types.DEVICE_DISTRIBUTION
      },
      {
        index: '3-4',
        title: '微信设置',
        icon: 'el-icon-location',
        name: types.WECHAT_SETTING
      },
      {
        index: '3-5',
        title: '快捷回复',
        icon: 'el-icon-location',
        name: types.QUICK_REPLY
      },
      {
        index: '3-6',
        title: '下属快捷回复',
        icon: 'el-icon-location',
        name: types.SUBORDINATES_REPLY
      },
      {
        index: '3-7',
        title: '自动通过好友',
        icon: 'el-icon-location',
        name: types.AUTO_FRIENDS
      },
      {
        index: '3-8',
        title: '全局自动回复',
        icon: 'el-icon-location',
        name: types.GLOBAL_REPLY
      },
      {
        index: '3-9',
        title: '个人号自动回复',
        icon: 'el-icon-location',
        name: types.PERSONAL_NUM_AUTO_REPLY
      },
      {
        index: '3-10',
        title: '素材管理',
        icon: 'el-icon-location',
        name: types.MATERIAL_MANAGE
      }
    ]
  },
  {
    deep: 3,
    index: '4',
    title: '营销平台',
    icon: 'el-icon-location',
    child: [
      {
        index: '4-1',
        title: '红包营销',
        icon: 'el-icon-location',
        child: [
          {
            index: '4-1-1',
            title: '红包充值',
            icon: 'el-icon-location',
            name: types.PACKETS_RECHARGE
          },
          {
            index: '4-1-2',
            title: '红包记录',
            icon: 'el-icon-location',
            name: types.PACKETS_RECORD
          }
        ]
      },
      {
        index: '4-2',
        title: '晒图活动',
        icon: 'el-icon-location',
        child: [
          {
            index: '4-2-1',
            title: '晒图活动',
            icon: 'el-icon-location',
            name: types.SHOW_PICTURE
          },
          {
            index: '4-2-2',
            title: '活动审核',
            icon: 'el-icon-location',
            name: types.ACTIVITY_AUDIT
          },
          {
            index: '4-2-3',
            title: '活动支出记录',
            icon: 'el-icon-location',
            name: types.ACTIVITY_RECORD
          }
        ]
      },
      {
        index: '4-3',
        title: '短信营销',
        icon: 'el-icon-location',
        child: [
          {
            index: '4-3-1',
            title: '短信充值',
            icon: 'el-icon-location',
            name: types.SMS_RECHARGE
          },
          {
            index: '4-3-2',
            title: '短信群发',
            icon: 'el-icon-location',
            name: types.GROUP_SEND
          },
          {
            index: '4-3-3',
            title: '短信历史模板',
            icon: 'el-icon-location',
            name: types.HISTORY_TEMPLATE
          },
          {
            index: '4-3-4',
            title: '短信发送记录',
            icon: 'el-icon-location',
            name: types.SEND_RECORD
          }
        ]
      },
      {
        index: '4-4',
        title: '朋友圈营销',
        icon: 'el-icon-location',
        child: [
          {
            index: '4-4-1',
            title: '定时发送朋友圈',
            icon: 'el-icon-location',
            name: types.REGULAR_CIRCLE
          },
          {
            index: '4-4-2',
            title: '朋友圈自动点赞',
            icon: 'el-icon-location',
            name: types.AUTO_PRAISE
          }
        ]
      },
      {
        index: '4-5',
        title: '微跳转',
        icon: 'el-icon-location',
        name: types.MICRO_JUMP
      }, {
        index: '4-1',
        title: '首绑有理',
        icon: 'el-icon-location',
        child: [
          {
            index: '4-1-1',
            title: '首绑有理',
            icon: 'el-icon-location',
            name: types.BINDING_GIFT
          }, {
            index: '4-1-2',
            title: '绑定记录',
            icon: 'el-icon-location',
            name: types.BINDING_RECORDING
          }
        ]
      }, {
        index: '4-2',
        title: '追评有理',
        icon: 'el-icon-location',
        child: [
          {
            index: '4-2-1',
            title: '追评有理',
            icon: 'el-icon-location',
            name: types.ADD_EVA_GIFT
          }, {
            index: '4-2-2',
            title: '活动审核',
            icon: 'el-icon-location',
            name: types.ADD_EVA_AUDIT
          }, {
            index: '4-2-3',
            title: '支出记录',
            icon: 'el-icon-location',
            name: types.ADD_EVA_OUTLAY
          }
        ]
      }, {
        index: '4-3',
        title: '加购有理',
        icon: 'el-icon-location',
        child: [
          {
            index: '4-3-1',
            title: '加购有理',
            icon: 'el-icon-location',
            name: types.ADD_CART_GIFT
          }, {
            index: '4-3-2',
            title: '活动审核',
            icon: 'el-icon-location',
            name: types.ADD_CART_AUDIT
          }, {
            index: '4-3-3',
            title: '支出记录',
            icon: 'el-icon-location',
            name: types.ADD_CART_OUTLAY
          }
        ]
      }, {
        index: '4-4',
        title: '收藏有礼',
        icon: 'el-icon-location',
        child: [
          {
            index: '4-4-1',
            title: '收藏有礼',
            icon: 'el-icon-location',
            name: types.COLLECTION_GIFT
          }, {
            index: '4-4-2',
            title: '活动审核',
            icon: 'el-icon-location',
            name: types.COLLECTION_AUDIT
          }, {
            index: '4-4-3',
            title: '支出记录',
            icon: 'el-icon-location',
            name: types.COLLECTION_OUTLAY
          }
        ]
      }, {
        index: '4-5',
        title: '新品投票',
        icon: 'el-icon-location',
        child: [
          {
            index: '4-5-1',
            title: '新品投票',
            icon: 'el-icon-location',
            name: types.NEWVOTE
          }, {
            index: '4-5-2',
            title: '活动审核',
            icon: 'el-icon-location',
            name: types.NEWVOTE_AUDIT
          }, {
            index: '4-5-3',
            title: '支出记录',
            icon: 'el-icon-location',
            name: types.NEWVOTE_OUTLAY
          }
        ]
      }, {
        index: '4-6',
        title: '买家秀',
        icon: 'el-icon-location',
        child: [
          {
            index: '4-6-1',
            title: '买家秀',
            icon: 'el-icon-location',
            name: types.BUYER_SHOW
          },
          {
            index: '4-6-2',
            title: '活动审核',
            icon: 'el-icon-location',
            name: types.BUYER_SHOW_AUDIT
          },
          {
            index: '4-6-3',
            title: '支出记录',
            icon: 'el-icon-location',
            name: types.BUYER_SHOW_OUTLAY
          }
        ]
      }
    ]
  },
  {
    deep: 2,
    index: '5',
    title: '加粉管理',
    icon: 'el-icon-location',
    child: [
      {
        index: '5-1',
        title: '手机号加好友',
        icon: 'el-icon-location',
        name: types.PHONE_PLUS
      },
      {
        index: '5-2',
        title: '定时加好友',
        icon: 'el-icon-location',
        name: types.TIMING_PLUS
      },
      {
        index: '5-3',
        title: '店铺自动加好友',
        icon: 'el-icon-location',
        name: types.STORES_PLUS
      },
      {
        index: '5-4',
        title: '微活码',
        icon: 'el-icon-location',
        name: types.MICRO_CODE
      }
    ]
  },
  {
    deep: 3,
    index: '6',
    title: '风控管理',
    icon: 'el-icon-location',
    child: [
      {
        index: '6-1',
        title: '微信风控',
        icon: 'el-icon-location',
        child: [
          {
            index: '6-1-1',
            title: '敏感词设置',
            icon: 'el-icon-location',
            name: types.SENSITIVE_WORD_SET
          },
          {
            index: '6-1-2',
            title: '敏感操作',
            icon: 'el-icon-location',
            name: types.SENSITIVE_OPERATION
          },
          {
            index: '6-1-3',
            title: '敏感通知',
            icon: 'el-icon-location',
            name: types.SENSITIVE_NOTICE
          },
          {
            index: '6-1-4',
            title: '重复好友统计',
            icon: 'el-icon-location',
            name: types.FRIEND_STATISTICS
          },
          {
            index: '6-1-5',
            title: '重复群统计',
            icon: 'el-icon-location',
            name: types.GROUP_STATISTICS
          },
          {
            index: '6-1-6',
            title: '微信好友导出',
            icon: 'el-icon-location',
            name: types.FRIEND_EXPORT
          },
          {
            index: '6-1-7',
            title: '微信财务统计',
            icon: 'el-icon-location',
            name: types.FINANCIAL_STATISTICS
          }
        ]
      },
      {
        index: '6-2',
        title: '手机风控',
        icon: 'el-icon-location',
        child: [
          {
            index: '6-2-1',
            title: '通话录音',
            icon: 'el-icon-location',
            name: types.CALL_RECORDING
          },
          {
            index: '6-2-2',
            title: '短信敏感词',
            icon: 'el-icon-location',
            name: types.SMS_SENSITIVE_WORD
          },
          {
            index: '6-2-3',
            title: '手机短信',
            icon: 'el-icon-location',
            name: types.PHONE_SMS
          },
          {
            index: '6-2-4',
            title: '敏感短信',
            icon: 'el-icon-location',
            name: types.SENSITIVE_SMS
          },
          {
            index: '6-2-5',
            title: '敏感通知',
            icon: 'el-icon-location',
            name: types.PHONE_SENSITIVE_NOTICE
          },
          {
            index: '6-2-6',
            title: 'APP设置',
            icon: 'el-icon-location',
            name: types.APP_SETTINGS
          },
          {
            index: '6-2-7',
            title: '权限管理',
            icon: 'el-icon-location',
            name: types.AUTHORITY_MANAGE
          }
        ]
      }
    ]
  },
  {
    deep: 2,
    index: '7',
    title: '员工管理',
    icon: 'el-icon-location',
    child: [
      {
        index: '7-1',
        title: '部门管理',
        icon: 'el-icon-location',
        name: types.DEPARTMENT_MANAGE
      },
      {
        index: '7-2',
        title: '角色管理',
        icon: 'el-icon-location',
        name: types.ROLE_MANAGE
      }
    ]
  },
  {
    deep: 2,
    index: '8',
    title: '电商平台',
    icon: 'el-icon-location',
    child: [
      {
        index: '8-1',
        title: '店铺列表',
        icon: 'el-icon-location',
        name: types.STORE_LIST
      },
      {
        index: '8-2',
        title: '店铺订单',
        icon: 'el-icon-location',
        name: types.STORE_ORDER
      },
      {
        index: '8-3',
        title: '导入订单',
        icon: 'el-icon-location',
        name: types.IMPORT_ORDER
      }
    ]
  },
  {
    deep: 1,
    index: '9',
    title: '操作日志',
    icon: 'el-icon-location',
    name: types.OPERATION_LOG
  },
  {
    deep: 1,
    index: '10',
    title: '弹窗',
    icon: 'el-icon-location',
    name: types.DIALOGS
  },
  {
    deep: 1,
    index: '11',
    title: '菜单管理',
    icon: 'el-icon-location',
    name: types.MENU
  },
  {
    deep: 3,
    index: '12',
    title: '钱包',
    icon: 'el-icon-location',
    child: [
      {
        index: '12-1',
        title: '账户余额',
        icon: 'el-icon-location',
        child: [
          {
            index: '12-1-1',
            title: '账户设置',
            icon: 'el-icon-location',
            name: types.ACCOUNT_SETTINGS
          },
          {
            index: '12-1-2',
            title: '微码微控账户余额',
            icon: 'el-icon-location',
            name: types.M_CONTROL_ACCOUNT_BALANCE
          },
          {
            index: '12-1-3',
            title: '微客服红包记录',
            icon: 'el-icon-location',
            name: types.M_CUSTOMER_RED_RECORD
          }
        ]
      },
      {
        index: '12-2',
        title: '短信余额',
        icon: 'el-icon-location',
        child: [
          {
            index: '12-2-1',
            title: '短信充值',
            icon: 'el-icon-location',
            name: types.WALLET_SMS_RECHARGE
          }
        ]
      }
    ]
  }
]
export default params
