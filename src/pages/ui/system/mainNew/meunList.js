import * as types from '@/router/system/types'
const params = [
  {
    deep: 1,
    index: '1',
    title: '首页',
    icon: 'el-icon-location',
    name: types.HOME
  },
  {
    deep: 2,
    index: '2',
    title: '个人号管理',
    icon: 'el-icon-location',
    child: [
      {
        index: '2-1',
        title: '个人号列表1',
        icon: 'el-icon-location',
        name: types.PERSON_NUM_LIST,
        child: [
          {
            index: '2-1',
            title: '个人号列表',
            icon: 'el-icon-location',
            name: types.PERSON_NUM_LIST
          },
          {
            index: '2-2',
            title: '微信好友列表',
            icon: 'el-icon-location',
            name: types.FRIENDS_LIST
          },
          {
            index: '2-3',
            title: '微信群查看',
            icon: 'el-icon-location',
            name: types.GROUP_VIEW
          },
          {
            index: '2-4',
            title: '分组管理',
            icon: 'el-icon-location',
            name: types.GROUP_MANAGE
          },
          {
            index: '2-5',
            title: '分配记录',
            icon: 'el-icon-location',
            name: types.DISTRIBUTION_RECORD
          }
        ]
      },
      {
        index: '21-1',
        title: '个人号列表1',
        icon: 'el-icon-location',
        name: types.PERSON_NUM_LIST,
        child: [
          {
            index: '21-1',
            title: '个人号列表',
            icon: 'el-icon-location',
            name: types.PERSON_NUM_LIST
          },
          {
            index: '21-2',
            title: '微信好友列表',
            icon: 'el-icon-location',
            name: types.FRIENDS_LIST
          },
          {
            index: '21-3',
            title: '微信群查看',
            icon: 'el-icon-location',
            name: types.GROUP_VIEW
          },
          {
            index: '21-4',
            title: '分组管理',
            icon: 'el-icon-location',
            name: types.GROUP_MANAGE
          },
          {
            index: '21-5',
            title: '分配记录',
            icon: 'el-icon-location',
            name: types.DISTRIBUTION_RECORD
          }
        ]
      }
    ]
  },
  {
    deep: 2,
    index: '3',
    title: '设备管理',
    icon: 'el-icon-location',
    child: [
      {
        deep: 2,
        index: '3',
        title: '设备管理',
        icon: 'el-icon-location',
        child: [
          {
            deep: 2,
            index: '3',
            title: '设备管理',
            icon: 'el-icon-location',
            name: types.DEVICE_MANAGE
          },
          {
            index: '3-2',
            title: '设备分组',
            icon: 'el-icon-location',
            name: types.DEVICE_GROUP
          },
          {
            index: '3-3',
            title: '分配记录',
            icon: 'el-icon-location',
            name: types.DEVICE_DISTRIBUTION
          },
          {
            index: '3-4',
            title: '微信设置',
            icon: 'el-icon-location',
            name: types.WECHAT_SETTING
          },
          {
            index: '3-5',
            title: '快捷回复',
            icon: 'el-icon-location',
            name: types.QUICK_REPLY
          },
          {
            index: '3-6',
            title: '下属快捷回复',
            icon: 'el-icon-location',
            name: types.SUBORDINATES_REPLY
          },
          {
            index: '3-7',
            title: '自动通过好友',
            icon: 'el-icon-location',
            name: types.AUTO_FRIENDS
          },
          {
            index: '3-8',
            title: '全局自动回复',
            icon: 'el-icon-location',
            name: types.GLOBAL_REPLY
          },
          {
            index: '3-9',
            title: '个人号自动回复',
            icon: 'el-icon-location',
            name: types.PERSONAL_NUM_AUTO_REPLY
          },
          {
            index: '3-10',
            title: '素材管理',
            icon: 'el-icon-location',
            name: types.MATERIAL_MANAGE
          }
        ]
      }
    ]
  }
]
export default params
