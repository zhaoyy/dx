export default {
  title: {
    text: '好友地区统计 共10(位)',
    left: 'center',
    textStyle: {
      color: '#2897FF'
    }
  },
  tooltip: {
    trigger: 'item',
    showDelay: 0,
    transitionDuration: 0.2
  },
  visualMap: {
    min: 0,
    max: 10000,
    itemWidth: 10,
    inRange: {
      color: ['#cbeaff', '#8dcffb', '#66c1ff', '#8accff', '#0089e7']
    },
    text: ['高', '低'],
    calculable: true
  },
  // toolbox: {
  //   show: true,
  //   left: 'right',
  //   top: 'top',
  //   feature: {
  //     saveAsImage: {}
  //   }
  // },
  series: [{
    name: '中国地图',
    type: 'map',
    roam: false,
    map: 'china',
    top: 20,
    right: 20,
    bottom: 20,
    left: 20,
    label: {
      normal: {
        show: true,
        color: '#454545'
      },
      emphasis: {
        show: true
      }
    },
    itemStyle: {
      normal: {
        borderWidth: 1,
        borderColor: '#fff'
      },
      emphasis: {
        borderWidth: 2,
        borderColor: '#fff',
        areaColor: '#fed183'
      }
    }
  }]
}
