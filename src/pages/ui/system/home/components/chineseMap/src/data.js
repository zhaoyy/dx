import provinces from './provinces'

export default provinces.map(province => {
  province.value = Math.round(Math.random() * 10000)
  return province
})
