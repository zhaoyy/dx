import Cards from './cards'
import ChineseMap from './chineseMap'
import FriendCountPie from './friendCountPie'
import SensitivePie from './sensitivePie'
import FansCount from './fansCount'
import FriendUpCount from './friendUpCount'
import FriendUpSort from './friendUpSort'
import FriendAllotCount from './friendAllotCount'
import ReceiveNumCount from './receiveNumCount'
import ServiceDialogCount from './serviceDialogCount'
import FriendNotpassCount from './friendNotpassCount'
import WxGroupCount from './wxGroupCount'
import FriendResTimeCount from './friendResTimeCount'
import WxGroupActiveCount from './wxGroupActiveCount'
import TelRecordCount from './telRecordCount'

export {
  Cards, // 卡片列表
  ChineseMap, // 中国地图
  FriendCountPie, // 好友统计饼图
  SensitivePie, // 敏感操作统计
  FansCount, // 进粉统计
  FriendUpCount, // 好友增长统计
  FriendUpSort, // 好友增长排行
  FriendAllotCount, // 好友分配统计
  ReceiveNumCount, // 接待人数统计
  ServiceDialogCount, // 客服对话统计
  FriendNotpassCount, // 好友未通过统计
  WxGroupCount, // 微信群统计
  FriendResTimeCount, // 好友响应时间统计
  WxGroupActiveCount, // 微信群活跃统计
  TelRecordCount // 通话录音统计
}
