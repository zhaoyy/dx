export default{
  title: {
    text: '收发消息（条）',
    subtext: '123',
    textStyle: {
      color: '#454545',
      fontSize: '14'
    },
    subtextStyle: {
      color: '#151515',
      fontSize: '22',
      lineHeight: '52'
    }
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'line',
      label: {
        backgroundColor: 'red'
      }
    }
  },
  legend: {
    icon: 'circle',
    right: 0,
    itemWidth: 8,
    itemHeight: 8,
    data: ['接收信息', '发送信息']
  },
  grid: {
    top: '28%',
    left: '0',
    right: 40,
    bottom: '3%',
    containLabel: true
    // width: '100%'
  },
  xAxis: [
    {
      type: 'category',
      boundaryGap: false,
      data: ['3/19', '3/20', '3/21', '3/22', '3/23', '3/24', '3/25'],
      axisLine: {
        lineStyle: {
          color: 'transparent'
        }
      },
      axisLabel: {
        textStyle: {
          color: '#151515' // 坐标值得具体的颜色
        }
      }
    }
  ],
  yAxis: [
    {
      type: 'value',
      scale: true,
      splitNumber: 200,
      max: 1000,
      minInterval: 200,
      splitLine: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: 'transparent'
        }
      },
      axisLabel: {
        textStyle: {
          color: '#151515' // 坐标值得具体的颜色
        }
      }
    }
  ],
  series: [
    {
      name: '接收信息',
      type: 'line',
      stack: '总量',
      areaStyle: {
        color: 'rgba(20, 147, 233, 0.3)'
      },
      itemStyle: {
        color: '#1493E9'
      },
      data: [120, 132, 101, 134, 700, 230, 210]
    },
    {
      name: '发送信息',
      type: 'line',
      stack: '总量',
      areaStyle: {
        color: 'rgba(20, 147, 233, 0.3)'
      },
      itemStyle: {
        color: '#44C89A'
      },
      data: [220, 182, 191, 500, 290, 330, 310]
    }
  ]
}
