require('echarts-wordcloud')
export default {
  series: [{
    type: 'wordCloud',
    gridSize: 20,
    sizeRange: [12, 50],
    rotationRange: [0, 0],
    shape: 'circle',
    textStyle: {
      normal: {
        color: function() {
          return 'rgb(' + [
            Math.round(Math.random() * 160),
            Math.round(Math.random() * 160),
            Math.round(Math.random() * 160)
          ].join(',') + ')'
        }
      },
      emphasis: {
        shadowBlur: 10,
        shadowColor: '#333'
      }
    },
    data: [{
      name: '贷款期限',
      value: 6181
    }, {
      name: '建筑材料',
      value: 4386
    }, {
      name: '大学生兼职',
      value: 4055
    }, {
      name: '太贵了',
      value: 4055
    }]
  }]
}
