export default {
  tooltip: {
    trigger: 'item',
    showDelay: 0,
    transitionDuration: 0.2
  },
  visualMap: {
    min: 0,
    // max: 10000,
    itemWidth: 10,
    itemHeight: 80,
    inRange: {
      color: ['#cbeaff', '#8dcffb', '#66c1ff', '#8accff', '#0089e7']
    },
    text: ['高', '低'],
    calculable: true
  },
  series: [{
    name: '中国地图',
    type: 'map',
    roam: false,
    map: 'china',
    top: '4%',
    left: '8%',
    right: '8%',
    bottom: '4%',
    zoom: 0.9,
    label: {
      normal: {
        show: true,
        color: '#454545',
        fontSize: 8
      },
      emphasis: {
        show: true
      }
    },
    itemStyle: {
      normal: {
        borderWidth: 1,
        borderColor: '#fff'
      },
      emphasis: {
        borderWidth: 2,
        borderColor: '#fff',
        areaColor: '#fed183'
      }
    }
  }]
}
