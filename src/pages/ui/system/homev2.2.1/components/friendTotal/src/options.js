export default {
  title: {
    text: '好友人数（人）',
    subtext: '123',
    textStyle: {
      color: '#454545',
      fontSize: '14'
    },
    subtextStyle: {
      color: '#151515',
      fontSize: '22',
      lineHeight: '52'
    }
  },
  color: ['#2897FF'],
  tooltip: {
    trigger: 'axis',
    axisPointer: { // 坐标轴指示器，坐标轴触发有效
      type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
    }
  },
  grid: {
    top: '28%',
    left: '0',
    right: '0',
    bottom: '3%',
    containLabel: true
  },
  xAxis: [
    {
      type: 'category',
      data: ['3/19', '3/20', '3/21', '3/22', '3/23', '3/24', '3/25'],
      axisTick: {
        alignWithLabel: true
      },
      axisLine: {
        lineStyle: {
          color: '#ddd'
        }
      },
      axisLabel: {
        textStyle: {
          color: '#151515' // 坐标值得具体的颜色
        }
      }
    }
  ],
  yAxis: [
    {
      type: 'value',
      scale: true,
      splitNumber: 200,
      max: 1000,
      minInterval: 200,
      splitLine: {
        lineStyle: {
          color: '#ddd'
        }
      },
      axisLine: {
        lineStyle: {
          color: 'transparent'
        }
      },
      axisLabel: {
        textStyle: {
          color: '#151515' // 坐标值得具体的颜色
        }
      }
    }
  ],
  series: [
    {
      name: '好友人数（人）',
      type: 'bar',
      barWidth: '60%',
      data: [10, 52, 200, 334, 390, 330, 220]
    }
  ]
}
