import gActivityAdd from './gActivityAdd'
import gActivityAddCode from './gActivityAddCode'
import gActivityShowCode from './gActivityShowCode'
import gActivitySetGroupNum from './gActivitySetGroupNum'
import gActivityGroupList from './gActivityGroupList'
import gActivityGroupListAddGroup from './gActivityGroupListAddGroup'
import gActivityGroupListAddCode from './gActivityGroupListAddCode'
import gActivityGroupListHandle from './gActivityGroupListHandle'
import gActivityGroupListHandleAddNum from './gActivityGroupListHandleAddNum'
import gActivityGroupListHandleDetail from './gActivityGroupListHandleDetail'
import gActivityGroupListMembers from './gActivityGroupListMembers'
import gActivityGroupListMembersRecords from './gActivityGroupListMembersRecords'
import Statistics from './Statistics'
import LineChart from './LineChart'
export {
  gActivityAdd, // 引流 => 群活动--新建
  gActivityAddCode, // 引流 => 群活动--添加二维码
  gActivityShowCode, // 引流 => 群活动--展示二维码
  gActivitySetGroupNum, // 引流 => 群活动--人数设置
  gActivityGroupList, // 引流 => 群活动--群列表
  gActivityGroupListAddGroup, // 引流 => 群活动--群列表--添加群
  gActivityGroupListAddCode, // 引流 => 群活动--群列表--二维码名片
  gActivityGroupListHandle, // 引流 => 群活动--群列表--群拉手
  gActivityGroupListHandleAddNum, // 引流 => 群活动--群列表--群拉手--选择个人号
  gActivityGroupListHandleDetail, // 引流 => 群活动--群列表--群拉手--拉手详情
  gActivityGroupListMembers, // 引流 => 群活动--群列表--群成员管理
  gActivityGroupListMembersRecords, // 引流 => 群活动--群列表--群成员管理--列表
  Statistics, // 引流 => 群活动--统计
  LineChart // 引流 => 群活动--统计--echats图
}
