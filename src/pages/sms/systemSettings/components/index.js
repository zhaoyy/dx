import pManageAddOrEdit from './pManageAddOrEdit'
import dConfigAddDomain from './dConfigAddDomain'

export {
  pManageAddOrEdit, // 套餐管理 => 新增/编辑
  dConfigAddDomain // 域名配置 => 新增/编辑
}
