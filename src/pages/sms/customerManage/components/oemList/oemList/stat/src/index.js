import basicInfo from '../components/basicInfo'
import agentsList from '../components/agentsList'
import enterpriseList from '../components/enterpriseList'
import equipmentDetails from '../components/equipmentDetails'
import authorizedShop from '../components/authorizedShop'
import personalNum from '../components/personalNum'
import notificationList from '../components/notificationList'

export {
  basicInfo,
  agentsList,
  enterpriseList,
  equipmentDetails,
  authorizedShop,
  personalNum,
  notificationList
}
