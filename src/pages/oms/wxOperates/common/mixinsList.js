import {
    wechatApi
} from '@/api/oms'

const mixinsList = {
    data() {
        return {
            publicParams: {
                current: 1,
                size: 100,
            },
            searchParams: {
                current: 1,
                size: 100,
                day: -1
            },
            excelName: 'eq'
        }
    },
    created() {
        // this.getTableList(this.publicParams)
    },
    methods: {
        getSummaries(param) {
            const {
                columns,
                data
            } = param
            const sums = []

            columns.forEach((column, index) => {
                if (index === 0) {
                    sums[index] = '总价';
                    return;
                }
                const values = data.map(item => Number(item[column.property]));
                if (!values.every(value => isNaN(value))) {
                    sums[index] = values.reduce((prev, curr) => {
                        const value = Number(curr);
                        if (!isNaN(value)) {
                            return prev + curr;
                        } else {
                            return prev;
                        }
                    }, 0);
                    sums[index] += ' ';
                } else {
                    sums[index] = 'N/A';
                }
            });
            sums[sums.length - 1] = this.params.annualTotal
            sums[1] = ' '
            sums[2] = ' '
            sums[3] = ' '
            sums[5] = ' '
            sums[4] = ' '
            return sums;
        },
        getSummariesTwo(param) {
            const {
                columns,
                data
            } = param
            const sums = []

            columns.forEach((column, index) => {
                if (index === 0) {
                    sums[index] = '总价';
                    return;
                }
                const values = data.map(item => Number(item[column.property]));
                if (!values.every(value => isNaN(value))) {
                    sums[index] = values.reduce((prev, curr) => {
                        const value = Number(curr);
                        if (!isNaN(value)) {
                            return prev + curr;
                        } else {
                            return prev;
                        }
                    }, 0);
                    sums[index] += ' ';
                } else {
                    sums[index] = 'N/A';
                }
            });
            sums[sums.length - 1] = this.paramsTwo.annualTotal
            sums[1] = ' '
            sums[2] = ' '
            sums[3] = ' '
            sums[5] = ' '
            sums[4] = ' '
            return sums;
        },
        // 总计金额自定义
        getEquipmentSums(param) {
            const {
                columns,
                data
            } = param
            const sums = []

            columns.forEach((column, index) => {
                if (index === 0) {
                    sums[index] = '2020年总计';
                    return;
                }
                const values = data.map(item => Number(item[column.property]));
                if (!values.every(value => isNaN(value))) {
                    sums[index] = values.reduce((prev, curr) => {
                        const value = Number(curr);
                        if (!isNaN(value)) {
                            return prev + curr;
                        } else {
                            return prev;
                        }
                    }, 0);
                    sums[index] += ' ';
                } else {
                    sums[index] = 'N/A';
                }
            });
            sums[sums.length - 1] = this.params.annualTotal
            sums[1] = ' '
            sums[2] = ' '
            sums[3] = ' '
            sums[5] = ' '
            sums[4] = ' '
            return sums;
        }
    },
}

export default mixinsList