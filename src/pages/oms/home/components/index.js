import Cards from './cards'
import ChineseMap from './chineseMap'
import FriendCountPie from './friendCountPie'
import FriendTotal from './friendTotal'
import SendRecieveLine from './sendRecieveLine'
import FollowQuestion from './followQuestion'

export {
  Cards, // 卡片列表
  ChineseMap, // 中国地图
  FriendCountPie, // 好友性别统计饼图
  FriendTotal, // 好友人数（柱形图）
  SendRecieveLine, // 收发信息
  FollowQuestion // 用户关注问题（词云图）
}
