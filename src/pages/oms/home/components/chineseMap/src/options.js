export default {
  tooltip: {       // 提示框组件可以设置在多种地方 
    trigger: 'item', // 触发类型 数据项图形触发，主要在散点图，饼图等无类目轴的图表中使用。
    showDelay: 0,  //浮层显示的延迟
    transitionDuration: 0.2 //提示框浮层的移动动画过渡时间，单位是 s，设置为 0 的时候会紧跟着鼠标移动。
  },
  visualMap: { //是视觉映射组件，用于进行『视觉编码』，也就是将数据映射到视觉元素（视觉通道）
    min: 0,
    max: 60000,
    itemWidth: 10, // 图形的宽度，即每个小块的宽度。
    itemHeight: 180, // 图形的高度，即每个小块的高度。
    inRange: {  // 定义 在选中范围中 的视觉元素。
      // color: ['#cbeaff', '#8dcffb', '#66c1ff', '#8accff', '#0089e7']
      
      color: ['#01d4e7', '#7c56c1', '#59aac8', '#16b18b', '#fbd062', '#eeb311', '#f5791e', '#d92c28']

    },
    text: ['高', '低'],
    calculable: true // 是否显示拖拽用的手柄
  },
  series: [{
    name: '中国地图',
    type: 'map',
    roam: false,
    map: 'china',
    top: '4%',
    left: '8%',
    right: '8%',
    bottom: '4%',
    zoom: 0.9,
    label: {
      normal: {
        show: false,
        color: '#454545',
        fontSize: 8
      },
      emphasis: {
        show: true
      }
    },
    itemStyle: {
      normal: {
        borderWidth: 1,
        borderColor: '#fff'
      },
      emphasis: {
        borderWidth: 2,
        borderColor: '#fff',
        areaColor: '#fed183'
      }
    }
  }]
}
