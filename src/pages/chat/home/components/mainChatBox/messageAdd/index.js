import messageOfText from './components/messageOfText'
import messageOfAudio from './components/messageOfAudio'
import messageOfVideo from './components/messageOfVideo'
import messageOfImgVido from './components/messageOfImgVido'
import messageOfTxtImgAudVido from './components/messageOfTxtImgAudVido'
import pmessageOfTxtImgAudVido from './components/pmessageOfTxtImgAudVido'
import messageOfWeChat from './components/messageOfWeChat'
import messageOfImgTxtLink from './components/messageOfImgTxtLink'
import chatMsgmessageOfTxtImgAudVido from './components/chatMsgmessageOfTxtImgAudVido'

export {
  messageOfText, // 消息类型 => 文本
  messageOfAudio, // 消息类型 => 音频
  messageOfVideo, // 消息类型 => 视频
  messageOfImgVido, // 消息类型 => 图片、视频（定时发送朋友圈使用）
  messageOfTxtImgAudVido, // 消息类型 => 文本、音频、图片、视频、小程序
  messageOfWeChat, // 消息类型 => 小程序
  messageOfImgTxtLink, // 消息类型 => 图文链接,
  pmessageOfTxtImgAudVido, // 个人库消息类型 => 文本、音频、图片、视频、小程序
  chatMsgmessageOfTxtImgAudVido
}
