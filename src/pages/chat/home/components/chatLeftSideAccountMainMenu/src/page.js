class Page {
  constructor({
    params = {},
    totalTableList = [],
    list,
    afterList
  } = {}) {
    this.totalTableList = list || []
    this.afterList = afterList || this.afterList
    let leng
    if (typeof totalTableList === 'function') {
      const tableList = totalTableList()
      this.setTotalTableList(tableList)
      leng = tableList.length
    } else {
      this.setTotalTableList(totalTableList)
      leng = totalTableList.length
    }
    this.params = Object.assign({}, this.params, params)
    this.params.total = leng
    // 判断是否有数据库，有则打开，没有就建立
  }

  params = {
    current: 1,
    size: 20,
    total: 0
  }

  // 好友的各个分页总数据
  totalTableList = []

  // tableData = []

  // 好友信息map
  // dataMap = {}

  // [Function] 刷新一条
  refreshOneItem = null

  // [Function] 新增一条
  newOneItem = null

  // [Function] 删除一条
  delOneItem = null

  search() {
    this.clearSelection()
    this.list(1)
  }

  // [Function] 获取分页数据
  async list(current, size) {
    let params = this.getParams()
    if (this.beforeList(params, current, size)) return
    let page = this.getList(params)
    page.records = await this.afterList(page.records)
    // this.setTableList(page.records)
    this.setPagination(page)
    return page
  }

  // 查询前执行,可更改查看参数
  beforeList(params, current, size) {
    if (current) {
      params.current = current
    }
    if (size) {
      params.size = size
    }
  }

  getList(params) {
    const start = (params.current - 1) * params.size
    const records = (this.getTotalTableList(params) || []).slice(start, start + params.size)
    return {
      records,
      current: params.current,
      size: params.size,
      total: this.getTotalTableList(params).length
    }
  }

  // [Function] 查询成功后执行，可更改list
  async afterList(list) {
    // 遍历列表数据收集id
    // 获取对应好友信息,并存储
    // this.setInfoData(accountId, data)
    // 再遍历列表数据进行数据填充
    return list
  }

  // 保存列表数据
  setTotalTableList(tableList = []) {
    this.totalTableList = tableList
    this.params.total = tableList.length
  }

  getTotalTableList(params) {
    return this.totalTableList
  }

  getParams() {
    return {
      current: this.params.current,
      size: this.params.size
    }
  }

  // setTableList(list) {
  //   this.tableData = list
  // }

  setPagination(page) {
    this.params = Object.assign({}, this.params, {
      current: page.current
    })
    this.params.current = page.current
    this.params.total = page.total
  }

  // 保存用户信息
  setInfoData(accountId, data) {}

  // 获取用户信息
  getInfoData(accountId, data) {}

  // 清空数据
  clear() {
    this.totalTableList = []
    // this.tableData = []
  }
}

export default Page
