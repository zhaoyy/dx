import {
  wechatInfoSession
} from '@/storage'
import {
  COMMAND_TYPE,
  MESSAGE_SUB_TYPE
} from '@/constants/chat'
import mappingMoment from './mappingMoment'

// 存储当前消息实例
// {
//   msgType: {
//     commandType: []
//   }
// }
const MSG_MAP = {}

export default class WxMsg {
  constructor({
    msgType = WxMsg.MSG_TYPE.MOMENT,
    commandType,
    chatIM,
    onReceiveMsg
  }) {
    this.msgType = msgType
    this.commandType = commandType
    this.chatIM = chatIM
    this.onReceiveMsg = onReceiveMsg

    WxMsg.add(msgType, commandType, this)
  }

  static MSG_TYPE = {
    // 朋友圈
    MOMENT: MESSAGE_SUB_TYPE.VMA_MOMENTS_ELEM
  }

  static COMMAND_TYPE = {
    // 朋友圈
    MOMENT: {
      // 获取朋友圈列表
      CIRCLE: COMMAND_TYPE.CIRCLE,
      // 根据朋友圈id取朋友圈
      CIRCLE_GET: COMMAND_TYPE.CIRCLE_GET,
      // 朋友圈评论
      CIRCLE_COMMENT: COMMAND_TYPE.CIRCLE_COMMENT,
      // 朋友圈回复
      CIRCLE_REPLY: COMMAND_TYPE.CIRCLE_REPLY,
      // 评论
      CIRCLE_LIKE: COMMAND_TYPE.CIRCLE_LIKE
    }
  }

  // [String -> WxMsg.MSG_TYPE.XX] 消息类型
  msgType = null

  // [String -> WxMsg.COMMAND_TYPE.XX] 指令类型
  commandType = null

  // [*] IM
  chatIM = null

  // [Function] 收到消息的回调
  onReceiveMsg = null

  // [Object] 最后一次返回的消息体
  msgData = null

  // [Object] 最后一次发送的参数
  params = null

  // 发送消息
  static send(chatIM, options) {
    if (typeof chatIM === 'function') {
      chatIM = chatIM()
    }
    return chatIM.sendMessge(options)
  }

  // 添加消息监听
  static add(msgType, commandType, wxMsg) {
    let msgTypeMap = MSG_MAP[msgType] || (MSG_MAP[msgType] = {})
    let commandTypeList = msgTypeMap[commandType] || (msgTypeMap[commandType] = [])
    commandTypeList.push(wxMsg)
  }

  // 移除消息监听
  static remove(msgType, commandType, wxMsg) {
    if (MSG_MAP[msgType] && MSG_MAP[msgType][commandType]) {
      if (wxMsg) {
        let commandTypeList = MSG_MAP[msgType][commandType]
        if (commandTypeList.includes(wxMsg)) {
          commandTypeList.splice(commandTypeList.indexOf(wxMsg), 1)
        }
      } else {
        MSG_MAP[msgType][commandType] = null
      }
    }
  }

  // 收到消息后分发
  static receiveMsg(msgData, fromImAccount) {
    const {
      msgType,
      commandType
    } = msgData
    if (MSG_MAP[msgType] && MSG_MAP[msgType][commandType]) {
      let commandTypeList = MSG_MAP[msgType][commandType]
      commandTypeList.forEach(wxMsg => {
        wxMsg.receiveMsg(msgData, fromImAccount)
      })
    }
  }

  // 转换参数
  static transferoptions(options, msgData) {
    options = Object.assign({
      msgSubType: 'custom',
      sessionType: 'c2c',
      identifier: wechatInfoSession.getJSON().identifier,
      // selToID必须由调用方提供
      selToID: '',
      msgData: {}
    }, options)
    options.msgData = Object.assign({
      commandId: Date.now()
    }, msgData, options.msgData)
    return options
  }

  // 执行
  exec(params) {
    if (typeof params === 'function') {
      params = params()
    }
    this.params = params = WxMsg.transferoptions(params, {
      msgType: this.msgType,
      commandType: this.commandType
    })
    WxMsg.send(this.chatIM, params)
  }

  // 收到消息
  receiveMsg(msgData, fromImAccount) {
    if (typeof msgData.data === 'object') {
      // 朋友圈列表/朋友圈单条信息需要解码
      switch (msgData.commandType) {
        case COMMAND_TYPE.CIRCLE:
        case COMMAND_TYPE.CIRCLE_GET:
          msgData.data = mappingMoment(msgData.data)
          break
      }
    }
    this.msgData = msgData
    if (typeof this.onReceiveMsg === 'function') {
      this.onReceiveMsg(msgData, fromImAccount)
    }
  }

  // 清空数据
  clear() {
    this.msgData = null
  }

  // 销毁
  destroy() {
    WxMsg.remove(this.msgType, this.commandType, this)
  }
}
