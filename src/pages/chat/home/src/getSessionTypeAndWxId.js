import {
  SESSION_TYPE
} from '@/constants/chat'

/**
 * 获取聊天消息/当前会话，是群聊/好友聊天
 * @param  {[type]} talkerRoomWxId 群聊id
 * @param  {[type]} talkerWxId     好友id
 * @return {[type]}                [description]
 */
export default (talkerRoomWxId, talkerWxId) => {
  let type, wxId
  if (talkerRoomWxId) {
    // 群聊
    type = SESSION_TYPE.GROUP
    wxId = talkerRoomWxId
  } else {
    // 好友单聊
    type = SESSION_TYPE.C2C
    wxId = talkerWxId
  }
  return {
    type,
    wxId
  }
}
