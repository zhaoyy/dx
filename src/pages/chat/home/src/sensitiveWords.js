import { customerServiceApi } from '@/api/chat'

class SensitiveWords {
    constructor({
        getApi,
        afterList
    } = {}) {
        this.getApi = getApi || this.getApi
        this.afterList = afterList || this.afterList
    }

    sensitiveWordsList = {
        errors: [],
        warnings: [],
        reserachs: [],
        seatReserach: [],
    }

    /**
     * 初始化铭感词列表
     * @return {[type]} [description]
     */
    initDataList(params) {
        return this.list(params)
    }

    /**
     * 提供api
     * @return {[type]} [description]
     */
    getApi() {
        return {
            list: customerServiceApi.searchSensitiveWords,
            save: customerServiceApi.saveSensitiveRecord,
            resave: customerServiceApi.saveCustomerRecord
        }
    }

    /**
     * 获取列表数据之后的处理
     * @return {[type]} [description]
     */
    afterList(list) {
        let errors = []
        let warnings = []
        let reserachs = []
        let seatReserach = []

        list.forEach(val => {
            if (val.type === 1) {
                errors.push(val)
            } else if (val.type === 2) {
                warnings.push(val)
            } else if (val.type === 3) {
                reserachs.push(val)
            } else if (val.type === 4) {
                seatReserach.push(val)
            }
        })
        return {
            errors,
            warnings,
            reserachs,
            seatReserach
        }
    }

    /**
     * 查询敏感词列表数据
     * @param  {Object} params [description]
     * @return {[type]}        [description]
     */
    async list(params = {}) {
        let list = await this.getApi().list(params)
        list = this.afterList(list) || []
        this.setDataList(list)
        return list
    }

    /**
     * 获取保存的敏感词列表
     * @return {[type]} [description]
     */
    getDataList() {
        return this.sensitiveWordsList
    }

    /**
     * 保存敏感词列表
     * @return {[type]} [description]
     */
    setDataList(list) {
        this.sensitiveWordsList = list
    }

    /**
     * 检索是否有敏感词
     */
    checkSensitiveWords(content) {
        let reg
        let warning, error, seatWord
            // 校验阻止的
        if (this.sensitiveWordsList.errors.length > 0) {
            this.sensitiveWordsList.errors.some(val => {
                reg = new RegExp(val.keyWord, 'i')
                if (content.match(reg)) {
                    error = val
                    reg = null
                    return true
                }
                reg = null
            })
        }
        if (error) {
            return error
        }
        // 没有阻止的话，继续查找是否警告
        this.sensitiveWordsList.warnings.some(val => {
            reg = new RegExp(val.keyWord, 'i')
            if (content.match(reg)) {
                warning = val
                reg = null
                return true
            }
            reg = null
        })
        if (warning) {
            return warning
        }
        //  检索是否有坐席体验词

        this.sensitiveWordsList.seatReserach.some(val => {
            reg = new RegExp(val.keyWord, 'i')
            if (content.match(reg)) {
                seatWord = val
                reg = null
                return true
            }
            reg = null
        })
        if (seatWord) {
            return seatWord
        }
    }

    /**
     * 检索是否有客户体验词
     */
    checkCustomerWords(content) {
        let reg
        let reserach
            // 查找调研词
        this.sensitiveWordsList.reserachs.some(val => {
            reg = new RegExp(val.keyWord, 'i')
            if (content.match(reg)) {
                reserach = val
                reg = null
                return true
            }
            reg = null
        })
        if (reserach) {
            return reserach
        }
    }


    /**
     * 保存敏感词记录
     * @param  {[type]} params [description]
     */
    saveRecord(params) {
        return this.getApi().save(params)
    }

    /**
     * 保存调研词记录
     * @param  {[type]} params [description]
     */
    saveCustomer(params) {
        return this.getApi().resave(params)
    }
}

export default SensitiveWords