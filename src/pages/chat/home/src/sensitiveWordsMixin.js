import SensitiveWords from '@/pages/chat/home/src/sensitiveWords'
import Logger from '@/utils/logger'
import { mapMutations, mapGetters } from 'vuex'
import * as mutationsType from '@/store/mutation-types.js'
import { log } from 'util'

export default {
  data() {
    return {}
  },
  computed: {
    ...mapGetters([
      'chatSensitiveWords'
    ])
  },
  methods: {
    ...mapMutations({
      updateChatSensitiveWords: mutationsType.CHAT_SENSITIVE_WORDS
    }),
    /**
     * 初始化敏感词列表
     */
    async initSensitiveWords() {
      const sensitiveWords = new SensitiveWords()
      this.updateChatSensitiveWords(sensitiveWords)
      try {
        await this.chatSensitiveWords.initDataList()
      } catch (error) {
        Logger.error('敏感词获取失败:', error)
        await this.$confirm('敏感词列表获取失败，是否重新获取?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        })
        this.refreshSensitiveWords()
      }
    },
    /**
     * 更新敏感词列表
     */
    async refreshSensitiveWords() {
      try {
        this.chatSensitiveWords.list()
      } catch (error) {
        await this.$confirm('敏感词列表获取失败，是否重新获取?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        })
        this.refreshSensitiveWords()
      }
    },
    /**
     * 检查敏感词
     * @return {[type]} 输出为true是没有敏感词，数组为警告
     */
    checkUpSensitiveWords(text, imAccount, fType, fWxId) {
      // 当this.chatSensitiveWords 直接不检查敏感词
      if (JSON.stringify(this.chatSensitiveWords) === '{}') {
        return 
      }
      let warningData = this.chatSensitiveWords.checkSensitiveWords(text)
      if (warningData && warningData.type > 0) {
        // 保存记录，在阻止的情况
        if (imAccount && warningData.type === 1 || warningData.type === 4) {
          let params = {
            content: text,
            createTime: new Date().getTime(),
            imei: imAccount,
            objType: fType,
            objWxId: fWxId,
            riskId: warningData.id
          }
          this.chatSensitiveWords.saveRecord(params)
        }
        if (warningData.type === 1) {
          // 阻止
          this.error('您发送的消息中含有敏感内容,请修改之后再进行发送')
          return {
            type: 'error',
            info: warningData
          }
        } else if (warningData.type === 4) {

        } else {
          this.warn('您发送的消息中含有敏感内容')
          return {
            type: 'warning',
            info: warningData
          }
        }
      }
    },

    /**
     * 检查客户调研词
     * @return {[type]} 输出为true是没有调研词，数组为警告
     */
    checkCustomerds(text, imAccount, fType, fWxId) {
      let warningData = this.chatSensitiveWords.checkCustomerWords(text)
      if (warningData && warningData.type > 0) {
        // 保存记录，在阻止的情况
        if (imAccount && warningData.type === 3) {
          let params = {
            content: text,
            createTime: new Date().getTime(),
            imei: imAccount,
            objType: fType,
            objWxId: fWxId,
            riskId: warningData.id
          }
          this.chatSensitiveWords.saveCustomer(params)
        }
      }
    }

  }
}
