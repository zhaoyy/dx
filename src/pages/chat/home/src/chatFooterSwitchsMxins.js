import {
  mapGetters
} from 'vuex'

export default {
  computed: {
    ...mapGetters(['chatFooterSwitchs'])
  },
  methods: {
    /**
     * 获取[chatFooterSwitchs]的状态
     *
     * @param {*} chatFooterSwitchs
     * @returns
     */
    getChatFooterSwitchsStatus(footerSwitchsCode) {
      return this.chatFooterSwitchs[footerSwitchsCode]
    }
  }
}
