// indexedDB.js，浏览器本地数据库操作

export default {
  // indexedDB兼容
  indexedDB: window.indexedDB || window.webkitindexedDB || window.msIndexedDB || window.mozIndexedDB,
  IDBTransaction: window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction,
  IDBKeyRange: window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange,

  // 打开数据库
  // 新对象储存空间newStore参数：newStore.name、newStore.key
  // 新增对象存储空间要更改数据库版本
  openDB: function (dbname, version, onsuccess, onerror, onupgradeneeded) {
    if (!window.indexedDB) {
      window.alert('你的浏览器不支持IndexDB,请更换浏览器')
    }
    version = version || 1
    let request = this.indexedDB.open(dbname, version)
    request.onerror = function (event) { // 打开数据库失败
      alert('不能打开数据库,错误代码: ' + event.target.errorCode)
      // console.error('IndexedDB数据库打开错误')
      if (onerror && (typeof onerror === 'function')) {
        onerror(event)
      }
    }
    request.onsuccess = function (event) {
      // console.log('DB-onsuccess')
      const db = event.target.result
      if (onsuccess && (typeof onsuccess === 'function')) {
        onsuccess(db, event, version)
      }
    }
    // onupgradeneeded，调用创建新的储存空间
    request.onupgradeneeded = function (event) {
      // console.log('DB-onupgradeneeded')
      const db = event.target.result
      if (onupgradeneeded && (typeof onupgradeneeded === 'function')) {
        onupgradeneeded(db, event, db.version)
      }
    }
    return request
  },
  // 删除数据库
  deleteDB: function (dbname) {
    return new Promise((resolve, reject) => {
      let deleteQuest = this.indexedDB.deleteDatabase(dbname)
      deleteQuest.onerror = function (event) {
        // console.error('删除数据库出错')
        reject(event)
      }
      deleteQuest.onsuccess = function (event) {
        resolve(event)
      }
    })
  },
  // 关闭数据库
  closeDB: function (db) {
    db.close()
    // console.log('数据库已关闭')
  },
  // 新建数据库表
  createStore (db, newStore, indexs) {
    // console.log('表/仓库', db.objectStoreNames.contains(newStore.name))
    if (newStore) {
      if (!db.objectStoreNames.contains(newStore.name)) {
        const config = newStore.key ? {
          keyPath: newStore.key
        } : {
          autoIncrement: true
        }
        let store = db.createObjectStore(newStore.name, config)
        if (indexs && indexs.length > 0) {
          indexs.some(v => {
            this.createStoreIndex(Object.assign({}, v, {store}))
          })
        }
        return store
      }
    }
  },
  // 清空数据库表
  clearStore ({ store, db, storeName } = {}) {
    const storeDel = store || db.transaction([storeName])
    storeDel.clear()
  },
  // 删除数据库表
  deleteStore (db, storeName) {
    db.deleteObjectStore(storeName)
  },
  // 建立数据库表的索引
  createStoreIndex ({db, storeName, store, indexName, keyPath, unique = false}) {
    store = store || db.transaction([storeName], 'readwrite')
    store.createIndex(indexName, keyPath, { unique })
  },
  // 添加数据，add添加新值
  addData: function (db, storeName, data) {
    return new Promise((resolve, reject) => {
      let store = db.transaction([storeName], 'readwrite').objectStore(storeName)
      let request = store.add(data)
      request.onerror = function (event) {
        // console.error('ADD添加数据报错')
        reject(event)
      }
      request.onsuccess = function (event) {
        resolve(event)
      }
    })
  },
  // 更新旧值
  putData: function (db, storeName, data, key) {
    return new Promise((resolve, reject) => {
      let store = db.transaction([storeName], 'readwrite').objectStore(storeName)
      let request = store.put(data, key)
      request.onerror = function (event) {
        // console.error('PUT添加数据报错', event)
        reject(event)
      }
      request.onsuccess = function (event) {
        resolve(event)
      }
    })
  },
  // 删除数据
  deleteData: function (db, storeName, key) {
    let store = db.transaction([storeName], 'readwrite').objectStore(storeName)
    store.delete(key)
  },
  // 清空数据
  clearData: function (db, storeName) {
    let store = db.transaction([storeName], 'readwrite').objectStore(storeName)
    store.clear()
  },
  // 通过key获取数据
  getData: function (db, storeName, indexQuery) {
    return new Promise((resolve, reject) => {
      // 待改进
      let store = db.transaction([storeName], 'readonly').objectStore(storeName)
      let request
      if (indexQuery.all) {
        request = store.getAll(indexQuery.key, indexQuery.limit || 0)
      } else {
        request = store.get(indexQuery.key)
      }
      request.onerror = function (event) {
        // console.error('通过KEY获取数据报错')
        reject(event)
      }
      request.onsuccess = function (event) {
        let result = event.target.result
        resolve(result)
      }
    })
  },
  // 通过索引搜索
  getDataByIndex: async function (db, storeName, indexQuery) {
    return new Promise((resolve, reject) => {
      let store = db.transaction([storeName], 'readonly').objectStore(storeName)
      let index = store.index(indexQuery.key)
      let request
      // console.log('我在等', index, indexQuery)
      try {
        if (indexQuery.all) {
          request = index.getAll(indexQuery.value, indexQuery.limit || 0)
        } else {
          request = index.get(indexQuery.value)
        }
        request.onerror = function (event) {
          console.error('通过index获取数据报错')
          const res = event.target.result
          reject(res)
        }
        request.onsuccess = function (event) {
          const res = event.target.result
          resolve(res)
        }
      } catch (error) {
        resolve([])
        // alert(error.message)
      }
    }).then(function (response) {
      // console.log('response', response)
    }).catch((error) => {
      console.error(error)
    })
  },
  // 通过索引搜索主键
  getKeyByIndex: async function (db, storeName, indexQuery) {
    return new Promise((resolve, reject) => {
      let store = db.transaction([storeName], 'readonly').objectStore(storeName)
      let index = store.index(indexQuery.key)
      let request
      try {
        if (indexQuery.all) {
          request = index.getAllKeys(indexQuery.value)
        } else {
          request = index.getKey(indexQuery.value)
        }
        request.onerror = function (event) {
          // console.error('通过index.getKey获取数据报错')
          reject(event)
        }
        request.onsuccess = function (event) {
          let result = event.target.result
          resolve(result)
        }
      } catch (error) {
        resolve([])
      }
    })
  }
  // 通过游标操作数据, callback中要有游标移动方式
  // handleDataByCursor: function (db, storeName, keyRange) {
  //   return new Promise((resolve, reject) => {
  //     keyRange = keyRange || ''
  //     let store = db.transaction([storeName], 'readwrite').objectStore(storeName)
  //     let request = store.openCursor(keyRange)
  //     request.onerror = function (event) {
  //       console.error('通过游标获取数据报错')
  //       reject(event)
  //     }
  //     request.onsuccess = function (event) {
  //       let cursor = event.target.result
  //       // 操作cursor的方法有cursor.update(value)、cursor.delete()
  //       if (cursor) {
  //         resolve(cursor)
  //         // 移动游标方式
  //         // cursor.continue()
  //       }
  //     }
  //   })
  // },
  // 通过索引游标操作数据, callback中要有游标移动方式
  // handleDataByIndexCursor: function (db, storeName, cursorIndex, onSuccess, onError, keyRange) {
  //   keyRange = keyRange || ''
  //   let store = db.transaction([storeName], 'readwrite').objectStore(storeName)
  //   let request = store.index(cursorIndex).openCursor(keyRange)
  //   request.onerror = function (event) {
  //     console.error('通过索引游标获取数据报错')
  //     if (onError && (typeof onError === 'function')) {
  //       onError(event)
  //     }
  //   }
  //   request.onsuccess = function (event) {
  //     let cursor = event.target.result
  //     if (cursor) {
  //       // 将查询结果传入回调函数
  //       if (onSuccess && (typeof onSuccess === 'function')) {
  //         // 将查询结果传入回调函数
  //         onSuccess(cursor, () => cursor.continue())
  //       }
  //       // 移动游标方式
  //       // cursor.continue()
  //     }
  //   }
  // }
  // // 创建游标索引
  // createCursorIndex: function (db, storeName, cursorIndex) {
  //   let store = db.transaction([storeName], 'readwrite').objectStore(storeName)
  //   store.createIndex(cursorIndex, cursorIndex, {
  //     unique: false
  //   })
  // }
}
