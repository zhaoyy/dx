import { wechatUserDBInfoLocal } from '@/storage'
import {
  appNamespace
} from '@/config'

const name = `${appNamespace}/chatUserInfo`

const version = 1

// 微客服的客户信息
export const chatUserInfo = function(firmId) {
  let config = wechatUserDBInfoLocal.getJSON()
  // 默认值
  if (Object.keys(config).length === 0) {
    config = {
      name,
      version
    }
    wechatUserDBInfoLocal.setJSON(config)
  }
  return config
}
