import idb from '../indexedDB'
import * as config from '../config'
import Logger from '@/utils/logger'
import { wechatUserDBInfoLocal } from '@/storage'

class UserInfoDB {
  constructor({
    firmId,
    storeConfig,
    onSuccess,
    onError,
    onUpgradeneeded
  } = {}) {
    // 企业id
    this.firmId = firmId
    // 个人号信息统计仓库配置
    // this.accountListStoreConfig = Object.assign({}, this.accountListStoreConfig, accountListStoreConfig || {})
    // 仓库配置
    let storeName = this.createStoreName(firmId)
    this.storeConfig = Object.assign({},
      this.storeConfig,
      { name: storeName },
      storeConfig || {}
    )
    // 初始化indexDB,新建仓库，记录各个个人号及其对应的仓库信息。
    // 判断各个个人号、对应仓库是否存在，如果没有对应仓库,就创建
    const dbConfig = this.getDbConfig()
    idb.openDB(
      dbConfig.name,
      dbConfig.version,
      (db, event, version) => {
        Logger.info('constructor-打开现有数据库')
        // 保存数据库连接
        if (!this.db) {
          this.saveDB(db)
        }
        let callback = () => {
          if (onSuccess && (typeof onSuccess === 'function')) {
            onSuccess(this.db)
          }
        }
        // 判断对应企业是否存在，不存在就建表，然后会导致数据库版本升级onUpgradeneeded
        if (!db.objectStoreNames.contains(storeName)) {
          return this.createStoreWithUpdate(db, storeName, callback)
        }
        callback()
      },
      (event) => {
        if (onError && (typeof onError === 'function')) {
          onError(event)
        }
      },
      (db, event, version) => {
        Logger.info('constructor-初始化数据库:v' + version)
        // 数据库版本变化（包括刚新建数据库）
        // 保存数据库连接
        this.saveDB(db)
        // 初始化数据库，建表
        this.initDB(db)
        if (onUpgradeneeded && (typeof onUpgradeneeded === 'function')) {
          onUpgradeneeded(db)
        }
      }
    )
  }

  // 初始化数据库，建表，会导致数据库版本从v1版升级
  initDB(db) {
    // 建表，存储各个用户信息
    idb.createStore(db, this.storeConfig, this.storeConfig.indexs)
    // 1.建立总表，插入数据
    // 2.递归升级数据库，新建各个个人号仓库
  }

  // 企业id
  firmId = null

  // 数据库连接
  db = null

  // Function 获取indexDb名称、版本
  dbConfig(firmId) {
    return config.chatUserInfo(firmId)
  }

  // 个人号客户信息仓库，存储客户信息。
  // 字段根据获取联系人资料的接口来直接存储,indexedDB主键是自增id,字段id为业务id
  storeConfig = {
    name: null,
    indexs: [
      { indexName: 'account_wx_id', keyPath: ['wechatAccountId', 'wxId'], unique: true },
      { indexName: 'account_id', keyPath: 'wechatAccountId', unique: false },
      { indexName: 'wx_id', keyPath: 'wxId', unique: false }
    ]
  }

  // 保存数据库连接
  saveDB(db) {
    this.db = db
  }

  // 关闭数据库连接false
  closeDb() {
    this.db.close()
    this.saveDB(null)
  }

  getDbConfig() {
    let dbConfig
    if (this.dbConfig && (typeof this.dbConfig === 'function')) {
      dbConfig = this.dbConfig(this.firmId)
    } else {
      dbConfig = this.dbConfig
    }
    return dbConfig
  }

  // 记录数据库版本变更
  updateDbConfig(version) {
    let dbConfig = this.getDbConfig()
    dbConfig.version = version
    this.dbConfig = dbConfig
    wechatUserDBInfoLocal.setJSON(dbConfig)
  }

  // 升级数据库版本
  updateDbVersion(success, onerror, onupgradeneeded) {
    const dbConfig = this.getDbConfig()
    let nextVersion = dbConfig.version + 1
    if (this.db) {
      // 关闭当前数据库连接
      idb.closeDB(this.db)
      this.db = null
    }
    idb.openDB(
      dbConfig.name,
      nextVersion,
      (db, event) => {
        if (!this.db) {
          this.db = db
        }
        if (success && (typeof success === 'function')) {
          success(db, event)
        }
      },
      (event) => {
        if (onerror && (typeof onerror === 'function')) {
          onerror(event)
        }
      },
      (db, event, version) => {
        // 数据库版本变化（包括刚新建数据库）
        // 更新缓存中的版本
        this.updateDbConfig(nextVersion)
        // 保存数据库连接
        this.db = db
        if (onupgradeneeded && (typeof onupgradeneeded === 'function')) {
          onupgradeneeded(db, event)
        }
      }
    )
  }

  // 新建另外一张企业的表
  createStoreWithUpdate(db, storeName, onSuccess) {
    this.storeConfig.storeName = storeName
    this.updateDbVersion(() => {
    }, () => {
    }, (db) => {
      idb.createStore(db, {
        name: storeName
      }, this.storeConfig.indexs)
      onSuccess()
    })
  }

  // 生成企业对应表的名称,混肴
  createStoreName(firmId) {
    // let firmIdStr = firmId.toString()
    // let str1 = firmIdStr.substring(0, 1)
    // let str2 = firmIdStr.substring(1) || 'N'
    // let strMixed = 'xxxxxx'
    return `vma-store-${firmId}`
  }

  // 查找多条用户信息
  async selectUserByIds(ids) {
    // 已有本地信息的用户map
    let infoList = []
    // 没有本地信息的用户map
    let emptyList = []
    let len = ids.length
    for (let i = 0; i < len; i++) {
      let accountId = ids[i].wechatAccountId
      let wxId = ids[i].wxId
      if (!(accountId && wxId)) {
        continue
      }
      let result = await this.getDataByIndex({
        key: 'account_wx_id',
        value: [accountId, wxId]
      })
      if (result) {
        infoList.push(result)
      } else {
        emptyList.push(ids[i])
      }
    }
    return {
      infoList,
      emptyList
    }
  }

  // 根据索引查主键,根据复合索引查找返回的是数组，但索引直接返回主键
  async getKeyByIndex(query) {
    // 检查是否存在
    return idb.getKeyByIndex(
      this.db,
      this.storeConfig.name,
      query
    )
  }

  // 根据主键查找用户信息,根据复合索引查找返回的是数组，但索引直接返回主键
  getDataByIndex(query) {
    // 检查是否存在
    return idb.getDataByIndex(
      this.db,
      this.storeConfig.name,
      query
    )
  }

  // 根据主键删除信息
  deleteDataByKey(key) {
    return idb.deleteData(
      this.db,
      this.storeConfig.name,
      key
    )
  }

  // 更新对应的信息
  putData(data, key) {
    return idb.putData(
      this.db,
      this.storeConfig.name,
      data,
      key
    )
  }

  // 新增用户信息
  async addUserInfos(list) {
    const len = list.length
    for (let i = 0; i < len; i++) {
      // 检查是否存在
      let accountId = list[i].wechatAccountId
      let wxId = list[i].wxId
      if (!(accountId && wxId)) {
        continue
      }
      let key = await this.getKeyByIndex({
        key: 'account_wx_id',
        value: [accountId, wxId]
      })
      if (key) continue
      await idb.addData(this.db, this.storeConfig.name, list[i])
    }
  }

  // 更新对应的用户信息
  async putUserInfo(data) {
    if (!(data.wxId && data.wechatAccountId)) return
    // 是否需要先检查是否存在，如果不存在，就不更新
    let key = await this.getKeyByIndex({
      key: 'account_wx_id',
      value: [data.wechatAccountId, data.wxId]
    })
    if (key) {
      return this.putData(data, key)
    }
    // throw new Error('不存在该条记录')
    idb.addData(this.db, this.storeConfig.name, data)
  }

  // 删除对应的客户
  async deleteUserInfo(accountId, wxId) {
    if (!(wxId && accountId)) return
    let key = await this.getKeyByIndex({
      key: 'account_wx_id',
      value: [accountId, wxId]
    })
    if (key) {
      await this.deleteDataByKey(key)
    }
  }

  // 删除个人号的所有用户信息
  async deleteAccountUserInfo(accountId) {
    if (!accountId) return
    let keys = await this.getKeyByIndex({
      key: 'account_id',
      value: accountId,
      all: true
    })
    if (keys.length > 0) {
      keys.some(v => {
        this.deleteDataByKey(v)
      })
    }
  }

  async refreshRemark(accountId, wxId, remark) {
    if (!(wxId && accountId)) return
    let data = await this.getDataByIndex({
      key: 'account_wx_id',
      value: [accountId, wxId]
    })
    if (data) {
      data.remark = remark
      await this.putUserInfo(data)
    }
  }
}
export default UserInfoDB
