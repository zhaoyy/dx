'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')
/**
 * https://cxcrmdev.cpic.com.cn/cxsijp/home.html#/index  太保本地开发环境
 * https://cxcrmsit.cpic.com.cn/cxsijp/home.html#/index  太保sit测试环境
 * 环境 AES加密key
 * 开发 DRkqRBiuyt6uDeU5
 * 测试 r7UCre12rftO265a
 */
module.exports = merge(prodEnv, {
  NODE_ENV: '"test"',
  api: '"https://t-cpic-api.wksaas.net"',
  imApi: '"https://t-cpic-im.wksaas.net"',
  aiWs: '"wss://multirobot-test.kxjlcc.com:13997/microservice/ws/"',
  aiApi: '"https://multirobot-test.kxjlcc.com:13997"',
  cxsijp: '"https://cxcrmsit.cpic.com.cn/cxsijp/home.html#/index"',
  aseKey: 'r7UCre12rftO265a'
})
