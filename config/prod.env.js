'use strict'
module.exports = {
  NODE_ENV: '"production"',
  app: '"vma-web-scrm"',
  theme: '"default"',

  api: '"https://business.api.scrm.ecdpower.net"',
  imApi: '"https://tencent.scrm.ecdpower.net"',
  aiWs: '"wss://ai-ui.kxjlcc.com:13997/microservice/ws/"',
  aiApi: '"https://ai-ui.kxjlcc.com:13997"',
  /**
   * https://cxcrmdev.cpic.com.cn/cxsijp/home.html#/index  太保本地开发环境
   * https://cxcrmsit.cpic.com.cn/cxsijp/home.html#/index  太保sit测试环境
   * 环境 AES加密key
   * 开发 DRkqRBiuyt6uDeU5
   * 测试 r7UCre12rftO265a
   */
  cxsijp: '"https://cxydcrm.cpic.com.cn/cxsijp/home.html#/index"',
  aseKey: 'LmElapQlTsVlbu5p',


  vmaH5Scrm: '"https://h5.scrm.ecdpower.net"',
  // 浏览器tab上显示的标题
  webTitle: '"太平洋保险"',
  // 浏览器tab上显示的icon 64*64
  webIcon: '"https://taibao-image.ecdpower.net/IMG-H-3b6b8b56-f8d6-4736-a1c2-c148ca205f27"',
  // 系统后台logo 124*26
  systemLogo: '"https://s1.ax1x.com/2020/03/12/8Vxwkj.png"',
  // 登录页logo 415*48
  loginLogo: '"https://s1.ax1x.com/2020/03/12/8Vv8LF.png"',
  // 登录页背景图 1920*1080
  loginBg: '"https://taibao-image.ecdpower.net/IMG-H-aaf316ae-1c74-4751-9e67-19e2d4e9fc9b"',
  // 登录页客服热线
  hotLine: '"400-021-1220"',
  // 登录页备案号
  caseNumber: '"闽ICP备16000346号-3"',
  // 登录页版权所有
  copyRight: '"上海盟鼎网络科技有限公司"',
  // 登录页公众号二维码 160*160
  qrCode: '"https://taibao-image.ecdpower.net/taibao_service.png"',
  // 登录页技术支持
  technicalSupport: '"technicalSupport: \'盟鼎科技\'"',
  // 是否显示微客服上商品推荐
  showChatHeaderProductionRecommand: '"false"',
  // 是否显示微客服上购物订单
  showChatHeaderShoppingOrder: '"false"',
  // 是否显示登录也客服热线
  showLoginHotLine: '"false"'
}
