'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"local"',
  api: '"https://t-cpic-api.wksaas.net"',
  imApi: '"https://t-cpic-im.wksaas.net"',
  aseKey: 'DRkqRBiuyt6uDeU5',
})
